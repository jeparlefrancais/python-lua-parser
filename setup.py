from setuptools import setup, find_packages


def readme():
    with open('README.md') as f:
        return f.read()


def get_version():
    with open('VERSION') as f:
        return f.read()


setup(
    name='lua-parser',
    version=get_version(),
    description='A lua parser entirely written in python, without any external dependencies',
    long_description=readme(),
    long_description_content_type='text/markdown',
    url='https://gitlab.com/jeparlefrancais21/python-lua-parser',
    author='jeparlefrancais',
    author_email='jeparlefrancais21@gmail.com',
    keywords='lua parser',
    packages=find_packages(),
    install_requires='',
    test_suite='tests',
    zip_safe=False,
)
