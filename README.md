[![pipeline status](https://gitlab.com/seaofvoices/horror-game/badges/master/pipeline.svg)](https://gitlab.com/jeparlefrancais/python-lua-parser/pipelines)

# luaparser

- Typed
- Preserve whitespace information
- Depends only on standard library packages

## Install

This python package can be installed using `pip` with the following command. Make sure to enter the `-` in `lua-parser`, otherwise you'll end up with a different parser.

```bash
python -m pip install lua-parser
```

## Docs

The official documentation can be found [here](https://jeparlefrancais.gitlab.io/python-lua-parser). You'll find the API for the different nodes of the abstract syntax tree built from this parser.

## Usage Example

```python
from luaparser import Parser

parser = Parser()
node = parser.parse('print("Hello, world!"')
```
