from typing import Any, Callable, List, Optional, Sequence, Tuple, Type, TypeVar, Union

from luaparser import nodes, tokens

T = TypeVar('T')
_comparators: List[Tuple[type, Callable[[T, T, 'NodeComparator'], None]]] = []
ComparatorFunction = Callable[[T, T, 'NodeComparator'], None]


def register_comparator(
    instance: Type[T]
) -> Callable[[Callable[[T, T, 'NodeComparator'], None]], Callable[[T, T, 'NodeComparator'], None]]:

    def wrap(func: ComparatorFunction) -> ComparatorFunction:
        _comparators.append((instance, func))
        return func

    return wrap


class NodeComparator():

    def __init__(self, indentation_level=2):
        self._output: List[str] = []
        self._level = -1
        self._indentation = ' ' * indentation_level

    def compare(self, expect: Any, result: Any):
        self._level += 1

        if type(expect) != type(result):
            self.append('expected type to be {} but got {}'.format(type(expect), type(result)))
        elif expect != result:
            self.append('{} != {}'.format(expect, result))

            for instance, comparator in _comparators:
                if isinstance(expect, instance):
                    comparator(expect, result, self)

        self._level -= 1

    def compare_lists(
        self,
        expect: Sequence[T],
        result: Sequence[T],
        parent: str,
        element: str,
        plural='s',
    ):
        self._level += 1
        expect_count = len(expect)
        result_count = len(result)

        for i in range(0, expect_count):
            if i == result_count:
                self.append('result {} is missing {}({}):'.format(
                    parent,
                    element,
                    plural,
                ))
                self.append('{}'.format(expect[i]))
            elif i > result_count:
                self.append('{}'.format(expect[i]))
            elif expect[i] != result[i]:
                self.append('result {} has unexpected {} #{}:'.format(
                    parent,
                    element,
                    i + 1,
                ))
                self.compare(expect[i], result[i])
                break

        if result_count > expect_count:
            self.append('result {} has unexpected {}({}):'.format(
                parent,
                element,
                plural,
            ))
            for j in range(expect_count, result_count):
                self.append('{}'.format(result[j]))
        self._level -= 1

    def compare_optionals(
        self,
        expect: Optional[T],
        result: Optional[T],
        parent: str,
        element: str,
    ):
        self._level += 1
        need_compare = False

        if expect is None:
            self.append('result {} has an unexpected {}: {}'.format(
                parent,
                element,
                result,
            ))
        else:
            if result is None:
                self.append('result {} is missing a {}: {}'.format(
                    parent,
                    element,
                    expect,
                ))
            else:
                need_compare = True

        self._level -= 1

        if need_compare:
            self.compare(expect, result)

    def append(self, message: str):
        self._output.append('{}{}'.format(self._indentation * self._level, message))

    def get_output(self) -> str:
        return '\n'.join(self._output)
