import unittest

from luaparser import nodes, tokens
from tests.int_test_parser import IntTestParser


class IntTestFileStatement(IntTestParser, unittest.TestCase):

    def get_lua_test_script(self):
        return 'local_variables'

    def get_expected_result(self):
        return self.create_chunk(
            nodes.Block([
                nodes.LocalVariableAssignStatement(
                    nodes.NameList([self.get_var('a', 1)]),
                    None,
                    self.get_local(1, ''),
                    None,
                    None,
                ),
                nodes.LocalVariableAssignStatement(
                    nodes.NameList([self.get_var('b', 2)]),
                    None,
                    self.get_local(2),
                    None,
                    None,
                ),
                nodes.LocalVariableAssignStatement(
                    nodes.NameList(
                        [self.get_var('c', 3), self.get_var('d', 3)],
                        [self.get_comma(3)],
                    ),
                    None,
                    self.get_local(3),
                    None,
                    None,
                ),
                nodes.LocalVariableAssignStatement(
                    nodes.NameList([self.get_var('e', 4)]),
                    nodes.ExpressionList([nodes.VariableExpression(self.get_var('a', 4))]),
                    self.get_local(4),
                    self.get_assign(4),
                    None,
                ),
                nodes.LocalVariableAssignStatement(
                    nodes.NameList(
                        [self.get_var('f', 5), self.get_var('g', 5)],
                        [self.get_comma(5)],
                    ),
                    nodes.ExpressionList(
                        [
                            nodes.VariableExpression(self.get_var('b', 5)),
                            nodes.VariableExpression(self.get_var('c', 5)),
                        ],
                        [self.get_comma(5)],
                    ),
                    self.get_local(5),
                    self.get_assign(5),
                    None,
                ),
                nodes.LocalVariableAssignStatement(
                    nodes.NameList([self.get_var('h', 6)]),
                    nodes.ExpressionList(
                        [
                            nodes.VariableExpression(self.get_var('a', 6)),
                            nodes.VariableExpression(self.get_var('b', 6)),
                            nodes.VariableExpression(self.get_var('c', 6)),
                        ],
                        [self.get_comma(6), self.get_comma(6)],
                    ),
                    self.get_local(6),
                    self.get_assign(6),
                    None,
                ),
                nodes.LocalVariableAssignStatement(
                    nodes.NameList(
                        [
                            self.get_var('i', 7),
                            self.get_var('j', 7),
                            self.get_var('k', 7),
                        ],
                        [self.get_comma(7), self.get_comma(7)],
                    ),
                    nodes.ExpressionList(
                        [
                            nodes.VariableExpression(self.get_var('a', 7)),
                            nodes.VariableExpression(self.get_var('b', 7)),
                            nodes.VariableExpression(self.get_var('c', 7)),
                        ],
                        [self.get_comma(7), self.get_comma(7)],
                    ),
                    self.get_local(7),
                    self.get_assign(7),
                    None,
                ),
                nodes.LocalVariableAssignStatement(
                    nodes.NameList([self.get_var('l', 8)]),
                    None,
                    self.get_local(8),
                    None,
                    self.wrap_token(tokens.SemiColon(), 8),
                ),
                nodes.LocalVariableAssignStatement(
                    nodes.NameList([self.get_var('m', 9)]),
                    None,
                    self.get_local(9),
                    None,
                    None,
                ),
                nodes.LocalVariableAssignStatement(
                    nodes.NameList([self.get_var('n', 9)]),
                    None,
                    self.get_local(9, ' '),
                    None,
                    None,
                ),
            ]),
            9,
            '',
        )

    def get_var(self, name, line):
        return self.wrap_token(tokens.Identifier(name), line, ' ')

    def get_local(self, line, whitespace='\n'):
        return self.wrap_token(tokens.LocalKeyword(), line, whitespace)

    def get_comma(self, line):
        return self.wrap_token(tokens.Comma(), line)

    def get_assign(self, line):
        return self.wrap_token(tokens.Assign(), line, ' ')
