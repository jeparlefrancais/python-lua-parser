import unittest

from luaparser import nodes, tokens
from tests.int_test_parser import IntTestParser


class IntTestFileStatement(IntTestParser, unittest.TestCase):

    def get_lua_test_script(self):
        return 'statements'

    def get_expected_result(self):
        return self.create_chunk(
            nodes.Block([
                nodes.NumericForStatement(
                    self.wrap_token(tokens.Identifier('i'), 1, ' '),
                    nodes.Block([
                        nodes.BreakStatement(
                            self.wrap_token(tokens.BreakKeyword(), 2, '\n    '),
                            None,
                        ),
                    ]),
                    nodes.NumberExpression(self.wrap_token(tokens.Number(1, '1'))),
                    nodes.NumberExpression(self.wrap_token(tokens.Number(10, '10'), 1, ' ')),
                    None,
                    self.wrap_token(tokens.ForKeyword()),
                    self.wrap_token(tokens.DoKeyword(), 1, ' '),
                    self.wrap_token(tokens.EndKeyword(), 3, '\n'),
                    self.wrap_token(tokens.Assign()),
                    self.wrap_token(tokens.Comma()),
                    None,
                ),
                nodes.IfStatement(
                    nodes.Block([
                        nodes.ReturnStatement(
                            self.wrap_token(tokens.ReturnKeyword(), 6, '\n    '),
                            nodes.ExpressionList(),
                        )
                    ]),
                    nodes.FalseExpression(self.wrap_token(tokens.FalseKeyword(), 5, ' ')),
                    [
                        nodes.ElseifBranch(
                            nodes.Block([
                                nodes.ReturnStatement(
                                    self.wrap_token(tokens.ReturnKeyword(), 8, '\n    '),
                                    nodes.ExpressionList([
                                        nodes.FalseExpression(
                                            self.wrap_token(
                                                tokens.FalseKeyword(),
                                                8,
                                                ' ',
                                            )
                                        ),
                                    ]),
                                )
                            ]),
                            nodes.TrueExpression(self.wrap_token(tokens.TrueKeyword(), 7, ' ')),
                            self.wrap_token(tokens.ElseifKeyword(), 7, '\n'),
                            self.wrap_token(tokens.ThenKeyword(), 7, ' '),
                        )
                    ],
                    nodes.ElseBranch(
                        nodes.Block([
                            nodes.DoStatement(
                                nodes.Block(),
                                self.wrap_token(tokens.DoKeyword(), 10, '\n    '),
                                self.wrap_token(tokens.EndKeyword(), 12, '\n\n    '),
                            )
                        ]),
                        self.wrap_token(tokens.ElseKeyword(), 9, '\n'),
                    ),
                    self.wrap_token(tokens.IfKeyword(), 5, '\n\n'),
                    self.wrap_token(tokens.ThenKeyword(), 5, ' '),
                    self.wrap_token(tokens.EndKeyword(), 13, '\n'),
                ),
                nodes.NumericForStatement(
                    self.wrap_token(tokens.Identifier('j'), 15, ' '),
                    nodes.Block(),
                    nodes.NumberExpression(self.wrap_token(tokens.Number(1, '1'), 15)),
                    nodes.NumberExpression(self.wrap_token(tokens.Number(10, '10'), 15, ' ')),
                    nodes.NumberExpression(self.wrap_token(tokens.Number(2, '2'), 15, ' ')),
                    self.wrap_token(tokens.ForKeyword(), 15, '\n\n'),
                    self.wrap_token(tokens.DoKeyword(), 15, ' '),
                    self.wrap_token(tokens.EndKeyword(), 17, '\n\n'),
                    self.wrap_token(tokens.Assign(), 15),
                    self.wrap_token(tokens.Comma(), 15),
                    self.wrap_token(tokens.Comma(), 15),
                ),
                nodes.LocalVariableAssignStatement(
                    nodes.NameList([self.wrap_token(tokens.Identifier('a'), 19, ' ')]),
                    nodes.ExpressionList([
                        nodes.TrueExpression(self.wrap_token(tokens.TrueKeyword(), 19, ' '))
                    ]),
                    self.wrap_token(tokens.LocalKeyword(), 19, '\n\n'),
                    self.wrap_token(tokens.Assign(), 19, ' '),
                    None,
                ),
                nodes.LocalVariableAssignStatement(
                    nodes.NameList([self.wrap_token(tokens.Identifier('b'), 20, ' ')]),
                    nodes.ExpressionList([
                        nodes.FalseExpression(self.wrap_token(tokens.FalseKeyword(), 20, ' '))
                    ]),
                    self.wrap_token(tokens.LocalKeyword(), 20, '\n'),
                    self.wrap_token(tokens.Assign(), 20, ' '),
                ),
                nodes.RepeatStatement(
                    nodes.Block([
                        nodes.VariableAssignStatement(
                            nodes.VariableList(
                                [
                                    nodes.VariableExpression(
                                        self.wrap_token(
                                            tokens.Identifier('a'),
                                            23,
                                            '\n    ',
                                        )
                                    ),
                                    nodes.VariableExpression(
                                        self.wrap_token(
                                            tokens.Identifier('b'),
                                            23,
                                            ' ',
                                        )
                                    ),
                                ],
                                [self.wrap_token(tokens.Comma(), 23)],
                            ),
                            nodes.ExpressionList(
                                [
                                    nodes.VariableExpression(
                                        self.wrap_token(
                                            tokens.Identifier('b'),
                                            23,
                                            ' ',
                                        )
                                    ),
                                    nodes.VariableExpression(
                                        self.wrap_token(
                                            tokens.Identifier('a'),
                                            23,
                                            ' ',
                                        )
                                    ),
                                ],
                                [self.wrap_token(tokens.Comma(), 23)],
                            ),
                            self.wrap_token(tokens.Assign(), 23, ' '),
                        )
                    ]),
                    nodes.VariableExpression(self.wrap_token(tokens.Identifier('a'), 24, ' ')),
                    self.wrap_token(tokens.RepeatKeyword(), 22, '\n\n'),
                    self.wrap_token(tokens.UntilKeyword(), 24, '\n'),
                ),
                nodes.WhileStatement(
                    nodes.FalseExpression(self.wrap_token(tokens.FalseKeyword(), 26, ' ')),
                    nodes.Block([
                        nodes.LocalVariableAssignStatement(
                            nodes.NameList(
                                [
                                    self.wrap_token(tokens.Identifier('c'), 27, ' '),
                                    self.wrap_token(tokens.Identifier('d'), 27, ' '),
                                ],
                                [self.wrap_token(tokens.Comma(), 27)],
                            ),
                            None,
                            self.wrap_token(tokens.LocalKeyword(), 27, '\n    '),
                            None,
                        ),
                        nodes.LocalFunctionAssignStatement(
                            self.wrap_token(tokens.Identifier('done'), 28, ' '),
                            nodes.FunctionExpression(
                                nodes.Block([
                                    nodes.ReturnStatement(
                                        self.wrap_token(tokens.ReturnKeyword(), 29, '\n        '),
                                        nodes.ExpressionList(
                                            [
                                                nodes.VariableExpression(
                                                    self.wrap_token(
                                                        tokens.Identifier('c'),
                                                        29,
                                                        ' ',
                                                    )
                                                ),
                                                nodes.VariableExpression(
                                                    self.wrap_token(
                                                        tokens.Identifier('d'),
                                                        29,
                                                        ' ',
                                                    )
                                                ),
                                            ],
                                            [self.wrap_token(tokens.Comma(), 29)],
                                        ),
                                        None,
                                    ),
                                ]),
                                [],
                                self.wrap_token(tokens.FunctionKeyword(), 28, ' '),
                                self.wrap_token(tokens.EndKeyword(), 30, '\n    '),
                                self.wrap_token(tokens.OpeningParenthese(), 28),
                                self.wrap_token(tokens.ClosingParenthese(), 28),
                            ),
                            self.wrap_token(tokens.LocalKeyword(), 28, '\n    '),
                        )
                    ]),
                    self.wrap_token(tokens.WhileKeyword(), 26, '\n\n'),
                    self.wrap_token(tokens.DoKeyword(), 26, ' '),
                    self.wrap_token(tokens.EndKeyword(), 31, '\n'),
                )
            ]), 32, '\n'
        )
