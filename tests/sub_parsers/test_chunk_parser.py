import unittest

from luaparser import nodes, tokens, TokenQueue
from luaparser.exceptions import UnexpectedTokenException
from luaparser.sub_parsers import ChunkParser


class TestChunkParser(unittest.TestCase):

    def setUp(self):
        self.parser = ChunkParser()

    def validate(self, token_list, expect_chunk):
        result = self.parser.parse(TokenQueue(token_list))
        self.assertEqual(result, expect_chunk)

    def test_parse_end_of_file_return_empty_chunk(self):
        self.validate([tokens.EndOfFile()], nodes.Chunk(nodes.Block(), tokens.EndOfFile()))

    def test_parse_assign_token_raise_unexpected_token_exception(self):
        self.assertRaises(
            UnexpectedTokenException,
            self.parser.parse,
            TokenQueue([tokens.Assign()]),
        )
