import unittest

from luaparser import nodes, tokens, TokenQueue
from luaparser.exceptions import ExpressionExpectedException
from luaparser.sub_parsers import LocalVariableAssignStatementParser


class TestLocalVariableStatementParser(unittest.TestCase):

    def setUp(self):
        self.parser = LocalVariableAssignStatementParser()
        self.local_token = tokens.LocalKeyword()
        self.identifier = tokens.Identifier('foo')

    def validate(self, token_list, expect_statement):
        token_list.append(tokens.EndOfFile())
        result = self.parser.parse(self.local_token, TokenQueue(token_list))
        self.assertEqual(result, expect_statement)

    def assert_exception(self, exception, token_list):
        token_list.append(tokens.EndOfFile())

        with self.assertRaises(exception) as context:
            self.parser.parse(self.local_token, TokenQueue(token_list))

        return context.exception

    def test_parse_single_identifier_without_expressions(self):
        self.validate(
            [self.identifier],
            nodes.LocalVariableAssignStatement(
                nodes.NameList([self.identifier]),
                None,
                self.local_token,
                None,
            ),
        )

    def test_parse_two_identifiers_without_expressions(self):
        comma = tokens.Comma()
        self.validate(
            [self.identifier, comma, self.identifier],
            nodes.LocalVariableAssignStatement(
                nodes.NameList([self.identifier, self.identifier], [comma]),
                None,
                self.local_token,
                None,
            ),
        )

    def test_parse_single_identifier_with_expression(self):
        assign_token = tokens.Assign()
        true_token = tokens.TrueKeyword()
        self.validate(
            [self.identifier, assign_token, true_token],
            nodes.LocalVariableAssignStatement(
                nodes.NameList([self.identifier]),
                nodes.ExpressionList([nodes.TrueExpression(true_token)]),
                self.local_token,
                assign_token,
            ),
        )

    def test_parse_single_identifier_without_expression_raise_exception(self):
        assign_token = tokens.Assign()
        exception = self.assert_exception(
            ExpressionExpectedException,
            [self.identifier, assign_token],
        )

        self.assertEqual(assign_token, exception.get_previous_token())
