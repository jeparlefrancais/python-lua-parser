import unittest
from unittest import mock

from luaparser import exceptions, nodes, tokens, TokenQueue
from tests.comparator import NodeComparator
from luaparser.sub_parsers import ExpressionParser


class TestExpressionParser(unittest.TestCase):

    def setUp(self):
        self.parser = ExpressionParser()

    def validate(self, token_list, expect_expression):
        end_of_file = tokens.EndOfFile()
        token_list.append(end_of_file)
        queue = TokenQueue(token_list)
        result = self.parser.try_parse(queue)

        if result != expect_expression:
            self.fail(self.compare(expect_expression, result))

        self.assertTrue(queue.pop_front(), end_of_file)

    def compare(self, expect, result):
        comparator = NodeComparator()
        comparator.compare(expect, result)
        return comparator.get_output()

    def assert_exception(self, exception, token_list):
        with self.assertRaises(exception) as context:
            self.parser.try_parse(TokenQueue(token_list + [tokens.EndOfFile()]))

        return context.exception

    def test_parse_true_token(self):
        true_token = tokens.TrueKeyword()
        self.validate([true_token], nodes.TrueExpression(true_token))

    def test_parse_false_token(self):
        false_token = tokens.FalseKeyword()
        self.validate([false_token], nodes.FalseExpression(false_token))

    def test_parse_nil_token(self):
        nil_token = tokens.NilKeyword()
        self.validate([nil_token], nodes.NilExpression(nil_token))

    def test_parse_number_token(self):
        number_token = tokens.Number(0, '0')
        self.validate([number_token], nodes.NumberExpression(number_token))

    def test_parse_string_token(self):
        string_token = tokens.String('hello', True)
        self.validate([string_token], nodes.StringExpression(string_token))

    def test_parse_long_string_token(self):
        string_token = tokens.LongString('hello', 0)
        self.validate([string_token], nodes.StringExpression(string_token))

    def test_parse_var_args_token(self):
        var_args_token = tokens.VarArgs()
        self.validate([var_args_token], nodes.VarArgsExpression(var_args_token))

    def test_parse_parenthese_expression(self):
        left_parenthese = tokens.OpeningParenthese()
        right_parenthese = tokens.ClosingParenthese()
        true_token = tokens.TrueKeyword()
        self.validate(
            [
                left_parenthese,
                true_token,
                right_parenthese,
            ],
            nodes.ParentheseExpression(
                nodes.TrueExpression(true_token),
                left_parenthese,
                right_parenthese,
            ),
        )

    def test_parse_parenthese_expression_missing_expression_raise_exception(self):
        left_parenthese = tokens.OpeningParenthese()
        right_parenthese = tokens.ClosingParenthese()
        exception = self.assert_exception(
            exceptions.ExpressionExpectedException,
            [left_parenthese, right_parenthese],
        )
        self.assertEqual(exception.get_previous_token(), left_parenthese)

    def test_parse_parenthese_expression_missing_closing_parenthese_raise_exception(self):
        left_parenthese = tokens.OpeningParenthese()
        true_token = tokens.TrueKeyword()
        false_token = tokens.FalseKeyword()
        exception = self.assert_exception(
            exceptions.SymbolExpectedException,
            [left_parenthese, true_token, false_token],
        )
        self.assertEqual(exception.get_expected_symbol(), ')')
        self.assertEqual(exception.get_next_token(), false_token)

    def validate_unary_expression(self, operator):
        true_token = tokens.TrueKeyword()
        self.validate(
            [operator, true_token],
            nodes.UnaryOperationExpression(
                nodes.TrueExpression(true_token),
                operator,
            ),
        )

    def test_parse_length_expression(self):
        self.validate_unary_expression(tokens.Length())

    def test_parse_not_expression(self):
        self.validate_unary_expression(tokens.NotKeyword())

    def test_parse_negation_expression(self):
        self.validate_unary_expression(tokens.Minus())

    def test_parse_variable_expression(self):
        identifier = tokens.Identifier('foo')
        self.validate(
            [identifier],
            nodes.VariableExpression(identifier),
        )

    def test_parse_field_expression(self):
        table_identifier = tokens.Identifier('foo')
        dot_token = tokens.Dot()
        field = tokens.Identifier('bar')
        self.validate(
            [table_identifier, dot_token, field],
            nodes.FieldExpression(
                nodes.VariableExpression(table_identifier),
                field,
                dot_token,
            ),
        )

    def test_parse_field_expression_without_field_name_raise_exception_with_dot_token(self):
        table_identifier = tokens.Identifier('foo')
        dot_token = tokens.Dot()
        exception = self.assert_exception(
            exceptions.NameExpectedException,
            [table_identifier, dot_token],
        )
        self.assertEqual(exception.get_dot_token(), dot_token)

    def test_parse_index_expression(self):
        table_identifier = tokens.Identifier('foo')
        opening_bracket = tokens.OpeningSquareBracket()
        value = tokens.Identifier('bar')
        closing_bracket = tokens.ClosingSquareBracket()
        self.validate(
            [table_identifier, opening_bracket, value, closing_bracket],
            nodes.IndexExpression(
                nodes.VariableExpression(table_identifier),
                nodes.VariableExpression(value),
                opening_bracket,
                closing_bracket,
            ),
        )

    def test_parse_index_expression_with_missing_ending_bracket_raise_exception(self):
        table_identifier = tokens.Identifier('foo')
        opening_bracket = tokens.OpeningSquareBracket()
        value = tokens.Identifier('bar')

        exception = self.assert_exception(
            exceptions.SymbolExpectedException,
            [table_identifier, opening_bracket, value, table_identifier],
        )
        self.assertEqual(exception.get_expected_symbol(), ']')
        self.assertEqual(exception.get_next_token(), table_identifier)

    def test_parse_index_expression_with_missing_expression_raise_exception(self):
        table_identifier = tokens.Identifier('foo')
        opening_bracket = tokens.OpeningSquareBracket()
        closing_bracket = tokens.ClosingSquareBracket()

        exception = self.assert_exception(
            exceptions.ExpressionExpectedException,
            [table_identifier, opening_bracket, opening_bracket],
        )
        self.assertEqual(exception.get_previous_token(), opening_bracket)

    def test_parse_call_expression(self):
        variable = tokens.Identifier('foo')
        opening_parenthese = tokens.OpeningParenthese()
        closing_parenthese = tokens.ClosingParenthese()
        self.validate(
            [variable, opening_parenthese, closing_parenthese],
            nodes.CallExpression(
                nodes.VariableExpression(variable),
                nodes.TupleArguments(
                    nodes.ExpressionList(),
                    opening_parenthese,
                    closing_parenthese,
                ),
            ),
        )

    def test_parse_method_expression(self):
        variable = tokens.Identifier('foo')
        colon_token = tokens.Colon()
        method_name = tokens.Identifier('bar')
        opening_parenthese = tokens.OpeningParenthese()
        closing_parenthese = tokens.ClosingParenthese()
        self.validate(
            [variable, colon_token, method_name, opening_parenthese, closing_parenthese],
            nodes.MethodExpression(
                nodes.VariableExpression(variable),
                method_name,
                nodes.TupleArguments(
                    nodes.ExpressionList(),
                    opening_parenthese,
                    closing_parenthese,
                ),
                colon_token,
            ),
        )

    def test_parse_method_expression_without_method_name_raise_exception_with_colon_token(self):
        variable = tokens.Identifier('foo')
        colon_token = tokens.Colon()
        opening_parenthese = tokens.OpeningParenthese()
        closing_parenthese = tokens.ClosingParenthese()
        exception = self.assert_exception(
            exceptions.MethodNameExpectedException,
            [variable, colon_token, opening_parenthese, closing_parenthese],
        )
        self.assertEqual(exception.get_colon_token(), colon_token)

    @mock.patch('luaparser.sub_parsers.expression_parser.TableParser')
    def test_parse_table_expression_calls_table_parser(self, table_parser_class):
        table_expression_mock = mock.MagicMock()
        table_parser_class().parse.return_value = table_expression_mock
        result = self.parser.try_parse(TokenQueue([
            tokens.OpeningBrace(),
            tokens.ClosingBrace(),
        ]))
        self.assertEqual(result, table_expression_mock)

    @mock.patch('luaparser.sub_parsers.expression_parser.FunctionExpressionParser')
    def test_parse_function_expression_calls_function_expression_parser(
        self,
        function_expression_parser_class,
    ):
        function_expression_mock = mock.MagicMock()
        function_expression_parser_class().parse.return_value = function_expression_mock
        result = self.parser.try_parse(TokenQueue([
            tokens.FunctionKeyword(),
        ]))
        self.assertEqual(result, function_expression_mock)

    def test_parse_binary_and_expression(self):
        true_keyword = tokens.TrueKeyword()
        and_keyword = tokens.AndKeyword()
        false_keyword = tokens.FalseKeyword()
        self.validate(
            [true_keyword, and_keyword, false_keyword],
            nodes.BinaryOperationExpression(
                nodes.TrueExpression(true_keyword),
                nodes.FalseExpression(false_keyword),
                and_keyword,
            ),
        )

    def test_parse_binary_or_expression(self):
        true_keyword = tokens.TrueKeyword()
        or_keyword = tokens.OrKeyword()
        false_keyword = tokens.FalseKeyword()
        self.validate(
            [true_keyword, or_keyword, false_keyword],
            nodes.BinaryOperationExpression(
                nodes.TrueExpression(true_keyword),
                nodes.FalseExpression(false_keyword),
                or_keyword,
            ),
        )

    def test_parse_binary_or_with_missing_right_expression_raise_exception(self):
        true_keyword = tokens.TrueKeyword()
        or_keyword = tokens.OrKeyword()
        exception = self.assert_exception(
            exceptions.ExpressionExpectedException,
            [true_keyword, or_keyword],
        )
        self.assertEqual(or_keyword, exception.get_previous_token())

    def test_parse_precedence_true_or_true_and_false(self):
        true_keyword = tokens.TrueKeyword()
        or_keyword = tokens.OrKeyword()
        and_keyword = tokens.AndKeyword()
        false_keyword = tokens.FalseKeyword()
        self.validate(
            [true_keyword, or_keyword, true_keyword, and_keyword, false_keyword],
            nodes.BinaryOperationExpression(
                nodes.TrueExpression(true_keyword),
                nodes.BinaryOperationExpression(
                    nodes.TrueExpression(true_keyword),
                    nodes.FalseExpression(false_keyword),
                    and_keyword,
                ),
                or_keyword,
            ),
        )

    def test_parse_precedence_false_and_true_or_true(self):
        true_keyword = tokens.TrueKeyword()
        or_keyword = tokens.OrKeyword()
        and_keyword = tokens.AndKeyword()
        false_keyword = tokens.FalseKeyword()
        self.validate(
            [false_keyword, and_keyword, true_keyword, or_keyword, true_keyword],
            nodes.BinaryOperationExpression(
                nodes.BinaryOperationExpression(
                    nodes.FalseExpression(false_keyword),
                    nodes.TrueExpression(true_keyword),
                    and_keyword,
                ),
                nodes.TrueExpression(true_keyword),
                or_keyword,
            ),
        )

    def test_parse_exponent_right_associativity(self):
        two = tokens.Number(2, '2')
        caret = tokens.Caret()
        three = tokens.Number(3, '3')
        self.validate(
            [two, caret, two, caret, three],
            nodes.BinaryOperationExpression(
                nodes.NumberExpression(two),
                nodes.BinaryOperationExpression(
                    nodes.NumberExpression(two),
                    nodes.NumberExpression(three),
                    caret,
                ),
                caret,
            ),
        )

    def test_parse_exponent_precedence_over_unary_minus(self):
        minus = tokens.Minus()
        two = tokens.Number(2, '2')
        caret = tokens.Caret()
        three = tokens.Number(3, '3')
        self.validate(
            [minus, two, caret, three],
            nodes.UnaryOperationExpression(
                nodes.BinaryOperationExpression(
                    nodes.NumberExpression(two),
                    nodes.NumberExpression(three),
                    caret,
                ),
                minus,
            ),
        )

    def test_parse_plus_precedence_over_concatenation(self):
        plus = tokens.Plus()
        concat = tokens.Concat()
        two = tokens.Number(2, '2')
        three = tokens.Number(3, '3')
        variable = tokens.Identifier('var')
        self.validate(
            [two, plus, three, concat, variable],
            nodes.BinaryOperationExpression(
                nodes.BinaryOperationExpression(
                    nodes.NumberExpression(two),
                    nodes.NumberExpression(three),
                    plus,
                ),
                nodes.VariableExpression(variable),
                concat,
            ),
        )

    def test_parse_unary_minus_precedence_over_addition(self):
        minus = tokens.Minus()
        two = tokens.Number(2, '2')
        plus = tokens.Plus()
        three = tokens.Number(3, '3')
        self.validate(
            [minus, two, plus, three],
            nodes.BinaryOperationExpression(
                nodes.UnaryOperationExpression(
                    nodes.NumberExpression(two),
                    minus,
                ),
                nodes.NumberExpression(three),
                plus,
            ),
        )

    def test_parse_unary_minus(self):
        minus = tokens.Minus()
        two = tokens.Number(2, '2')
        self.validate(
            [minus, two],
            nodes.UnaryOperationExpression(
                nodes.NumberExpression(two),
                minus,
            ),
        )

    def test_parse_unary_length_expression(self):
        length_token = tokens.Length()
        identifier = tokens.Identifier('foo')
        self.validate(
            [length_token, identifier],
            nodes.UnaryOperationExpression(
                nodes.VariableExpression(identifier),
                length_token,
            ),
        )

    def test_parse_unary_not_expression(self):
        not_token = tokens.NotKeyword()
        identifier = tokens.Identifier('foo')
        self.validate(
            [not_token, identifier],
            nodes.UnaryOperationExpression(
                nodes.VariableExpression(identifier),
                not_token,
            ),
        )

    def test_parse_unary_operator_without_expression_raise_exception(self):
        not_token = tokens.NotKeyword()
        exception = self.assert_exception(exceptions.ExpressionExpectedException, [not_token])

        self.assertEqual(not_token, exception.get_previous_token())
