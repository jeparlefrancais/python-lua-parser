import unittest

from luaparser import exceptions, nodes, tokens, TokenQueue
from luaparser.sub_parsers import TableParser


class TestTableParser(unittest.TestCase):

    def setUp(self):
        self.parser = TableParser()
        self.left_brace = tokens.OpeningBrace()
        self.right_brace = tokens.ClosingBrace()

    def validate(self, token_list, expect_table):
        result = self.parser.parse(TokenQueue(token_list))
        self.assertEqual(result, expect_table)

    def assert_exception(self, exception, token_list):
        token_list.append(tokens.EndOfFile())

        with self.assertRaises(exception) as context:
            self.parser.parse(TokenQueue(token_list))

        return context.exception

    def get_table_expression(self, entries, separators):
        return nodes.TableExpression(
            nodes.TableEntryList(entries, separators),
            self.left_brace,
            self.right_brace,
        )

    def test_parse_empty_table(self):
        self.validate(
            [self.left_brace, self.right_brace],
            self.get_table_expression([], []),
        )

    def test_parse_table_with_two_value_entries(self):
        comma = tokens.Comma()
        true_token = tokens.TrueKeyword()
        false_token = tokens.FalseKeyword()
        self.validate(
            [
                self.left_brace,
                true_token,
                comma,
                false_token,
                self.right_brace,
            ],
            self.get_table_expression(
                [
                    nodes.ValueEntry(nodes.TrueExpression(true_token)),
                    nodes.ValueEntry(nodes.FalseExpression(false_token)),
                ],
                [comma],
            ),
        )

    def test_parse_table_with_two_value_entries_and_semi_colon_separator(self):
        semi_colon = tokens.SemiColon()
        true_token = tokens.TrueKeyword()
        false_token = tokens.FalseKeyword()
        self.validate(
            [
                self.left_brace,
                true_token,
                semi_colon,
                false_token,
                self.right_brace,
            ],
            self.get_table_expression(
                [
                    nodes.ValueEntry(nodes.TrueExpression(true_token)),
                    nodes.ValueEntry(nodes.FalseExpression(false_token)),
                ],
                [semi_colon],
            ),
        )

    def test_parse_table_with_field_entry(self):
        assign = tokens.Assign()
        name = tokens.Identifier('hello')
        true_token = tokens.TrueKeyword()
        self.validate(
            [
                self.left_brace,
                name,
                assign,
                true_token,
                self.right_brace,
            ],
            self.get_table_expression(
                [
                    nodes.FieldEntry(name, nodes.TrueExpression(true_token), assign),
                ],
                [],
            ),
        )

    def test_parse_table_with_field_missing_value_raise_exception(self):
        assign = tokens.Assign()
        name = tokens.Identifier('hello')

        exception = self.assert_exception(
            exceptions.ExpressionExpectedException,
            [
                self.left_brace,
                name,
                assign,
                self.right_brace,
            ],
        )
        self.assertEqual(exception.get_previous_token(), assign)

    def test_parse_table_with_index_entry(self):
        left_bracket = tokens.OpeningSquareBracket()
        right_bracket = tokens.ClosingSquareBracket()
        assign = tokens.Assign()
        false_token = tokens.FalseKeyword()
        true_token = tokens.TrueKeyword()
        self.validate(
            [
                self.left_brace,
                left_bracket,
                false_token,
                right_bracket,
                assign,
                true_token,
                self.right_brace,
            ],
            self.get_table_expression(
                [
                    nodes.IndexEntry(
                        nodes.FalseExpression(false_token),
                        nodes.TrueExpression(true_token),
                        left_bracket,
                        right_bracket,
                        assign,
                    ),
                ],
                [],
            ),
        )

    def test_parse_table_with_missing_closing_brace_raise_expression_expected_exception(self):
        end_token = tokens.EndKeyword()

        exception = self.assert_exception(
            exceptions.ExpressionExpectedException,
            [
                self.left_brace,
                end_token,
            ],
        )
        self.assertEqual(exception.get_previous_token(), self.left_brace)

    def test_parse_table_with_missing_closing_brace_raise_exception(self):
        true_token = tokens.TrueKeyword()
        end_token = tokens.EndKeyword()

        exception = self.assert_exception(
            exceptions.SymbolExpectedException,
            [
                self.left_brace,
                true_token,
                end_token,
            ],
        )
        self.assertEqual(exception.get_expected_symbol(), '}')
        self.assertEqual(exception.get_next_token(), end_token)

    def test_parse_table_with_index_key_missing_key_expression_raise_exception(self):
        left_bracket = tokens.OpeningSquareBracket()
        right_bracket = tokens.ClosingSquareBracket()
        assign = tokens.Assign()
        true_token = tokens.TrueKeyword()

        exception = self.assert_exception(
            exceptions.ExpressionExpectedException,
            [
                self.left_brace,
                left_bracket,
                right_bracket,
                assign,
                true_token,
                self.right_brace,
            ],
        )
        self.assertEqual(exception.get_previous_token(), left_bracket)

    def test_parse_table_with_index_key_missing_value_expression_raise_exception(self):
        left_bracket = tokens.OpeningSquareBracket()
        right_bracket = tokens.ClosingSquareBracket()
        assign = tokens.Assign()
        false_token = tokens.FalseKeyword()

        exception = self.assert_exception(
            exceptions.ExpressionExpectedException,
            [
                self.left_brace,
                left_bracket,
                false_token,
                right_bracket,
                assign,
                self.right_brace,
            ],
        )
        self.assertEqual(exception.get_previous_token(), assign)

    def test_parse_table_with_index_key_missing_closing_bracket_raise_exception(self):
        left_bracket = tokens.OpeningSquareBracket()
        assign = tokens.Assign()
        false_token = tokens.FalseKeyword()
        true_token = tokens.TrueKeyword()

        exception = self.assert_exception(
            exceptions.SymbolExpectedException,
            [
                self.left_brace,
                left_bracket,
                false_token,
                assign,
                true_token,
                self.right_brace,
            ],
        )
        self.assertEqual(exception.get_expected_symbol(), ']')
        self.assertEqual(exception.get_next_token(), assign)

    def test_parse_table_with_index_key_missing_assign_raise_exception(self):
        left_bracket = tokens.OpeningSquareBracket()
        right_bracket = tokens.ClosingSquareBracket()
        false_token = tokens.FalseKeyword()
        true_token = tokens.TrueKeyword()

        exception = self.assert_exception(
            exceptions.SymbolExpectedException,
            [
                self.left_brace,
                left_bracket,
                false_token,
                right_bracket,
                true_token,
                self.right_brace,
            ],
        )
        self.assertEqual(exception.get_expected_symbol(), '=')
        self.assertEqual(exception.get_next_token(), true_token)
