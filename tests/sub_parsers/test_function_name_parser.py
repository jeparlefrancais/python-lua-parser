import unittest

from luaparser import exceptions, nodes, tokens, TokenQueue
from luaparser.sub_parsers import FunctionNameParser


class TestFunctionNameParser(unittest.TestCase):

    def setUp(self):
        self.parser = FunctionNameParser()
        self.function_keyword = tokens.FunctionKeyword()
        self.foo = tokens.Identifier('foo')

    def validate(self, token_list, expect_name):
        token_list.append(tokens.EndOfFile())
        token_queue = TokenQueue(token_list)
        token_queue._previous_token = self.function_keyword

        self.assertEqual(token_queue.get_previous_token(), self.function_keyword)

        token_queue.get_previous_token().get_symbol()
        result = self.parser.parse(token_queue)

        self.assertEqual(result, expect_name)

    def assert_exception(self, exception, token_list):
        token_list.append(tokens.EndOfFile())
        token_queue = TokenQueue(token_list)
        token_queue._previous_token = self.function_keyword

        with self.assertRaises(exception) as context:
            self.parser.parse(token_queue)

        return context.exception

    def test_parse_identifier(self):
        self.validate([self.foo], nodes.FunctionName([self.foo]))

    def test_parse_field_name(self):
        dot = tokens.Dot()
        bar = tokens.Identifier('bar')
        self.validate([self.foo, dot, bar], nodes.FunctionName([self.foo, bar], [dot]))

    def test_parse_name_with_method(self):
        colon = tokens.Colon()
        method = tokens.Identifier('method')
        self.validate([self.foo, colon, method], nodes.FunctionName([self.foo], [], method, colon))

    def test_parse_field_name_with_method(self):
        dot = tokens.Dot()
        bar = tokens.Identifier('bar')
        colon = tokens.Colon()
        method = tokens.Identifier('method')
        self.validate(
            [self.foo, dot, bar, colon, method],
            nodes.FunctionName([self.foo, bar], [dot], method, colon),
        )

    def test_parse_no_name_raise_exception(self):
        exception = self.assert_exception(exceptions.IdentifierExpectedException, [])
        self.assertEqual(exception.get_previous_token(), self.function_keyword)

    def test_parse_missing_method_name_raise_exception(self):
        colon = tokens.Colon()

        exception = self.assert_exception(
            exceptions.MethodNameExpectedException,
            [self.foo, colon],
        )
        self.assertEqual(exception.get_colon_token(), colon)
