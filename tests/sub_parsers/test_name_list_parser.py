import unittest
from unittest import mock

from luaparser import exceptions, nodes, tokens, TokenQueue
from tests.comparator import NodeComparator
from luaparser.exceptions import IdentifierExpectedException
from luaparser.sub_parsers import NameListParser


class TestExpressionParser(unittest.TestCase):

    def setUp(self):
        self.parser = NameListParser()
        self.identifier = tokens.Identifier('foo')

    def validate(self, token_list, expect_expression):
        end_of_file = tokens.EndOfFile()
        token_list.append(end_of_file)
        queue = TokenQueue(token_list)
        result = self.parser.parse(queue)

        if result != expect_expression:
            self.fail(self.compare(expect_expression, result))

        self.assertTrue(queue.pop_front(), end_of_file)

    def compare(self, expect, result):
        comparator = NodeComparator()
        comparator.compare(expect, result)
        return comparator.get_output()

    def assert_exception(self, exception, token_list):
        token_list.append(tokens.EndOfFile())

        with self.assertRaises(exception) as context:
            self.parser.parse(TokenQueue(token_list))

        return context.exception

    def test_parse_single_identifier(self):
        self.validate([self.identifier], nodes.NameList([self.identifier]))

    def test_parse_two_identifier(self):
        comma = tokens.Comma()
        second_identifier = tokens.Identifier('bar')
        self.validate(
            [self.identifier, comma, second_identifier],
            nodes.NameList([self.identifier, second_identifier], [comma]),
        )

    def test_parse_missing_identifier_after_comma(self):
        comma = tokens.Comma()
        exception = self.assert_exception(
            IdentifierExpectedException,
            [self.identifier, comma],
        )

        self.assertEqual(comma, exception.get_previous_token())
