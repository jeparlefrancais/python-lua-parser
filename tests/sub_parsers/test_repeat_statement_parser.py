import unittest

from luaparser import exceptions, nodes, tokens, TokenQueue
from luaparser.sub_parsers import RepeatStatementParser


class TestRepeatStatementParser(unittest.TestCase):

    def setUp(self):
        self.parser = RepeatStatementParser()
        self.repeat_token = tokens.RepeatKeyword()
        self.until_token = tokens.UntilKeyword()
        self.true_token = tokens.TrueKeyword()

    def validate(self, token_list, expect_statement):
        result = self.parser.parse(TokenQueue(token_list))
        self.assertEqual(result, expect_statement)

    def assert_exception(self, exception, token_list):
        token_list.append(tokens.EndOfFile())

        with self.assertRaises(exception) as context:
            self.parser.parse(TokenQueue(token_list))

        return context.exception

    def test_parse_empty_repeat_statement_with_true_condition(self):
        self.validate(
            [
                self.repeat_token,
                self.until_token,
                self.true_token,
            ],
            nodes.RepeatStatement(
                nodes.Block(),
                nodes.TrueExpression(self.true_token),
                self.repeat_token,
                self.until_token,
            ),
        )

    def test_parse_empty_repeat_statement_with_true_condition_and_semi_colon(self):
        semi_colon = tokens.SemiColon()
        self.validate(
            [
                self.repeat_token,
                self.until_token,
                self.true_token,
                semi_colon,
            ],
            nodes.RepeatStatement(
                nodes.Block(),
                nodes.TrueExpression(self.true_token),
                self.repeat_token,
                self.until_token,
                semi_colon,
            ),
        )

    def test_parse_repeat_statement_missing_condition(self):
        exception = self.assert_exception(
            exceptions.ExpressionExpectedException,
            [
                self.repeat_token,
                self.until_token,
            ],
        )
        self.assertEqual(exception.get_previous_token(), self.until_token)

    def test_parse__repeat_statement_missing_until_keyword(self):
        end_token = tokens.EndKeyword()

        exception = self.assert_exception(
            exceptions.SymbolExpectedException,
            [
                self.repeat_token,
                end_token,
            ],
        )
        self.assertEqual(exception.get_expected_symbol(), 'until')
        self.assertEqual(exception.get_next_token(), end_token)
