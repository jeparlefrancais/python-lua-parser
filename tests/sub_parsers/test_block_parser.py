import unittest

from luaparser import nodes, tokens, TokenQueue
from luaparser.sub_parsers import BlockParser


class TestBlockParser(unittest.TestCase):

    def setUp(self):
        self.parser = BlockParser()

    def validate(self, token_list, expect_block):
        token_list.append(tokens.EndOfFile())
        self._queue = TokenQueue(token_list)
        result = self.parser.parse(self._queue, tokens.EndOfFile)
        self.assertEqual(result, expect_block)

    def test_parse_end_of_file_return_empty_block(self):
        self.validate([], nodes.Block())

    def test_parse_one_statement(self):
        do_token = tokens.DoKeyword()
        end_token = tokens.EndKeyword()
        self.validate(
            [do_token, end_token],
            nodes.Block([
                nodes.DoStatement(nodes.Block(), do_token, end_token),
            ]),
        )

    def test_parse_last_statement_leave_next_tokens(self):
        return_token = tokens.ReturnKeyword()
        self.validate(
            [return_token, return_token],
            nodes.Block([
                nodes.ReturnStatement(return_token, nodes.ExpressionList()),
            ]),
        )
        self.assertEqual(
            self._queue.get_tokens(),
            [return_token, tokens.EndOfFile()],
        )
