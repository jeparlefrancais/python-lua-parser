import unittest

from luaparser import exceptions, nodes, tokens, TokenQueue
from luaparser.sub_parsers import FunctionExpressionParser


class TestFunctionExpressionParser(unittest.TestCase):

    def setUp(self):
        self.parser = FunctionExpressionParser()
        self.function_token = tokens.FunctionKeyword()
        self.opening_parenthese_token = tokens.OpeningParenthese()
        self.closing_parenthese_token = tokens.ClosingParenthese()
        self.end_token = tokens.EndKeyword()
        self.parameter = tokens.Identifier('param')

    def validate(self, token_list, expect_expression):
        token_list.append(tokens.EndOfFile())
        result = self.parser.parse(TokenQueue(token_list))
        self.assertEqual(result, expect_expression)

    def assert_exception(self, exception, token_list):
        token_list.append(tokens.EndOfFile())

        with self.assertRaises(exception) as context:
            self.parser.parse(TokenQueue(token_list))

        return context.exception

    def test_parse_function_without_parameters(self):
        self.validate(
            [
                self.function_token,
                self.opening_parenthese_token,
                self.closing_parenthese_token,
                self.end_token,
            ],
            nodes.FunctionExpression(
                nodes.Block(),
                [],
                self.function_token,
                self.end_token,
                self.opening_parenthese_token,
                self.closing_parenthese_token,
            ),
        )

    def test_parse_function_without_one_parameter(self):
        self.validate(
            [
                self.function_token,
                self.opening_parenthese_token,
                self.parameter,
                self.closing_parenthese_token,
                self.end_token,
            ],
            nodes.FunctionExpression(
                nodes.Block(),
                [self.parameter],
                self.function_token,
                self.end_token,
                self.opening_parenthese_token,
                self.closing_parenthese_token,
            ),
        )

    def test_parse_function_with_only_variable_arguments(self):
        var_args = tokens.VarArgs()
        self.validate(
            [
                self.function_token,
                self.opening_parenthese_token,
                var_args,
                self.closing_parenthese_token,
                self.end_token,
            ],
            nodes.FunctionExpression(
                nodes.Block(),
                [],
                self.function_token,
                self.end_token,
                self.opening_parenthese_token,
                self.closing_parenthese_token,
                [],
                var_args,
            ),
        )

    def test_parse_function_without_two_parameters(self):
        first_parameter = tokens.Identifier('foo')
        second_parameter = tokens.Identifier('bar')
        comma = tokens.Comma()
        self.validate(
            [
                self.function_token,
                self.opening_parenthese_token,
                first_parameter,
                comma,
                second_parameter,
                self.closing_parenthese_token,
                self.end_token,
            ],
            nodes.FunctionExpression(
                nodes.Block(),
                [first_parameter, second_parameter],
                self.function_token,
                self.end_token,
                self.opening_parenthese_token,
                self.closing_parenthese_token,
                [comma],
            ),
        )

    def test_parse_function_without_named_parameter_and_variable_arguments(self):
        var_args = tokens.VarArgs()
        comma = tokens.Comma()
        self.validate(
            [
                self.function_token,
                self.opening_parenthese_token,
                self.parameter,
                comma,
                var_args,
                self.closing_parenthese_token,
                self.end_token,
            ],
            nodes.FunctionExpression(
                nodes.Block(),
                [self.parameter],
                self.function_token,
                self.end_token,
                self.opening_parenthese_token,
                self.closing_parenthese_token,
                [comma],
                var_args,
            ),
        )

    def test_parse_function_missing_opening_parenthese_raise_exception(self):
        exception = self.assert_exception(
            exceptions.SymbolExpectedException,
            [
                self.function_token,
                self.closing_parenthese_token,
                self.end_token,
            ],
        )
        self.assertEqual(exception.get_expected_symbol(), '(')
        self.assertEqual(exception.get_next_token(), self.closing_parenthese_token)

    def test_parse_function_missing_closing_parenthese_raise_exception(self):
        exception = self.assert_exception(
            exceptions.SymbolExpectedException,
            [
                self.function_token,
                self.opening_parenthese_token,
                self.end_token,
            ],
        )
        self.assertEqual(exception.get_expected_symbol(), ')')
        self.assertEqual(exception.get_next_token(), self.end_token)

    def test_parse_function_invalid_parameter_raise_exception(self):
        comma = tokens.Comma()
        true_token = tokens.TrueKeyword()

        exception = self.assert_exception(
            exceptions.InvalidFunctionParameterException,
            [
                self.function_token,
                self.opening_parenthese_token,
                self.parameter,
                comma,
                true_token,
                self.closing_parenthese_token,
                self.end_token,
            ],
        )
        self.assertEqual(exception.get_previous_token(), comma)

    def test_parse_function_missing_end_raise_exception(self):
        true_token = tokens.TrueKeyword()

        exception = self.assert_exception(
            exceptions.SymbolExpectedException,
            [
                self.function_token,
                self.opening_parenthese_token,
                self.closing_parenthese_token,
                true_token,
            ],
        )
        self.assertEqual(exception.get_expected_symbol(), 'end')
        self.assertEqual(exception.get_next_token(), true_token)
