import unittest

from luaparser import nodes, tokens, TokenQueue
from luaparser.exceptions import ExpressionExpectedException
from luaparser.sub_parsers import ExpressionListParser


class TestExpressionListParser(unittest.TestCase):

    def setUp(self):
        self.parser = ExpressionListParser()

    def validate(self, token_list, expect_list):
        result = self.parser.parse(TokenQueue(token_list))
        self.assertEqual(result, expect_list)

    def assert_exception(self, exception, token_list):
        with self.assertRaises(exception) as context:
            self.parser.parse(TokenQueue(token_list))

        return context.exception

    def test_parse_no_tokens_return_empty_list(self):
        self.validate([], nodes.ExpressionList())

    def test_parse_end_keyword_return_empty_list(self):
        self.validate([tokens.EndKeyword()], nodes.ExpressionList())

    def test_parse_one_expression(self):
        true_token = tokens.TrueKeyword()
        self.validate(
            [true_token],
            nodes.ExpressionList(
                [nodes.TrueExpression(true_token)],
                [],
            ),
        )

    def test_parse_two_expressions(self):
        true_token = tokens.TrueKeyword()
        false_token = tokens.FalseKeyword()
        comma = tokens.Comma()
        self.validate(
            [true_token, comma, false_token],
            nodes.ExpressionList(
                [
                    nodes.TrueExpression(true_token),
                    nodes.FalseExpression(false_token),
                ],
                [comma],
            ),
        )

    def test_parse_expression_list_missing_last_expression_has_previous_comma(self):
        comma = tokens.Comma()
        exception = self.assert_exception(
            ExpressionExpectedException,
            [tokens.TrueKeyword(), comma],
        )
        self.assertEqual(exception.get_previous_token(), comma)
