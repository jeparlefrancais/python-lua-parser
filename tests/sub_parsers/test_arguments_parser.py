import unittest

from luaparser import exceptions, nodes, tokens, TokenQueue
from luaparser.sub_parsers import ArgumentsParser


class TestArgumentsParser(unittest.TestCase):

    def setUp(self):
        self.parser = ArgumentsParser()

    def validate(self, token_list, expect):
        result = self.parser.parse(TokenQueue(token_list))
        self.assertEqual(result, expect)

    def assert_exception(self, exception, token_list):
        with self.assertRaises(exception) as context:
            self.parser.parse(TokenQueue(token_list))

        return context.exception

    def test_parse_string_argument(self):
        string_token = tokens.String('foo', True)
        self.validate(
            [string_token],
            nodes.StringArgument(nodes.StringExpression(string_token)),
        )

    def test_parse_long_string_argument(self):
        string_token = tokens.LongString('foo', 0)
        self.validate(
            [string_token],
            nodes.StringArgument(nodes.StringExpression(string_token)),
        )

    def test_parse_empty_table_argument(self):
        left_brace = tokens.OpeningBrace()
        right_brace = tokens.ClosingBrace()
        self.validate(
            [left_brace, right_brace],
            nodes.TableArgument(
                nodes.TableExpression(nodes.TableEntryList(), left_brace, right_brace)
            ),
        )

    def test_parse_empty_tuple_argument(self):
        left_parenthese = tokens.OpeningParenthese()
        right_parenthese = tokens.ClosingParenthese()
        self.validate(
            [left_parenthese, right_parenthese],
            nodes.TupleArguments(
                nodes.ExpressionList(),
                left_parenthese,
                right_parenthese,
            ),
        )

    def test_parse_tuple_with_one_argument(self):
        left_parenthese = tokens.OpeningParenthese()
        right_parenthese = tokens.ClosingParenthese()
        variable = tokens.Identifier('foo')
        self.validate(
            [left_parenthese, variable, right_parenthese],
            nodes.TupleArguments(
                nodes.ExpressionList([nodes.VariableExpression(variable)]),
                left_parenthese,
                right_parenthese,
            ),
        )

    def test_parse_tuple_arguments_without_closed_parenthese_raise_exception_with_next_token(self):
        exception = self.assert_exception(
            exceptions.SymbolExpectedException,
            [tokens.OpeningParenthese(), tokens.EndOfFile()],
        )
        self.assertEqual(exception.get_next_token(), tokens.EndOfFile())

    def test_parse_tuple_arguments_without_closed_parenthese_raise_exception_with_expected_symbol(
        self
    ):
        exception = self.assert_exception(
            exceptions.SymbolExpectedException,
            [tokens.OpeningParenthese(), tokens.EndOfFile()],
        )
        self.assertEqual(exception.get_expected_symbol(), ')')

    def test_parse_unexpected_symbol_raise_exception(self):
        exception = self.assert_exception(
            exceptions.UnexpectedTokenException,
            [tokens.EndOfFile()],
        )
        self.assertEqual(exception.get_token(), tokens.EndOfFile())
