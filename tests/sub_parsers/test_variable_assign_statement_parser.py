import unittest

from luaparser import exceptions, nodes, tokens, TokenQueue
from luaparser.sub_parsers import VariableAssignStatementParser


class TestVariableStatementParser(unittest.TestCase):

    def setUp(self):
        self.parser = VariableAssignStatementParser()
        self.identifier = tokens.Identifier('foo')
        self.variable_expression = nodes.VariableExpression(self.identifier)
        self.assign_token = tokens.Assign()
        self.true_token = tokens.TrueKeyword()
        self.true_expression = nodes.TrueExpression(self.true_token)

    def validate(self, token_list, expect_statement):
        result = self.parser.try_parse(TokenQueue(token_list))
        self.assertEqual(result, expect_statement)

    def assert_exception(self, exception, token_list):
        token_list.append(tokens.EndOfFile())

        with self.assertRaises(exception) as context:
            self.parser.try_parse(TokenQueue(token_list))

        return context.exception

    def test_parse_single_identifier_with_true_expression(self):
        self.validate(
            [self.identifier, self.assign_token, self.true_token],
            nodes.VariableAssignStatement(
                nodes.VariableList([self.variable_expression]),
                nodes.ExpressionList([self.true_expression]),
                self.assign_token,
                None,
            ),
        )

    def test_parse_single_index_variable_with_true_expression(self):
        left_bracket = tokens.OpeningSquareBracket()
        right_bracket = tokens.ClosingSquareBracket()
        string_token = tokens.String('bar', True)

        self.validate(
            [
                self.identifier,
                left_bracket,
                string_token,
                right_bracket,
                self.assign_token,
                self.true_token,
            ],
            nodes.VariableAssignStatement(
                nodes.VariableList([
                    nodes.IndexExpression(
                        self.variable_expression,
                        nodes.StringExpression(string_token),
                        left_bracket,
                        right_bracket,
                    )
                ]),
                nodes.ExpressionList([self.true_expression]),
                self.assign_token,
                None,
            ),
        )

    def test_parse_single_field_variable_with_true_expression(self):
        dot_token = tokens.Dot()
        field_token = tokens.Identifier('bar')

        self.validate(
            [
                self.identifier,
                dot_token,
                field_token,
                self.assign_token,
                self.true_token,
            ],
            nodes.VariableAssignStatement(
                nodes.VariableList([
                    nodes.FieldExpression(
                        self.variable_expression,
                        field_token,
                        dot_token,
                    )
                ]),
                nodes.ExpressionList([self.true_expression]),
                self.assign_token,
                None,
            ),
        )

    def test_parse_field_and_index_variable_with_true_expression(self):
        dot_token = tokens.Dot()
        field_token = tokens.Identifier('bar')
        left_bracket = tokens.OpeningSquareBracket()
        right_bracket = tokens.ClosingSquareBracket()
        string_token = tokens.String('bar', True)

        self.validate(
            [
                self.identifier,
                dot_token,
                field_token,
                left_bracket,
                string_token,
                right_bracket,
                self.assign_token,
                self.true_token,
            ],
            nodes.VariableAssignStatement(
                nodes.VariableList([
                    nodes.IndexExpression(
                        nodes.FieldExpression(
                            self.variable_expression,
                            field_token,
                            dot_token,
                        ),
                        nodes.StringExpression(string_token),
                        left_bracket,
                        right_bracket,
                    )
                ]),
                nodes.ExpressionList([self.true_expression]),
                self.assign_token,
                None,
            ),
        )

    def test_parse_variable_raise_assign_expected_exception(self):
        end_token = tokens.EndKeyword()

        exception = self.assert_exception(
            exceptions.SymbolExpectedException,
            [
                self.identifier,
                end_token,
            ],
        )
        self.assertEqual(exception.get_expected_symbol(), '=')
        self.assertEqual(exception.get_next_token(), end_token)

    def test_parse_variable_assign_missing_expression_raise_exception(self):
        exception = self.assert_exception(
            exceptions.ExpressionExpectedException,
            [
                self.identifier,
                self.assign_token,
            ],
        )
        self.assertEqual(exception.get_previous_token(), self.assign_token)
