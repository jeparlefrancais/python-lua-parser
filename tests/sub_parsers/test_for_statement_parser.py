import unittest

from luaparser import exceptions, nodes, tokens, TokenQueue
from luaparser.sub_parsers import ForStatementParser


class TestForStatementParser(unittest.TestCase):

    def setUp(self):
        self.parser = ForStatementParser()
        self.for_token = tokens.ForKeyword()
        self.do_token = tokens.DoKeyword()
        self.end_token = tokens.EndKeyword()
        self.identifier_token = tokens.Identifier('i')
        self.assign_token = tokens.Assign()
        self.start_number = tokens.Number(1, '1')
        self.end_number = tokens.Number(10, '10')
        self.start_end_comma = tokens.Comma()
        self.in_token = tokens.InKeyword()

    def validate(self, token_list, expect_statement):
        token_list.append(tokens.EndOfFile())
        result = self.parser.parse(TokenQueue(token_list))
        self.assertEqual(result, expect_statement)

    def assert_exception(self, exception, token_list):
        token_list.append(tokens.EndOfFile())

        with self.assertRaises(exception) as context:
            self.parser.parse(TokenQueue(token_list))

        return context.exception

    def test_parse_empty_numeric_for_without_step_expression(self):
        self.validate(
            [
                self.for_token,
                self.identifier_token,
                self.assign_token,
                self.start_number,
                self.start_end_comma,
                self.end_number,
                self.do_token,
                self.end_token,
            ],
            nodes.NumericForStatement(
                self.identifier_token,
                nodes.Block(),
                nodes.NumberExpression(self.start_number),
                nodes.NumberExpression(self.end_number),
                None,
                self.for_token,
                self.do_token,
                self.end_token,
                self.assign_token,
                self.start_end_comma,
                None,
            ),
        )

    def test_parse_empty_numeric_for_with_step_expression(self):
        step = tokens.Number(2, '2')
        end_step_comma = tokens.Comma()

        self.validate(
            [
                self.for_token,
                self.identifier_token,
                self.assign_token,
                self.start_number,
                self.start_end_comma,
                self.end_number,
                end_step_comma,
                step,
                self.do_token,
                self.end_token,
            ],
            nodes.NumericForStatement(
                self.identifier_token,
                nodes.Block(),
                nodes.NumberExpression(self.start_number),
                nodes.NumberExpression(self.end_number),
                nodes.NumberExpression(step),
                self.for_token,
                self.do_token,
                self.end_token,
                self.assign_token,
                self.start_end_comma,
                end_step_comma,
            ),
        )

    def test_parse_numeric_for_without_start_raise_exception(self):
        exception = self.assert_exception(
            exceptions.ExpressionExpectedException,
            [
                self.for_token,
                self.identifier_token,
                self.assign_token,
                self.start_end_comma,
                self.end_number,
                self.do_token,
                self.end_token,
            ],
        )
        self.assertEqual(exception.get_previous_token(), self.assign_token)

    def test_parse_numeric_for_without_first_comma_raise_exception(self):
        exception = self.assert_exception(
            exceptions.SymbolExpectedException,
            [
                self.for_token,
                self.identifier_token,
                self.assign_token,
                self.start_number,
                self.end_number,
                self.do_token,
                self.end_token,
            ],
        )
        self.assertEqual(exception.get_expected_symbol(), ',')
        self.assertEqual(exception.get_next_token(), self.end_number)

    def test_parse_numeric_for_without_end_number_raise_exception(self):
        exception = self.assert_exception(
            exceptions.ExpressionExpectedException,
            [
                self.for_token,
                self.identifier_token,
                self.assign_token,
                self.start_number,
                self.start_end_comma,
                self.do_token,
                self.end_token,
            ],
        )
        self.assertEqual(exception.get_previous_token(), self.start_end_comma)

    def test_parse_numeric_for_without_step_number_raise_exception(self):
        end_step_comma = tokens.Comma()

        exception = self.assert_exception(
            exceptions.ExpressionExpectedException,
            [
                self.for_token,
                self.identifier_token,
                self.assign_token,
                self.start_number,
                self.start_end_comma,
                self.end_number,
                end_step_comma,
                self.do_token,
                self.end_token,
            ],
        )
        self.assertEqual(exception.get_previous_token(), end_step_comma)

    def test_parse_empty_generic_for(self):
        key_identifier = tokens.Identifier('k')
        key_value_comma = tokens.Comma()
        value_identifier = tokens.Identifier('v')
        iterator = tokens.TrueKeyword()

        self.validate(
            [
                self.for_token,
                key_identifier,
                key_value_comma,
                value_identifier,
                self.in_token,
                iterator,
                self.do_token,
                self.end_token,
            ],
            nodes.GenericForStatement(
                nodes.NameList(
                    [key_identifier, value_identifier],
                    [key_value_comma],
                ),
                nodes.ExpressionList([nodes.TrueExpression(iterator)]),
                nodes.Block(),
                self.for_token,
                self.in_token,
                self.do_token,
                self.end_token,
            ),
        )

    def test_parse_generic_for_missing_identifier_raise_exception(self):
        iterator = tokens.TrueKeyword()

        exception = self.assert_exception(
            exceptions.IdentifierExpectedException,
            [
                self.for_token,
                self.in_token,
                iterator,
                self.do_token,
                self.end_token,
            ],
        )
        self.assertEqual(exception.get_previous_token(), self.for_token)

    def test_parse_generic_for_missing_in_raise_exception(self):
        key_identifier = tokens.Identifier('k')
        iterator = tokens.TrueKeyword()

        exception = self.assert_exception(
            exceptions.SymbolExpectedException,
            [
                self.for_token,
                key_identifier,
                iterator,
                self.do_token,
                self.end_token,
            ],
        )
        self.assertEqual(exception.get_expected_symbol(), 'in')
        self.assertEqual(exception.get_next_token(), iterator)

    def test_parse_generic_for_missing_iterator_expression_raise_exception(self):
        key_identifier = tokens.Identifier('k')
        iterator = tokens.TrueKeyword()

        exception = self.assert_exception(
            exceptions.ExpressionExpectedException,
            [
                self.for_token,
                key_identifier,
                self.in_token,
                self.do_token,
                self.end_token,
            ],
        )
        self.assertEqual(exception.get_previous_token(), self.in_token)

    def test_parse_for_missing_do_raise_exception(self):
        key_identifier = tokens.Identifier('k')
        iterator = tokens.TrueKeyword()

        exception = self.assert_exception(
            exceptions.SymbolExpectedException,
            [
                self.for_token,
                key_identifier,
                self.in_token,
                iterator,
                self.end_token,
            ],
        )
        self.assertEqual(exception.get_expected_symbol(), 'do')
        self.assertEqual(exception.get_next_token(), self.end_token)

    def test_parse_for_missing_end_raise_exception(self):
        key_identifier = tokens.Identifier('k')
        iterator = tokens.TrueKeyword()
        random_token = tokens.FalseKeyword()

        exception = self.assert_exception(
            exceptions.SymbolExpectedException,
            [
                self.for_token,
                key_identifier,
                self.in_token,
                iterator,
                self.do_token,
                random_token,
            ],
        )
        self.assertEqual(exception.get_expected_symbol(), 'end')
        self.assertEqual(exception.get_next_token(), random_token)
