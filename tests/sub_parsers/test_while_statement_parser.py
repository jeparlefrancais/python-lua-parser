import unittest

from luaparser import exceptions, nodes, tokens, TokenQueue
from luaparser.sub_parsers import WhileStatementParser


class TestWhileStatementParser(unittest.TestCase):

    def setUp(self):
        self.parser = WhileStatementParser()
        self.while_token = tokens.WhileKeyword()
        self.do_token = tokens.DoKeyword()
        self.end_token = tokens.EndKeyword()
        self.true_token = tokens.TrueKeyword()

    def validate(self, token_list, expect_statement):
        token_list.append(tokens.EndOfFile())
        result = self.parser.parse(TokenQueue(token_list))
        self.assertEqual(result, expect_statement)

    def assert_exception(self, exception, token_list):
        token_list.append(tokens.EndOfFile())

        with self.assertRaises(exception) as context:
            self.parser.parse(TokenQueue(token_list))

        return context.exception

    def test_parse_empty_while(self):
        self.validate(
            [
                self.while_token,
                self.true_token,
                self.do_token,
                self.end_token,
            ],
            nodes.WhileStatement(
                nodes.TrueExpression(self.true_token),
                nodes.Block(),
                self.while_token,
                self.do_token,
                self.end_token,
            ),
        )

    def test_parse_while_missing_condition_raise_exception(self):
        exception = self.assert_exception(
            exceptions.ExpressionExpectedException,
            [
                self.while_token,
                self.do_token,
                self.end_token,
            ],
        )
        self.assertEqual(exception.get_previous_token(), self.while_token)

    def test_parse_while_missing_do_raise_exception(self):
        exception = self.assert_exception(
            exceptions.SymbolExpectedException,
            [
                self.while_token,
                self.true_token,
                self.end_token,
            ],
        )
        self.assertEqual(exception.get_expected_symbol(), 'do')
        self.assertEqual(exception.get_next_token(), self.end_token)

    def test_parse_while_missing_end_raise_exception(self):
        false_keyword = tokens.FalseKeyword()

        exception = self.assert_exception(
            exceptions.SymbolExpectedException,
            [
                self.while_token,
                self.true_token,
                self.do_token,
                false_keyword,
            ],
        )
        self.assertEqual(exception.get_expected_symbol(), 'end')
        self.assertEqual(exception.get_next_token(), false_keyword)
