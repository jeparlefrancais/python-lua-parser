import unittest

from luaparser import exceptions, nodes, tokens, TokenQueue
from luaparser.sub_parsers import IfStatementParser


class TestIfStatementParser(unittest.TestCase):

    def setUp(self):
        self.parser = IfStatementParser()
        self.end_of_file = tokens.EndOfFile()
        self.if_token = tokens.IfKeyword()
        self.then_token = tokens.ThenKeyword()
        self.end_token = tokens.EndKeyword()
        self.true_token = tokens.TrueKeyword()

    def validate(self, token_list, expect_statement):
        token_list.append(self.end_of_file)
        result = self.parser.parse(TokenQueue(token_list))
        self.assertEqual(result, expect_statement)

    def assert_exception(self, exception, token_list):
        token_list.append(self.end_of_file)

        with self.assertRaises(exception) as context:
            self.parser.parse(TokenQueue(token_list))

        return context.exception

    def test_parse_empty_if_statement_with_true_condition(self):
        self.validate(
            [
                self.if_token,
                self.true_token,
                self.then_token,
                self.end_token,
            ],
            nodes.IfStatement(
                nodes.Block(),
                nodes.TrueExpression(self.true_token),
                [],
                None,
                self.if_token,
                self.then_token,
                self.end_token,
                None,
            ),
        )

    def test_parse_empty_repeat_statement_with_true_condition_and_semi_colon(self):
        semi_colon = tokens.SemiColon()
        self.validate(
            [
                self.if_token,
                self.true_token,
                self.then_token,
                self.end_token,
                semi_colon,
            ],
            nodes.IfStatement(
                nodes.Block(),
                nodes.TrueExpression(self.true_token),
                [],
                None,
                self.if_token,
                self.then_token,
                self.end_token,
                semi_colon,
            ),
        )

    def test_parse_empty_if_statement_with_empty_else(self):
        else_token = tokens.ElseKeyword()
        self.validate(
            [
                self.if_token,
                self.true_token,
                self.then_token,
                else_token,
                self.end_token,
            ],
            nodes.IfStatement(
                nodes.Block(),
                nodes.TrueExpression(self.true_token),
                [],
                nodes.ElseBranch(nodes.Block(), else_token),
                self.if_token,
                self.then_token,
                self.end_token,
                None,
            ),
        )

    def test_parse_empty_if_statement_with_empty_elseif(self):
        elseif_token = tokens.ElseifKeyword()
        self.validate(
            [
                self.if_token,
                self.true_token,
                self.then_token,
                elseif_token,
                self.true_token,
                self.then_token,
                self.end_token,
            ],
            nodes.IfStatement(
                nodes.Block(),
                nodes.TrueExpression(self.true_token),
                [
                    nodes.ElseifBranch(
                        nodes.Block(),
                        nodes.TrueExpression(self.true_token),
                        elseif_token,
                        self.then_token,
                    )
                ],
                None,
                self.if_token,
                self.then_token,
                self.end_token,
                None,
            ),
        )

    def test_parse_if_missing_condition(self):
        exception = self.assert_exception(
            exceptions.ExpressionExpectedException,
            [
                self.if_token,
                self.then_token,
                self.end_token,
            ],
        )
        self.assertEqual(exception.get_previous_token(), self.if_token)

    def test_parse_if_missing_then_token(self):
        exception = self.assert_exception(
            exceptions.SymbolExpectedException,
            [
                self.if_token,
                self.true_token,
                self.end_token,
            ],
        )
        self.assertEqual(exception.get_expected_symbol(), 'then')
        self.assertEqual(exception.get_next_token(), self.end_token)

    def test_parse_elseif_missing_condition(self):
        elseif_token = tokens.ElseifKeyword()

        exception = self.assert_exception(
            exceptions.ExpressionExpectedException,
            [
                self.if_token,
                self.true_token,
                self.then_token,
                elseif_token,
                self.then_token,
                self.end_token,
            ],
        )
        self.assertEqual(exception.get_previous_token(), elseif_token)

    def test_parse_elseif_missing_then_token(self):
        elseif_token = tokens.ElseifKeyword()

        exception = self.assert_exception(
            exceptions.SymbolExpectedException,
            [
                self.if_token,
                self.true_token,
                self.then_token,
                elseif_token,
                self.true_token,
                self.end_token,
            ],
        )
        self.assertEqual(exception.get_expected_symbol(), 'then')
        self.assertEqual(exception.get_next_token(), self.end_token)

    def test_parse_if_missing_end_token(self):
        exception = self.assert_exception(
            exceptions.SymbolExpectedException,
            [
                self.if_token,
                self.true_token,
                self.then_token,
            ],
        )
        self.assertEqual(exception.get_expected_symbol(), 'end')
        self.assertEqual(exception.get_next_token(), self.end_of_file)
