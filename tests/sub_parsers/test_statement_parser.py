import unittest

from luaparser import exceptions, nodes, tokens, TokenQueue
from tests.comparator import NodeComparator
from luaparser.sub_parsers import StatementParser


class TestStatementParser(unittest.TestCase):

    def setUp(self):
        self.parser = StatementParser()

    def validate(self, token_list, expect_statement):
        result = self.parser.try_parse(TokenQueue(token_list))

        if result != expect_statement:
            self.fail(self.compare(expect_statement, result))

    def compare(self, expect, result):
        comparator = NodeComparator()
        comparator.compare(expect, result)
        return comparator.get_output()

    def assert_exception(self, exception, token_list):
        with self.assertRaises(exception) as context:
            self.parser.try_parse(TokenQueue(token_list))

        return context.exception

    def test_try_parse_return_none_if_only_end_of_file_token(self):
        result = self.parser.try_parse(TokenQueue([tokens.EndOfFile()]))
        self.assertIsNone(result)

    def test_try_parse_break_statement(self):
        break_token = tokens.BreakKeyword()

        self.validate(
            [break_token],
            nodes.BreakStatement(break_token),
        )

    def test_try_parse_empty_do_statement(self):
        self.validate(
            [tokens.DoKeyword(), tokens.EndKeyword()],
            nodes.DoStatement(nodes.Block(), tokens.DoKeyword(), tokens.EndKeyword()),
        )

    def test_try_parse_empty_do_statement_with_semi_colon(self):
        self.validate(
            [
                tokens.DoKeyword(),
                tokens.EndKeyword(),
                tokens.SemiColon(),
            ],
            nodes.DoStatement(
                nodes.Block(),
                tokens.DoKeyword(),
                tokens.EndKeyword(),
                tokens.SemiColon(),
            ),
        )

    def test_try_parse_empty_while_statement(self):
        self.validate(
            [
                tokens.WhileKeyword(),
                tokens.TrueKeyword(),
                tokens.DoKeyword(),
                tokens.EndKeyword(),
            ],
            nodes.WhileStatement(
                nodes.TrueExpression(tokens.TrueKeyword()),
                nodes.Block(),
                tokens.WhileKeyword(),
                tokens.DoKeyword(),
                tokens.EndKeyword(),
            ),
        )

    def test_try_parse_empty_while_statement_with_semi_colon(self):
        self.validate(
            [
                tokens.WhileKeyword(),
                tokens.TrueKeyword(),
                tokens.DoKeyword(),
                tokens.EndKeyword(),
                tokens.SemiColon(),
            ],
            nodes.WhileStatement(
                nodes.TrueExpression(tokens.TrueKeyword()),
                nodes.Block(),
                tokens.WhileKeyword(),
                tokens.DoKeyword(),
                tokens.EndKeyword(),
                tokens.SemiColon(),
            ),
        )

    def test_try_parse_empty_repeat_statement(self):
        self.validate(
            [
                tokens.RepeatKeyword(),
                tokens.UntilKeyword(),
                tokens.TrueKeyword(),
            ],
            nodes.RepeatStatement(
                nodes.Block(),
                nodes.TrueExpression(tokens.TrueKeyword()),
                tokens.RepeatKeyword(),
                tokens.UntilKeyword(),
            ),
        )

    def test_try_parse_empty_repeat_statement_with_semi_colon(self):
        self.validate(
            [
                tokens.RepeatKeyword(),
                tokens.UntilKeyword(),
                tokens.TrueKeyword(),
                tokens.SemiColon(),
            ],
            nodes.RepeatStatement(
                nodes.Block(),
                nodes.TrueExpression(tokens.TrueKeyword()),
                tokens.RepeatKeyword(),
                tokens.UntilKeyword(),
                tokens.SemiColon(),
            ),
        )

    def test_try_parse_function_call_statement(self):
        function_name = tokens.Identifier('foo')
        left_parenthese = tokens.OpeningParenthese()
        right_parenthese = tokens.ClosingParenthese()
        self.validate(
            [
                function_name,
                left_parenthese,
                right_parenthese,
            ],
            nodes.FunctionCallStatement(
                nodes.CallExpression(
                    nodes.VariableExpression(function_name),
                    nodes.TupleArguments(
                        nodes.ExpressionList(),
                        left_parenthese,
                        right_parenthese,
                    )
                )
            ),
        )

    def test_parse_variable_assign_with_single_identifier_and_true_expression(self):
        identifier = tokens.Identifier('foo')
        assign_token = tokens.Assign()
        true_token = tokens.TrueKeyword()
        self.validate(
            [identifier, assign_token, true_token],
            nodes.VariableAssignStatement(
                nodes.VariableList([nodes.VariableExpression(identifier)]),
                nodes.ExpressionList([nodes.TrueExpression(true_token)]),
                assign_token,
            ),
        )

    def test_parse_variable_assign_with_index_variable_and_true_expression(self):
        identifier = tokens.Identifier('foo')
        assign_token = tokens.Assign()
        true_token = tokens.TrueKeyword()
        left_bracket = tokens.OpeningSquareBracket()
        right_bracket = tokens.ClosingSquareBracket()
        string_token = tokens.String('bar', True)
        self.validate(
            [
                identifier,
                left_bracket,
                string_token,
                right_bracket,
                assign_token,
                true_token,
            ],
            nodes.VariableAssignStatement(
                nodes.VariableList([
                    nodes.IndexExpression(
                        nodes.VariableExpression(identifier),
                        nodes.StringExpression(string_token),
                        left_bracket,
                        right_bracket,
                    )
                ]),
                nodes.ExpressionList([nodes.TrueExpression(true_token)]),
                assign_token,
            ),
        )

    def test_parse_field_and_index_variable_with_true_expression(self):
        identifier = tokens.Identifier('foo')
        assign_token = tokens.Assign()
        true_token = tokens.TrueKeyword()
        dot_token = tokens.Dot()
        field_token = tokens.Identifier('bar')
        left_bracket = tokens.OpeningSquareBracket()
        right_bracket = tokens.ClosingSquareBracket()
        string_token = tokens.String('bar', True)

        self.validate(
            [
                identifier,
                dot_token,
                field_token,
                left_bracket,
                string_token,
                right_bracket,
                assign_token,
                true_token,
            ],
            nodes.VariableAssignStatement(
                nodes.VariableList([
                    nodes.IndexExpression(
                        nodes.FieldExpression(
                            nodes.VariableExpression(identifier),
                            field_token,
                            dot_token,
                        ),
                        nodes.StringExpression(string_token),
                        left_bracket,
                        right_bracket,
                    )
                ]),
                nodes.ExpressionList([nodes.TrueExpression(true_token)]),
                assign_token,
            ),
        )

    def test_parse_empty_if_statement(self):
        if_token = tokens.IfKeyword()
        true_token = tokens.TrueKeyword()
        then_token = tokens.ThenKeyword()
        end_token = tokens.EndKeyword()

        self.validate(
            [
                if_token,
                true_token,
                then_token,
                end_token,
            ],
            nodes.IfStatement(
                nodes.Block(),
                nodes.TrueExpression(true_token),
                [],
                None,
                if_token,
                then_token,
                end_token,
            ),
        )

    def test_parse_empty_generic_for_statent(self):
        for_token = tokens.ForKeyword()
        key_identifier = tokens.Identifier('k')
        iterator = tokens.TrueKeyword()
        in_token = tokens.InKeyword()
        do_token = tokens.DoKeyword()
        end_token = tokens.EndKeyword()

        self.validate(
            [
                for_token,
                key_identifier,
                in_token,
                iterator,
                do_token,
                end_token,
            ],
            nodes.GenericForStatement(
                nodes.NameList([key_identifier]),
                nodes.ExpressionList([nodes.TrueExpression(iterator)]),
                nodes.Block(),
                for_token,
                in_token,
                do_token,
                end_token,
            ),
        )

    def test_parse_local_variable_assign_statement(self):
        local_token = tokens.LocalKeyword()
        identifier = tokens.Identifier('foo')

        self.validate(
            [local_token, identifier],
            nodes.LocalVariableAssignStatement(
                nodes.NameList([identifier]),
                None,
                local_token,
                None,
            ),
        )

    def test_parse_local_function_assign_statement(self):
        local_token = tokens.LocalKeyword()
        function_token = tokens.FunctionKeyword()
        identifier = tokens.Identifier('foo')
        left_parenthese = tokens.OpeningParenthese()
        right_parenthese = tokens.ClosingParenthese()
        end_token = tokens.EndKeyword()

        self.validate(
            [
                local_token,
                function_token,
                identifier,
                left_parenthese,
                right_parenthese,
                end_token,
            ],
            nodes.LocalFunctionAssignStatement(
                identifier,
                nodes.FunctionExpression(
                    nodes.Block(),
                    [],
                    function_token,
                    end_token,
                    left_parenthese,
                    right_parenthese,
                ),
                local_token,
            ),
        )

    def test_parse_local_function_assign_missing_function_name(self):
        local_token = tokens.LocalKeyword()
        function_token = tokens.FunctionKeyword()
        left_parenthese = tokens.OpeningParenthese()
        right_parenthese = tokens.ClosingParenthese()
        end_token = tokens.EndKeyword()

        exception = self.assert_exception(
            exceptions.IdentifierExpectedException,
            [
                local_token,
                function_token,
                left_parenthese,
                right_parenthese,
                end_token,
            ],
        )
        self.assertEqual(exception.get_previous_token(), function_token)

    def test_parse_function_assign_statement(self):
        function_token = tokens.FunctionKeyword()
        identifier = tokens.Identifier('foo')
        left_parenthese = tokens.OpeningParenthese()
        right_parenthese = tokens.ClosingParenthese()
        end_token = tokens.EndKeyword()

        self.validate(
            [
                function_token,
                identifier,
                left_parenthese,
                right_parenthese,
                end_token,
            ],
            nodes.FunctionAssignStatement(
                nodes.FunctionName([identifier]),
                nodes.FunctionExpression(
                    nodes.Block(),
                    [],
                    function_token,
                    end_token,
                    left_parenthese,
                    right_parenthese,
                ),
            ),
        )

    def test_try_parse_end_token_return_none(self):
        self.validate([tokens.EndKeyword()], None)

    # TODO: decide between test_try_parse or test_parse