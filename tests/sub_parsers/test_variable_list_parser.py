import unittest

from luaparser import exceptions, nodes, tokens, TokenQueue
from luaparser.sub_parsers import VariableListParser


class TestVariableListParser(unittest.TestCase):

    def setUp(self):
        self.parser = VariableListParser()
        self.end_of_file = tokens.EndOfFile()
        self.foo_identifier = tokens.Identifier('foo')
        self.bar_identifier = tokens.Identifier('bar')
        self.comma = tokens.Comma()

    def validate(self, token_list, expect_statement):
        token_list.append(self.end_of_file)
        result = self.parser.parse(TokenQueue(token_list))
        self.assertEqual(result, expect_statement)

    def assert_exception(self, exception, token_list, initial=None):
        token_list.append(tokens.EndOfFile())
        token_queue = TokenQueue(token_list)

        with self.assertRaises(exception) as context:
            self.parser.parse(token_queue, initial)

        return context.exception

    def test_parse_single_variable_expression(self):
        self.validate(
            [self.foo_identifier],
            nodes.VariableList([nodes.VariableExpression(self.foo_identifier)]),
        )

    def test_parse_two_variable_expression(self):
        self.validate(
            [self.foo_identifier, self.comma, self.bar_identifier],
            nodes.VariableList(
                [
                    nodes.VariableExpression(self.foo_identifier),
                    nodes.VariableExpression(self.bar_identifier),
                ],
                [self.comma],
            ),
        )

    def test_parse_field_expression(self):
        dot_token = tokens.Dot()

        self.validate(
            [self.foo_identifier, dot_token, self.bar_identifier],
            nodes.VariableList([
                nodes.FieldExpression(
                    nodes.VariableExpression(self.foo_identifier),
                    self.bar_identifier,
                    dot_token,
                ),
            ]),
        )

    def test_parse_index_expression(self):
        left_bracket = tokens.OpeningSquareBracket()
        right_bracket = tokens.ClosingSquareBracket()

        self.validate(
            [
                self.foo_identifier,
                left_bracket,
                self.bar_identifier,
                right_bracket,
            ],
            nodes.VariableList([
                nodes.IndexExpression(
                    nodes.VariableExpression(self.foo_identifier),
                    nodes.VariableExpression(self.bar_identifier),
                    left_bracket,
                    right_bracket,
                ),
            ]),
        )

    def test_parse_with_inital_true_expression_raise_exception(self):
        exception = self.assert_exception(
            exceptions.UnexpectedExpressionException,
            [],
            tokens.TrueKeyword(),
        )
        self.assertEqual(exception.get_next_token(), self.end_of_file)

    def test_parse_true_expression_raise_exception(self):
        exception = self.assert_exception(
            exceptions.UnexpectedExpressionException,
            [tokens.TrueKeyword()],
        )
        self.assertEqual(exception.get_next_token(), self.end_of_file)
