import unittest

from luaparser import nodes, tokens, TokenQueue
from luaparser.exceptions import SymbolExpectedException
from luaparser.sub_parsers import DoStatementParser


class TestDoStatementParser(unittest.TestCase):

    def setUp(self):
        self.parser = DoStatementParser()
        self.do_token = tokens.DoKeyword()
        self.end_token = tokens.EndKeyword()
        self.semi_colon = tokens.SemiColon()

    def validate(self, token_list, expect_statement):
        result = self.parser.parse(TokenQueue(token_list))
        self.assertEqual(result, expect_statement)

    def assert_exception(self, exception, token_list):
        with self.assertRaises(exception) as context:
            self.parser.parse(TokenQueue(token_list))

        return context.exception

    def test_parse_empty_do_statement(self):
        self.validate(
            [self.do_token, self.end_token],
            nodes.DoStatement(nodes.Block(), self.do_token, self.end_token),
        )

    def test_parse_empty_do_statement_with_semi_colon(self):
        self.validate(
            [self.do_token, self.end_token, self.semi_colon],
            nodes.DoStatement(nodes.Block(), self.do_token, self.end_token, self.semi_colon),
        )

    def test_parse_do_statement_with_missing_end_raise_exception_with_expected_symbol(self):
        exception = self.assert_exception(
            SymbolExpectedException,
            [self.do_token, self.semi_colon],
        )
        self.assertEqual(exception.get_expected_symbol(), 'end')

    def test_parse_do_statement_with_missing_end_raise_exception_next_token(self):
        exception = self.assert_exception(
            SymbolExpectedException,
            [self.do_token, self.semi_colon],
        )
        self.assertEqual(exception.get_next_token(), self.semi_colon)
