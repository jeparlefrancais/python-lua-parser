import unittest

from luaparser.lexer_state import LexerState
from luaparser.tokens import Token


class TestLexer(unittest.TestCase):

    def setUp(self):
        self.state = LexerState([])

    def test_append_token_set_token_line_number(self):
        token = Token()
        line_number = 7
        self.state._line_number = line_number
        self.state.append_token(token)

        self.assertEqual(token.get_line_number(), line_number)

    def test_next_line_return_false_when_no_more_lines(self):
        self.state._line_iterator = iter([])

        self.assertFalse(self.state.next_line())

    def test_next_line_return_true_when_another_line_exists(self):
        self.state._line_iterator = iter([''])

        self.assertTrue(self.state.next_line())

    def test_next_line_increment_line_number(self):
        test_line_count = 10
        self.state._line_iterator = iter([str(i) for i in range(1, test_line_count + 1)])

        for i in range(1, test_line_count + 1):
            self.state.next_line()
            self.assertEqual(self.state.get_line_number(), i)

    def test_has_line_ended_is_true_when_line_index_is_not_below_line_length(self):
        self.state._line_index = 10
        self.state._line_length = 5
        self.assertTrue(self.state.has_line_ended())

    def test_has_line_ended_is_true_when_line_index_is_below_line_length(self):
        self.state._line_index = 2
        self.state._line_length = 5
        self.assertFalse(self.state.has_line_ended())
