import unittest

from luaparser import Parser, tokens
from luaparser.nodes import Block, Chunk


class TestParser(unittest.TestCase):

    def setUp(self):
        self.parser = Parser()

    def test_parser_empty_string_return_empty_chunk(self):
        end_of_file_token = tokens.EndOfFile()
        end_of_file_token.set_line_number(1)
        self.assertEqual(self.parser.parse(''), Chunk(Block(), end_of_file_token))
