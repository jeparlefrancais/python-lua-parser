import unittest

from luaparser.tokens import separators


class TestSeparator():

    def setUp(self):
        self.separator = self.get_separator()
        self.other_separator = self.get_separator()

    def test_eq_is_true_with_same_line_number(self):
        self.assertTrue(self.separator == self.other_separator)

    def test_eq_is_false_with_different_line_number(self):
        self.other_separator.set_line_number(7)
        self.assertFalse(self.separator == self.other_separator)

    def test_ne_is_false_with_same_line_number(self):
        self.assertFalse(self.separator != self.other_separator)

    def test_ne_is_true_with_different_line_number(self):
        self.other_separator.set_line_number(7)
        self.assertTrue(self.separator != self.other_separator)

    def test_repr_returns_str(self):
        self.assertIsInstance(repr(self.separator), str)

    def get_separator(self):
        raise NotImplementedError()


def create_separator_test_case(separator_class):

    def get_separator(self):
        return separator_class()

    return type(
        'Test{}Token'.format(separator_class.__name__),
        (TestSeparator, unittest.TestCase),
        {'get_separator': get_separator},
    )


def get_separator_classes():
    return [
        separators.SemiColon,
        separators.Colon,
        separators.Comma,
        separators.Dot,
        separators.VarArgs,
        separators.OpeningSquareBracket,
        separators.ClosingSquareBracket,
        separators.OpeningBrace,
        separators.ClosingBrace,
        separators.OpeningParenthese,
        separators.ClosingParenthese,
    ]


def load_tests(loader, tests, pattern):
    suite = unittest.TestSuite()

    for separator_class in get_separator_classes():
        test_case = create_separator_test_case(separator_class)
        suite.addTests(loader.loadTestsFromTestCase(test_case))

    return suite
