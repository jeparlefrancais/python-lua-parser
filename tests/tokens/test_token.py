import unittest

from luaparser.tokens import Token


def new_token(line_number):
    token = Token()
    token.set_line_number(line_number)
    return token


class TestToken(unittest.TestCase):

    def setUp(self):
        self.line_number = 7
        self.other_line_number = 4

    def test_default_line_number_is_zero(self):
        self.assertEqual(Token().get_line_number(), 0)

    def test_set_line_number_update_line_value(self):
        token = new_token(self.line_number)
        self.assertEqual(token.get_line_number(), self.line_number)

    def test_eq_is_true_with_same_line_number(self):
        token = new_token(self.line_number)
        other_token = new_token(self.line_number)
        self.assertTrue(token == other_token)

    def test_eq_is_false_with_different_line_number(self):
        token = new_token(self.line_number)
        other_token = new_token(self.other_line_number)
        self.assertFalse(token == other_token)

    def test_ne_is_false_with_same_line_number(self):
        token = new_token(self.line_number)
        other_token = new_token(self.line_number)
        self.assertFalse(token != other_token)

    def test_ne_is_true_with_different_line_number(self):
        token = new_token(self.line_number)
        other_token = new_token(self.other_line_number)
        self.assertTrue(token != other_token)
