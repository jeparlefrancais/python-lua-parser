import unittest

from luaparser.tokens import operators


class TestOperator():

    def setUp(self):
        self.operator = self.get_operator()
        self.other_operator = self.get_operator()

    def test_eq_is_true_with_same_line_number(self):
        self.assertTrue(self.operator == self.other_operator)

    def test_eq_is_false_with_different_line_number(self):
        self.other_operator.set_line_number(7)
        self.assertFalse(self.operator == self.other_operator)

    def test_ne_is_false_with_same_line_number(self):
        self.assertFalse(self.operator != self.other_operator)

    def test_ne_is_true_with_different_line_number(self):
        self.other_operator.set_line_number(7)
        self.assertTrue(self.operator != self.other_operator)

    def test_repr_returns_str(self):
        self.assertIsInstance(repr(self.operator), str)

    def get_operator(self):
        raise NotImplementedError()


def create_test_case(operator_class):

    def get_operator(self):
        return operator_class()

    return type(
        'Test{}Token'.format(operator_class.__name__),
        (TestOperator, unittest.TestCase),
        {'get_operator': get_operator},
    )


def _is_token_sub_class(value):
    return isinstance(value, type) and issubclass(value, operators.Token)


def get_classes():
    return filter(
        lambda value: _is_token_sub_class(value) and value != operators.Token,
        map(
            lambda identifier: getattr(operators, identifier),
            filter(lambda identifier: not identifier.startswith('_'), dir(operators)),
        ),
    )


def load_tests(loader, tests, pattern):
    suite = unittest.TestSuite()

    for operator_class in get_classes():
        test_case = create_test_case(operator_class)
        suite.addTests(loader.loadTestsFromTestCase(test_case))

    return suite
