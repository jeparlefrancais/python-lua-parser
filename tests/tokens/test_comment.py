import unittest

from luaparser.tokens import Comment, LongComment


class TestComment(unittest.TestCase):

    def setUp(self):
        self.a_line = 'a'
        self.other_line = 'b'

    def test_repr_does_not_throw(self):
        repr(Comment(self.a_line))

    def test_equal_is_false_when_different_comment(self):
        self.assertFalse(Comment(self.a_line) == Comment(self.other_line))

    def test_equal_is_false_when_same_comment_and_different_line(self):
        comment = Comment(self.a_line)
        comment.set_line_number(4)
        self.assertFalse(comment == Comment(self.a_line))

    def test_equal_is_true_when_same_comment_and_line_number(self):
        self.assertTrue(Comment(self.a_line) == Comment(self.a_line))

    def test_not_equal_is_true_when_different_comment(self):
        self.assertTrue(Comment(self.a_line) != Comment(self.other_line))

    def test_not_equal_is_true_when_same_comment_and_different_line(self):
        comment = Comment(self.a_line)
        comment.set_line_number(4)
        self.assertTrue(comment != Comment(self.a_line))

    def test_not_equal_is_false_when_same_comment_and_line_number(self):
        self.assertFalse(Comment(self.a_line) != Comment(self.a_line))


class TestLongComment(unittest.TestCase):

    def setUp(self):
        self.a_line = 'a'
        self.other_line = 'b'

    def test_repr_does_not_throw(self):
        repr(LongComment(self.a_line, 0))

    def test_equal_is_false_when_different_comment(self):
        self.assertFalse(LongComment(self.a_line, 0) == LongComment(self.other_line, 0))

    def test_equal_is_false_when_same_comment_different_equal_count(self):
        self.assertFalse(LongComment(self.a_line, 0) == LongComment(self.other_line, 1))

    def test_equal_is_false_when_same_comment_and_different_line(self):
        comment = LongComment(self.a_line, 0)
        comment.set_line_number(4)
        self.assertFalse(comment == LongComment(self.a_line, 0))

    def test_equal_is_true_when_same_comment_and_line_number_and_equal_count(self):
        self.assertTrue(LongComment(self.a_line, 1) == LongComment(self.a_line, 1))

    def test_not_equal_is_true_when_different_comment(self):
        self.assertTrue(LongComment(self.a_line, 0) != LongComment(self.other_line, 0))

    def test_not_equal_is_true_when_same_comment_different_equal_count(self):
        self.assertTrue(LongComment(self.a_line, 0) != LongComment(self.other_line, 1))

    def test_not_equal_is_true_when_same_comment_and_different_line(self):
        comment = LongComment(self.a_line, 0)
        comment.set_line_number(4)
        self.assertTrue(comment != LongComment(self.a_line, 0))

    def test_not_equal_is_false_when_same_comment_and_line_number_and_equal_count(self):
        self.assertFalse(LongComment(self.a_line, 1) != LongComment(self.a_line, 1))
