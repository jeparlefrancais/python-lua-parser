import unittest

from luaparser.tokens import String, LongString


class TestLongString(unittest.TestCase):

    def setUp(self):
        self.a_string = 'a'
        self.other_string = 'b'

    def test_repr_does_not_throw(self):
        repr(LongString(self.a_string, 0))

    def test_equal_is_false_when_different_string(self):
        self.assertFalse(LongString(self.a_string, 0) == LongString(self.other_string, 0))

    def test_equal_is_false_when_same_string_different_equal_count(self):
        self.assertFalse(LongString(self.a_string, 0) == LongString(self.other_string, 1))

    def test_equal_is_false_when_same_string_and_different_line(self):
        comment = LongString(self.a_string, 0)
        comment.set_line_number(4)
        self.assertFalse(comment == LongString(self.a_string, 0))

    def test_equal_is_true_when_same_string_and_line_number_and_equal_count(self):
        self.assertTrue(LongString(self.a_string, 1) == LongString(self.a_string, 1))

    def test_not_equal_is_true_when_different_string(self):
        self.assertTrue(LongString(self.a_string, 0) != LongString(self.other_string, 0))

    def test_not_equal_is_true_when_same_string_different_equal_count(self):
        self.assertTrue(LongString(self.a_string, 0) != LongString(self.other_string, 1))

    def test_not_equal_is_true_when_same_string_and_different_line(self):
        comment = LongString(self.a_string, 0)
        comment.set_line_number(4)
        self.assertTrue(comment != LongString(self.a_string, 0))

    def test_not_equal_is_false_when_same_string_and_line_number_and_equal_count(self):
        self.assertFalse(LongString(self.a_string, 1) != LongString(self.a_string, 1))


class TestString(unittest.TestCase):

    def setUp(self):
        self.a_string = 'a'
        self.other_string = 'b'

    def test_repr_does_not_throw(self):
        repr(String(self.a_string, True))

    def test_get_string_returns_same_string(self):
        string = String(self.a_string, True)
        self.assertEqual(string.get_string(), self.a_string)

    def test_is_single_quotes_is_true_when_single_quotes(self):
        string = String(self.a_string, True)
        self.assertTrue(string.is_single_quotes())

    def test_is_single_quotes_is_false_when_double_quotes(self):
        string = String(self.a_string, False)
        self.assertFalse(string.is_single_quotes())

    def test_equal_is_false_when_different_string(self):
        self.assertFalse(String(self.a_string, True) == String(self.other_string, True))

    def test_equal_is_false_when_same_string_and_different_line(self):
        comment = String(self.a_string, True)
        comment.set_line_number(4)
        self.assertFalse(comment == String(self.a_string, True))

    def test_equal_is_false_when_same_string_and_different_quote_symbol(self):
        self.assertFalse(String(self.a_string, True) == String(self.a_string, False))

    def test_equal_is_true_when_same_string_and_line_number(self):
        self.assertTrue(String(self.a_string, True) == String(self.a_string, True))

    def test_not_equal_is_true_when_different_string(self):
        self.assertTrue(String(self.a_string, True) != String(self.other_string, True))

    def test_not_equal_is_true_when_same_string_and_different_line(self):
        comment = String(self.a_string, True)
        comment.set_line_number(4)
        self.assertTrue(comment != String(self.a_string, True))

    def test_not_equal_is_false_when_same_string_and_line_number_and_(self):
        self.assertFalse(String(self.a_string, True) != String(self.a_string, True))
