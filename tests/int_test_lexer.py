import unittest

from luaparser import Lexer
from luaparser import tokens

_test_data = (
    ('', ((tokens.EndOfFile(), 1),)),
    (
        'hello',
        (
            (tokens.Identifier('hello'), 1),
            (tokens.EndOfFile(), 1),
        ),
    ),
    (
        'hello someone',
        (
            (tokens.Identifier('hello'), 1),
            (tokens.Identifier('someone'), 1, ' '),
            (tokens.EndOfFile(), 1),
        ),
    ),
    (
        '"string"',
        (
            (tokens.String('string', False), 1),
            (tokens.EndOfFile(), 1),
        ),
    ),
    (
        "'string'",
        (
            (tokens.String('string', True), 1),
            (tokens.EndOfFile(), 1),
        ),
    ),
    (
        '"-- not a comment"',
        (
            (tokens.String('-- not a comment', False), 1),
            (tokens.EndOfFile(), 1),
        ),
    ),
    # (
    #     '"\\""',
    #     (
    #         (tokens.String('\\"', False), 1),
    #     ),
    # ),
    (
        '-- hey!',
        (
            (tokens.Comment(' hey!'), 1),
            (tokens.EndOfFile(), 1),
        ),
    ),
    (
        ' --hey! -- other comment?',
        (
            (tokens.Comment('hey! -- other comment?'), 1, ' '),
            (tokens.EndOfFile(), 1),
        ),
    ),
    (
        ' --[[first]] --second',
        (
            (tokens.LongComment('first', 0), 1, ' '),
            (tokens.Comment('second'), 1, ' '),
            (tokens.EndOfFile(), 1),
        ),
    ),
    (
        ' --[=[]==]--second]=]',
        (
            (tokens.LongComment(']==]--second', 1), 1, ' '),
            (tokens.EndOfFile(), 1),
        ),
    ),
    (
        '-- "not a string"',
        (
            (tokens.Comment(' "not a string"'), 1),
            (tokens.EndOfFile(), 1),
        ),
    ),
    (
        'a = 10',
        (
            (tokens.Identifier('a'), 1),
            (tokens.Assign(), 1, ' '),
            (tokens.Number(10, '10'), 1, ' '),
            (tokens.EndOfFile(), 1),
        ),
    ),
    (
        'local a = 10 == 7',
        (
            (tokens.LocalKeyword(), 1),
            (tokens.Identifier('a'), 1, ' '),
            (tokens.Assign(), 1, ' '),
            (tokens.Number(10, '10'), 1, ' '),
            (tokens.Equal(), 1, ' '),
            (tokens.Number(7, '7'), 1, ' '),
            (tokens.EndOfFile(), 1),
        ),
    ),
    (
        'var = {}; var["ok"] < 0x10',
        (
            (tokens.Identifier('var'), 1),
            (tokens.Assign(), 1, ' '),
            (tokens.OpeningBrace(), 1, ' '),
            (tokens.ClosingBrace(), 1),
            (tokens.SemiColon(), 1),
            (tokens.Identifier('var'), 1, ' '),
            (tokens.OpeningSquareBracket(), 1),
            (tokens.String('ok', False), 1),
            (tokens.ClosingSquareBracket(), 1),
            (tokens.LowerThan(), 1, ' '),
            (tokens.Number(16, '0x10', True), 1, ' '),
            (tokens.EndOfFile(), 1),
        ),
    ),
    (
        '[[string]]',
        (
            (tokens.LongString('string', 0), 1),
            (tokens.EndOfFile(), 1),
        ),
    ),
)


class IntTestLexer(unittest.TestCase):

    def setUp(self):
        self.lexer = Lexer()

    def validate(self, string_input, expect_tokens):
        self.assertListEqual(self.lexer.parse(string_input), expect_tokens)

    def _add_line_number(self, token_data):
        token = token_data[0]
        token.set_line_number(token_data[1])

        if len(token_data) > 2:
            token.set_preceding_whitespaces(token_data[2])

        return token

    def test_string_input(self):
        expect_tokens = list(map(self._add_line_number, self.token_datas))
        self.validate(self.string_input, expect_tokens)


def load_tests(loader, tests, pattern):
    suite = unittest.TestSuite()

    for string_input, token_datas in _test_data:
        name = '<empty string>' if string_input == '' else string_input.splitlines()[0]
        test_case = type(
            'IntTestLexer: {}'.format(name),
            (IntTestLexer,),
            {
                'string_input': string_input,
                'token_datas': token_datas,
            },
        )
        suite.addTests(loader.loadTestsFromTestCase(test_case))

    return suite
