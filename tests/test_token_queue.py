import unittest

from luaparser import tokens, TokenQueue


class TestTokenQueue(unittest.TestCase):

    def setUp(self):
        self.list = [tokens.TrueKeyword(), tokens.FalseKeyword()]
        self.token_queue = TokenQueue(self.list)

    def test_pop_front_return_first_element(self):
        self.assertEqual(self.token_queue.pop_front(), self.list[0])

    def test_is_next_instance_of_is_true_when_type_matches(self):
        self.assertTrue(self.token_queue.is_next_instance_of(tokens.TrueKeyword))

    def test_is_next_instance_of_is_false_when_type_matches(self):
        self.assertFalse(self.token_queue.is_next_instance_of(tokens.FalseKeyword))

    def test_are_next_instances_of_is_true_when_types_matches(self):
        result = self.token_queue.are_next_instances_of(
            tokens.TrueKeyword,
            tokens.FalseKeyword,
        )
        self.assertTrue(result)

    def test_are_next_instances_of_is_false_when_at_least_one_type_does_not_match(self):
        result = self.token_queue.are_next_instances_of(
            tokens.TrueKeyword,
            tokens.TrueKeyword,
        )
        self.assertFalse(result)

    def test_are_next_instances_of_is_false_when_less_tokens_than_types(self):
        result = self.token_queue.are_next_instances_of(
            tokens.TrueKeyword,
            tokens.FalseKeyword,
            tokens.TrueKeyword,
        )
        self.assertFalse(result)
