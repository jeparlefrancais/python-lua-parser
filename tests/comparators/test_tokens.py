import unittest

from unittest import mock

from luaparser import tokens
from tests.comparator import NodeComparator
from tests.comparators import tokens as token_comparators


class TestComparatorTokens(unittest.TestCase):

    def setUp(self):
        self.node_comparator_mock = mock.create_autospec(NodeComparator)
        self.token_mock_a = mock.create_autospec(tokens.Token)
        self.token_mock_b = mock.create_autospec(tokens.Token)

    def test_compare_token_same_objects_do_not_call_compare(self):
        token_comparators.compare_token(
            self.token_mock_a,
            self.token_mock_a,
            self.node_comparator_mock,
        )

        self.node_comparator_mock.compare.assert_not_called()
        self.node_comparator_mock.compare_lists.assert_not_called()
        self.node_comparator_mock.compare_optionals.assert_not_called()
        self.node_comparator_mock.append.assert_not_called()

    def test_compare_token_get_preceding_whitespaces_is_different_than_result_call_append(self):
        token_comparators.compare_token(
            self.token_mock_a,
            self.token_mock_b,
            self.node_comparator_mock,
        )

        self.node_comparator_mock.compare.assert_not_called()
        self.node_comparator_mock.compare_lists.assert_not_called()
        self.node_comparator_mock.compare_optionals.assert_not_called()
        self.node_comparator_mock.append.assert_called_once()
