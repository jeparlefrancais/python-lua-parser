from luaparser import nodes
from tests.comparator import register_comparator, NodeComparator


@register_comparator(nodes.Statement)
def compare_statement(
    expect: nodes.Statement,
    result: nodes.Statement,
    comparator: NodeComparator,
):
    if expect.get_semi_colon() != result.get_semi_colon():
        comparator.compare_optionals(
            expect.get_semi_colon(),
            result.get_semi_colon(),
            'statement',
            'semi colon',
        )


@register_comparator(nodes.ReturnStatement)
def compare_return(
    expect: nodes.ReturnStatement,
    result: nodes.ReturnStatement,
    comparator: NodeComparator,
):
    if expect.get_return_token() != result.get_return_token():
        comparator.compare(
            expect.get_return_token(),
            result.get_return_token(),
        )
    elif expect.get_expressions() != result.get_expressions():
        comparator.compare(
            expect.get_expressions(),
            result.get_expressions(),
        )


@register_comparator(nodes.ForStatement)
def compare_for(
    expect: nodes.ForStatement,
    result: nodes.ForStatement,
    comparator: NodeComparator,
):
    if expect.get_block() != result.get_block():
        comparator.compare(expect.get_block(), result.get_block())
    elif expect.get_for_token() != result.get_for_token():
        comparator.compare(expect.get_for_token(), result.get_for_token())
    elif expect.get_do_token() != result.get_do_token():
        comparator.compare(expect.get_do_token(), result.get_do_token())
    elif expect.get_end_token() != result.get_end_token():
        comparator.compare(expect.get_end_token(), result.get_end_token())


@register_comparator(nodes.NumericForStatement)
def compare_numeric_for(
    expect: nodes.NumericForStatement,
    result: nodes.NumericForStatement,
    comparator: NodeComparator,
):
    if expect.get_variable() != result.get_variable():
        comparator.compare(expect.get_variable(), result.get_variable())
    elif expect.get_start_expression() != result.get_start_expression():
        comparator.compare(
            expect.get_start_expression(),
            result.get_start_expression(),
        )
    elif expect.get_end_expression() != result.get_end_expression():
        comparator.compare(
            expect.get_end_expression(),
            result.get_end_expression(),
        )
    elif expect.get_step_expression() != result.get_step_expression():
        comparator.compare_optionals(
            expect.get_step_expression(),
            result.get_step_expression(),
            'numeric for statement',
            'step expression',
        )
    elif expect.get_assign_token() != result.get_assign_token():
        comparator.compare(expect.get_assign_token(), result.get_assign_token())
    elif expect.get_start_end_comma() != result.get_start_end_comma():
        comparator.compare(
            expect.get_start_end_comma(),
            result.get_start_end_comma(),
        )
    elif expect.get_end_step_comma() != result.get_end_step_comma():
        comparator.compare_optionals(
            expect.get_end_step_comma(),
            result.get_end_step_comma(),
            'numeric for statement',
            'end-step comma token',
        )


@register_comparator(nodes.ElseifBranch)
def compare_elseif(
    expect: nodes.ElseifBranch,
    result: nodes.ElseifBranch,
    comparator: NodeComparator,
):
    if expect.get_elseif_token() != result.get_elseif_token():
        comparator.compare(expect.get_elseif_token(), result.get_elseif_token())
    elif expect.get_then_token() != result.get_then_token():
        comparator.compare(expect.get_then_token(), result.get_then_token())
    elif expect.get_block() != result.get_block():
        comparator.compare(expect.get_block(), result.get_block())
    elif expect.get_condition() != result.get_condition():
        comparator.compare(expect.get_condition(), result.get_condition())


@register_comparator(nodes.ElseBranch)
def compare_else_branch(
    expect: nodes.ElseBranch,
    result: nodes.ElseBranch,
    comparator: NodeComparator,
):
    if expect.get_else_token() != result.get_else_token():
        comparator.compare(expect.get_else_token(), result.get_else_token())
    elif expect.get_block() != result.get_block():
        comparator.compare(expect.get_block(), result.get_block())


@register_comparator(nodes.IfStatement)
def compare_if(expect: nodes.IfStatement, result: nodes.IfStatement, comparator: NodeComparator):
    if expect.get_if_token() != result.get_if_token():
        comparator.compare(expect.get_if_token(), result.get_if_token())
    elif expect.get_then_token() != result.get_then_token():
        comparator.compare(expect.get_then_token(), result.get_then_token())
    elif expect.get_end_token() != result.get_end_token():
        comparator.compare(expect.get_end_token(), result.get_end_token())
    elif expect.get_block() != result.get_block():
        comparator.compare(expect.get_block(), result.get_block())
    elif expect.get_condition() != result.get_condition():
        comparator.compare(expect.get_condition(), result.get_condition())
    elif expect.get_else_branch() != result.get_else_branch():
        comparator.compare_optionals(
            expect.get_else_branch(),
            result.get_else_branch(),
            'if statement',
            'else branch',
        )
    elif expect.get_elseif_branches() != result.get_elseif_branches():
        comparator.compare_lists(
            expect.get_elseif_branches(),
            result.get_elseif_branches(),
            'if statement',
            'elseif branch',
        )


@register_comparator(nodes.LocalVariableAssignStatement)
def compare_local_assign(
    expect: nodes.LocalVariableAssignStatement,
    result: nodes.LocalVariableAssignStatement,
    comparator: NodeComparator,
):
    if expect.get_names() != result.get_names():
        comparator.compare(expect.get_names(), result.get_names())
    elif expect.get_expressions() != result.get_expressions():
        comparator.compare_optionals(
            expect.get_expressions(),
            result.get_expressions(),
            'local variable assign statement',
            'expression list',
        )
    elif expect.get_local_token() != result.get_local_token():
        comparator.compare_optionals(
            expect.get_local_token(),
            result.get_local_token(),
            'local variable assign statement',
            'local token',
        )
    elif expect.get_assign_token() != result.get_assign_token():
        comparator.compare_optionals(
            expect.get_assign_token(),
            result.get_assign_token(),
            'local variable assign statement',
            'assign token',
        )


@register_comparator(nodes.VariableAssignStatement)
def compare_variable_assign(
    expect: nodes.VariableAssignStatement,
    result: nodes.VariableAssignStatement,
    comparator: NodeComparator,
):
    if expect.get_variables() != result.get_variables():
        comparator.compare(expect.get_variables(), result.get_variables())
    elif expect.get_expressions() != result.get_expressions():
        comparator.compare(expect.get_expressions(), result.get_expressions())
    elif expect.get_assign_token() != result.get_assign_token():
        comparator.compare(expect.get_assign_token(), result.get_assign_token())


@register_comparator(nodes.RepeatStatement)
def compare_repeat(
    expect: nodes.RepeatStatement,
    result: nodes.RepeatStatement,
    comparator: NodeComparator,
):
    if expect.get_block() != result.get_block():
        comparator.compare(expect.get_block(), result.get_block())
    elif expect.get_condition() != result.get_condition():
        comparator.compare(expect.get_condition(), result.get_condition())
    elif expect.get_repeat_token() != result.get_repeat_token():
        comparator.compare(expect.get_repeat_token(), result.get_repeat_token())
    elif expect.get_until_token() != result.get_until_token():
        comparator.compare(expect.get_until_token(), result.get_until_token())


@register_comparator(nodes.WhileStatement)
def compare_while(
    expect: nodes.WhileStatement,
    result: nodes.WhileStatement,
    comparator: NodeComparator,
):
    if expect.get_block() != result.get_block():
        comparator.compare(expect.get_block(), result.get_block())
    elif expect.get_condition() != result.get_condition():
        comparator.compare(expect.get_condition(), result.get_condition())
    elif expect.get_while_token() != result.get_while_token():
        comparator.compare(expect.get_while_token(), result.get_while_token())
    elif expect.get_end_token() != result.get_end_token():
        comparator.compare(expect.get_end_token(), result.get_end_token())


@register_comparator(nodes.FunctionAssignStatement)
def compare_function_assign(
    expect: nodes.FunctionAssignStatement,
    result: nodes.FunctionAssignStatement,
    comparator: NodeComparator,
):
    if expect.get_block() != result.get_block():
        comparator.compare(expect.get_block(), result.get_block())
    elif expect.get_function_name() != result.get_function_name():
        comparator.compare(expect.get_function_name(), result.get_function_name())
    elif expect.get_parameters() != result.get_parameters():
        comparator.compare_lists(
            expect.get_parameters(),
            result.get_parameters(),
            'function assign statement',
            'parameter',
        )