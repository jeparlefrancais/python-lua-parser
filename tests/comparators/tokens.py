from luaparser import tokens
from tests.comparator import register_comparator, NodeComparator


@register_comparator(tokens.Token)
def compare_token(expect: tokens.Token, result: tokens.Token, comparator: NodeComparator):
    if expect.get_preceding_whitespaces() != result.get_preceding_whitespaces():
        comparator.append(
            'expected preceding whitespaces to be "{}" but got "{}"'.format(
                expect.get_preceding_whitespaces().replace('\n', '\\n'),
                result.get_preceding_whitespaces().replace('\n', '\\n'),
            )
        )
