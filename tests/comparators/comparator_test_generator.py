import inspect
import unittest

from unittest import mock
from typing import List, Union

from tests.comparator import NodeComparator


def filter_underscore(attribute):
    return not attribute.startswith('_')


def get_function_attributes(module):
    functions = []

    for attribute in filter(filter_underscore, dir(module)):
        value = getattr(module, attribute)

        if inspect.isfunction(value):
            functions.append(value)

    return functions


def keep_comparator_functions(function):
    signature = inspect.signature(function)

    if len(signature.parameters) != 3:
        return False

    expect_parameter, result_parameter, comparator = signature.parameters.values()

    if expect_parameter.annotation != result_parameter.annotation:
        return False

    if comparator.annotation != NodeComparator:
        return False

    return True


def map_call_names(call):
    name, _, _ = call
    return name


def create_test_function(function, expect_type, different_method, same_methods):
    class_function = getattr(expect_type, different_method)
    class_function_return_type = inspect.signature(class_function).return_annotation
    return_class_type = type(class_function_return_type)

    asserted_method = 'unknown'

    if return_class_type == type:
        asserted_method = 'compare'
    else:
        origin_type = class_function_return_type.__origin__

        # `origin_type == List` is needed for compatibility with 3.6
        if origin_type == list or origin_type == List:
            asserted_method = 'compare_lists'
        elif origin_type == Union:
            if type(None) in class_function_return_type.__args__:
                asserted_method = 'compare_optionals'
            else:
                asserted_method = 'compare'

    def test_getter_difference(self):
        same_method_mock = mock.MagicMock()
        expect_node_mock = mock.create_autospec(expect_type)
        result_node_mock = mock.create_autospec(expect_type)

        for same_method_name in same_methods:
            setattr(result_node_mock, same_method_name, same_method_mock)
            setattr(expect_node_mock, same_method_name, same_method_mock)

        node_comparator_mock = mock.create_autospec(NodeComparator)
        function(expect_node_mock, result_node_mock, node_comparator_mock)
        expect_compared = getattr(expect_node_mock, different_method)()
        result_compared = getattr(result_node_mock, different_method)()

        if asserted_method == 'compare':
            node_comparator_mock.compare.assert_called_once_with(
                expect_compared,
                result_compared,
            )
            node_comparator_mock.compare_lists.assert_not_called()
            node_comparator_mock.compare_optionals.assert_not_called()
            node_comparator_mock.append.assert_not_called()
        elif asserted_method == 'compare_lists':
            node_comparator_mock.compare.assert_not_called()
            node_comparator_mock.compare_lists.assert_called_once_with(
                expect_compared,
                result_compared,
                mock.ANY,
                mock.ANY,
            )
            node_comparator_mock.compare_optionals.assert_not_called()
            node_comparator_mock.append.assert_not_called()
        elif asserted_method == 'compare_optionals':
            node_comparator_mock.compare.assert_not_called()
            node_comparator_mock.compare_lists.assert_not_called()
            node_comparator_mock.compare_optionals.assert_called_once_with(
                expect_compared,
                result_compared,
                mock.ANY,
                mock.ANY,
            )
            node_comparator_mock.append.assert_not_called()
        else:
            self.fail(
                'unable to generate tests for function {} (unknown return type)'.format(
                    function.__qualname__,
                )
            )

    return test_getter_difference, asserted_method


def create_tests(function):
    signature = inspect.signature(function)

    expect_parameter, result_parameter, comparator = signature.parameters.values()

    def test_same_object(self):
        node_mock = mock.create_autospec(expect_parameter.annotation)
        node_comparator_mock = mock.create_autospec(NodeComparator)

        function(node_mock, node_mock, node_comparator_mock)
        node_comparator_mock.compare.assert_not_called()
        node_comparator_mock.compare_lists.assert_not_called()
        node_comparator_mock.compare_optionals.assert_not_called()
        node_comparator_mock.append.assert_not_called()

    tests = {
        'same_objects_do_not_call_compare': test_same_object,
    }

    expect_mock = mock.create_autospec(expect_parameter.annotation)
    function(expect_mock, expect_mock, mock.MagicMock())
    method_names = set(map(map_call_names, expect_mock.method_calls))

    for different_method in method_names:
        same_methods = method_names - {different_method}
        test_function, asserted_method = create_test_function(
            function,
            expect_parameter.annotation,
            different_method,
            same_methods,
        )
        test_name = '{}_is_different_than_result_call_{}'.format(
            different_method,
            asserted_method,
        )
        tests[test_name] = test_function

    return tests


def get_test_functions(module):
    comparators = filter(keep_comparator_functions, get_function_attributes(module))
    tests = {}

    for comparator_function in comparators:
        test_name_template = 'test_{}_'.format(comparator_function.__qualname__)

        for name, test_function in create_tests(comparator_function).items():
            tests[test_name_template + name] = test_function

    return tests
