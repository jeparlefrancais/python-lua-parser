import unittest

from tests.comparators.comparator_test_generator import get_test_functions

from tests.comparators import nodes


def load_tests(loader, tests, pattern):
    suite = unittest.TestSuite()

    test_case = type(
        'TestComparatorNodes',
        (unittest.TestCase,),
        get_test_functions(nodes),
    )
    suite.addTests(loader.loadTestsFromTestCase(test_case))

    return suite
