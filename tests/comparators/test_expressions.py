import unittest

from tests.comparators.comparator_test_generator import get_test_functions

from tests.comparators import expressions


def load_tests(loader, tests, pattern):
    suite = unittest.TestSuite()

    test_case = type(
        'TestComparatorExpressions',
        (unittest.TestCase,),
        get_test_functions(expressions),
    )
    suite.addTests(loader.loadTestsFromTestCase(test_case))

    return suite
