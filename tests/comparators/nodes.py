from luaparser import nodes
from tests.comparator import register_comparator, NodeComparator


@register_comparator(nodes.Chunk)
def compare_chunk(expect: nodes.Chunk, result: nodes.Chunk, comparator: NodeComparator):
    if expect.get_end_of_file_token() != result.get_end_of_file_token():
        comparator.compare(expect.get_end_of_file_token(), result.get_end_of_file_token())
    elif expect.get_block() != result.get_block():
        comparator.compare(expect.get_block(), result.get_block())


@register_comparator(nodes.Block)
def compare_block(expect: nodes.Block, result: nodes.Block, comparator: NodeComparator):
    if expect.get_statements() != result.get_statements():
        comparator.compare_lists(
            expect.get_statements(),
            result.get_statements(),
            'block',
            'statement',
        )


@register_comparator(nodes.ExpressionList)
def compare_expression_list(
    expect: nodes.ExpressionList,
    result: nodes.ExpressionList,
    comparator: NodeComparator,
):
    if expect.get_commas() != result.get_commas():
        comparator.compare_lists(
            expect.get_commas(),
            result.get_commas(),
            'expression list',
            'comma token',
        )
    elif expect.get_expressions() != result.get_expressions():
        comparator.compare_lists(
            expect.get_expressions(),
            result.get_expressions(),
            'expression list',
            'expression',
        )


@register_comparator(nodes.NameList)
def compare_name_list(expect: nodes.NameList, result: nodes.NameList, comparator: NodeComparator):
    if expect.get_names() != result.get_names():
        comparator.compare_lists(
            expect.get_names(),
            result.get_names(),
            'name list',
            'identifier',
        )
    elif expect.get_commas() != result.get_commas():
        comparator.compare_lists(
            expect.get_commas(),
            result.get_commas(),
            'name list',
            'comma',
        )


@register_comparator(nodes.VariableList)
def compare_variable_list(
    expect: nodes.VariableList,
    result: nodes.VariableList,
    comparator: NodeComparator,
):
    if expect.get_commas() != result.get_commas():
        comparator.compare_lists(
            expect.get_commas(),
            result.get_commas(),
            'variable list',
            'comma token',
        )
    elif expect.get_variables() != result.get_variables():
        comparator.compare_lists(
            expect.get_variables(),
            result.get_variables(),
            'variable list',
            'variable',
        )
