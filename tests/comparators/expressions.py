from luaparser import nodes
from tests.comparator import register_comparator, NodeComparator


@register_comparator(nodes.TrueExpression)
def compare_true(
    expect: nodes.TrueExpression,
    result: nodes.TrueExpression,
    comparator: NodeComparator,
):
    if expect.get_true_token() != result.get_true_token():
        comparator.compare(expect.get_true_token(), result.get_true_token())


@register_comparator(nodes.FalseExpression)
def compare_false(
    expect: nodes.FalseExpression,
    result: nodes.FalseExpression,
    comparator: NodeComparator,
):
    if expect.get_false_token() != result.get_false_token():
        comparator.compare(expect.get_false_token(), result.get_false_token())


@register_comparator(nodes.VariableExpression)
def compare_variable(
    expect: nodes.VariableExpression,
    result: nodes.VariableExpression,
    comparator: NodeComparator,
):
    if expect.get_name() != result.get_name():
        comparator.compare(expect.get_name(), result.get_name())


@register_comparator(nodes.BinaryOperationExpression)
def compare_binary_operation(
    expect: nodes.BinaryOperationExpression,
    result: nodes.BinaryOperationExpression,
    comparator: NodeComparator,
):
    if expect.get_operator() != result.get_operator():
        comparator.compare(expect.get_operator(), result.get_operator())
    elif expect.get_left_expression() != result.get_left_expression():
        comparator.compare(expect.get_left_expression(), result.get_left_expression())
    elif expect.get_right_expression() != result.get_right_expression():
        comparator.compare(expect.get_right_expression(), result.get_right_expression())


@register_comparator(nodes.UnaryOperationExpression)
def compare_unary_operation(
    expect: nodes.UnaryOperationExpression,
    result: nodes.UnaryOperationExpression,
    comparator: NodeComparator,
):
    if expect.get_operator() != result.get_operator():
        comparator.compare(expect.get_operator(), result.get_operator())
    elif expect.get_operand() != result.get_operand():
        comparator.compare(expect.get_operand(), result.get_operand())
