import unittest

from luaparser import tokens
from luaparser.lexer_tokenizers import IdentifierTokenizer
from luaparser.lexer_state import LexerState


class TestIdentifierTokenizer(unittest.TestCase):

    def setUp(self):
        self.line_number = 1
        self.state = LexerState(iter([]))
        self.tokenizer = IdentifierTokenizer()

    def set_line(self, line):
        self.state._line = line
        self.state._line_length = len(line)
        self.state._line_number = self.line_number
        self.state._line_index = 0

    def validate_token(self, expect_token):
        self.tokenizer.parse(self.state)
        expect_token.set_line_number(self.line_number)
        token_list = self.state.get_tokens()
        self.assertEqual(len(token_list), 1)
        self.assertEqual(token_list[0], expect_token)

    def test_parse_identifier(self):
        symbol = 'abc'
        self.set_line(symbol)
        self.validate_token(tokens.Identifier(symbol))

    def test_parse_identifier_followed_by_whitespace(self):
        symbol = 'abc'
        self.set_line(symbol + ' ')
        self.validate_token(tokens.Identifier(symbol))

    def test_parse_identifier_followed_by_dot(self):
        symbol = 'abc'
        self.set_line(symbol + '.')
        self.validate_token(tokens.Identifier(symbol))

    def test_parse_identifier_start_with_underscore(self):
        symbol = '_abc'
        self.set_line(symbol)
        self.validate_token(tokens.Identifier(symbol))

    def test_parse_identifier_contains_number(self):
        symbol = '_a1'
        self.set_line(symbol)
        self.validate_token(tokens.Identifier(symbol))

    def test_parse_and_return_and_keyword(self):
        self.set_line('and')
        self.validate_token(tokens.AndKeyword())

    def test_parse_break_return_keyword(self):
        self.set_line('break')
        self.validate_token(tokens.BreakKeyword())

    def test_parse_do_return_keyword(self):
        self.set_line('do')
        self.validate_token(tokens.DoKeyword())

    def test_parse_else_return_keyword(self):
        self.set_line('else')
        self.validate_token(tokens.ElseKeyword())

    def test_parse_elseif_return_keyword(self):
        self.set_line('elseif')
        self.validate_token(tokens.ElseifKeyword())

    def test_parse_end_return_keyword(self):
        self.set_line('end')
        self.validate_token(tokens.EndKeyword())

    def test_parse_false_return_keyword(self):
        self.set_line('false')
        self.validate_token(tokens.FalseKeyword())

    def test_parse_for_return_keyword(self):
        self.set_line('for')
        self.validate_token(tokens.ForKeyword())

    def test_parse_function_return_keyword(self):
        self.set_line('function')
        self.validate_token(tokens.FunctionKeyword())

    def test_parse_if_return_keyword(self):
        self.set_line('if')
        self.validate_token(tokens.IfKeyword())

    def test_parse_in_return_keyword(self):
        self.set_line('in')
        self.validate_token(tokens.InKeyword())

    def test_parse_local_return_keyword(self):
        self.set_line('local')
        self.validate_token(tokens.LocalKeyword())

    def test_parse_nil_return_keyword(self):
        self.set_line('nil')
        self.validate_token(tokens.NilKeyword())

    def test_parse_not_return_keyword(self):
        self.set_line('not')
        self.validate_token(tokens.NotKeyword())

    def test_parse_or_return_keyword(self):
        self.set_line('or')
        self.validate_token(tokens.OrKeyword())

    def test_parse_repeat_return_keyword(self):
        self.set_line('repeat')
        self.validate_token(tokens.RepeatKeyword())

    def test_parse_return_return_keyword(self):
        self.set_line('return')
        self.validate_token(tokens.ReturnKeyword())

    def test_parse_then_return_keyword(self):
        self.set_line('then')
        self.validate_token(tokens.ThenKeyword())

    def test_parse_true_return_keyword(self):
        self.set_line('true')
        self.validate_token(tokens.TrueKeyword())

    def test_parse_until_return_keyword(self):
        self.set_line('until')
        self.validate_token(tokens.UntilKeyword())

    def test_parse_while_return_keyword(self):
        self.set_line('while')
        self.validate_token(tokens.WhileKeyword())
