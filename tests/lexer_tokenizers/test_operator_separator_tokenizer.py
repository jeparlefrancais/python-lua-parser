import unittest

from luaparser import tokens
from luaparser.exceptions import UnexpectedSymbolException
from luaparser.lexer_state import LexerState
from luaparser.lexer_tokenizers import OperatorSeparatorTokenizer


class TestOperatorSeparatorTokenizer(unittest.TestCase):

    def setUp(self):
        self.line_number = 1
        self.state = LexerState(iter([]))
        self.tokenizer = OperatorSeparatorTokenizer()

    def set_line(self, line):
        self.state._line = line
        self.state._line_length = len(line)
        self.state._line_number = self.line_number
        self.state._line_index = 0

    def validate_token(self, expect_token):
        self.tokenizer.parse(self.state)
        expect_token.set_line_number(self.line_number)
        token_list = self.state.get_tokens()
        self.assertEqual(len(token_list), 1)
        self.assertEqual(token_list[0], expect_token)
        self.assertTrue(self.state.has_line_ended())

    def validate_tokens(self, expect_tokens):
        self.tokenizer.parse(self.state)

        for token in expect_tokens:
            token.set_line_number(self.line_number)

        token_list = self.state.get_tokens()
        self.assertListEqual(token_list, expect_tokens)

    def assert_exception(self, exception):
        with self.assertRaises(exception) as context:
            self.tokenizer.parse(self.state)

        return context.exception

    def test_parse_plus(self):
        self.set_line('+')
        self.validate_token(tokens.Plus())

    def test_parse_opening_parenthese(self):
        self.set_line('(')
        self.validate_token(tokens.OpeningParenthese())

    def test_parse_closing_parenthese(self):
        self.set_line(')')
        self.validate_token(tokens.ClosingParenthese())

    def test_parse_opening_brace(self):
        self.set_line('{')
        self.validate_token(tokens.OpeningBrace())

    def test_parse_closing_brace(self):
        self.set_line('}')
        self.validate_token(tokens.ClosingBrace())

    def test_parse_opening_square_bracket(self):
        self.set_line('[')
        self.validate_token(tokens.OpeningSquareBracket())

    def test_parse_closing_square_bracket(self):
        self.set_line(']')
        self.validate_token(tokens.ClosingSquareBracket())

    def test_parse_semi_colon(self):
        self.set_line(';')
        self.validate_token(tokens.SemiColon())

    def test_parse_comma(self):
        self.set_line(',')
        self.validate_token(tokens.Comma())

    def test_parse_slash(self):
        self.set_line('/')
        self.validate_token(tokens.Slash())

    def test_parse_length_operator(self):
        self.set_line('#')
        self.validate_token(tokens.Length())

    def test_parse_lower_than(self):
        self.set_line('<')
        self.validate_token(tokens.LowerThan())

    def test_parse_greater_than(self):
        self.set_line('>')
        self.validate_token(tokens.GreaterThan())

    def test_parse_lower_or_equal_than(self):
        self.set_line('<=')
        self.validate_token(tokens.LowerOrEqualThan())

    def test_parse_greater_or_equal_than(self):
        self.set_line('>=')
        self.validate_token(tokens.GreaterOrEqualThan())

    def test_parse_assign_equal(self):
        self.set_line('=')
        self.validate_token(tokens.Assign())

    def test_parse_equal_operator(self):
        self.set_line('==')
        self.validate_token(tokens.Equal())

    def test_parse_not_equal_operator(self):
        self.set_line('~=')
        self.validate_token(tokens.NotEqual())

    def test_parse_dot(self):
        self.set_line('.')
        self.validate_token(tokens.Dot())

    def test_parse_concat_operator(self):
        self.set_line('..')
        self.validate_token(tokens.Concat())

    def test_parse_varargs_dots(self):
        self.set_line('...')
        self.validate_token(tokens.VarArgs())

    def test_parse_float_starting_with_dot(self):
        self.set_line('.5')
        self.validate_token(tokens.Number(0.5, '.5'))

    def test_parse_long_string(self):
        self.set_line('[[hello]]')
        self.validate_token(tokens.LongString('hello', 0))

    def test_parse_long_string_with_equal_signs(self):
        for i in range(1, 10):
            with self.subTest(equal_count=i):
                equals = '=' * i
                self.setUp()
                self.set_line('[{}[hello]{}]'.format(equals, equals))
                self.validate_token(tokens.LongString('hello', i))

    def test_parse_tilde_should_throw(self):
        self.set_line('~')
        exception = self.assert_exception(UnexpectedSymbolException)
        self.assertEqual(exception.get_symbol(), '~')

    def test_parse_exclamation_should_throw(self):
        self.set_line('!')
        exception = self.assert_exception(UnexpectedSymbolException)
        self.assertEqual(exception.get_symbol(), '!')
