import unittest

from luaparser.exceptions import UnfinishedLongStringException
from luaparser.lexer_state import LexerState
from luaparser.lexer_tokenizers import LongStringTokenizer
from luaparser.tokens import LongString, EndOfFile


class TestLongStringTokenizer(unittest.TestCase):

    def setUp(self):
        self.line_number = 1
        self.state = LexerState(iter([]))
        self.tokenizer = LongStringTokenizer(0)

    def set_line(self, line, equal_count=0):
        self.tokenizer._equal_count = equal_count
        self.state._line = line
        self.state._line_length = len(line)
        self.state._line_number = self.line_number
        self.state._line_index = 0

    def validate_token(self, expect_token):
        self.tokenizer.parse(self.state)
        expect_token.set_line_number(self.line_number)
        token_list = self.state.get_tokens()
        self.assertEqual(len(token_list), 1)
        self.assertEqual(token_list[0], expect_token)

    def assert_exception(self, exception):
        with self.assertRaises(exception) as context:
            self.tokenizer.parse(self.state)

        return context.exception

    def test_parse_multiline_string_without_equal(self):
        self.set_line('hello\nworld!]]')
        self.validate_token(LongString('hello\nworld!', 0))

    def test_parse_multiline_string_with_equals(self):
        for i in range(1, 10):
            with self.subTest(equal_count=i):
                self.setUp()
                self.set_line('hello\nworld!]{}]'.format('=' * i), i)
                self.validate_token(LongString('hello\nworld!', i))

    def test_parse_unfinished_long_string_throw_exception(self):
        self.set_line("hello")
        exception = self.assert_exception(UnfinishedLongStringException)
        self.assertEqual(exception.get_line_number(), 1)
        self.assertEqual(exception.get_symbol(), EndOfFile().get_symbol())
