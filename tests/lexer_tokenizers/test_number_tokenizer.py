import unittest

from luaparser.exceptions import MalformedNumberException
from luaparser.lexer_tokenizers import NumberTokenizer
from luaparser.lexer_state import LexerState
from luaparser.tokens import Number


class TestNumberTokenizer(unittest.TestCase):

    def setUp(self):
        self.line_number = 1
        self.state = LexerState(iter([]))
        self.tokenizer = NumberTokenizer()

    def set_line(self, line):
        self.state._line = line
        self.state._line_length = len(line)
        self.state._line_number = self.line_number
        self.state._line_index = 0

    def validate_token(self, symbol, expect_token):
        self.set_line(symbol)
        self.tokenizer.parse(self.state)
        expect_token.set_line_number(self.line_number)
        token_list = self.state.get_tokens()
        self.assertEqual(len(token_list), 1)
        self.assertEqual(token_list[0], expect_token)

    def assert_exception(self, symbol):
        self.set_line(symbol)

        with self.assertRaises(MalformedNumberException) as context:
            self.tokenizer.parse(self.state)

        self.assertEqual(context.exception.get_symbol(), symbol)

    def test_parse_integer(self):
        symbol = '123'

        self.validate_token(symbol, Number(123, symbol))

    def test_parse_integer_with_exponent(self):
        symbol = '123e12'

        self.validate_token(symbol, Number(123, symbol, exponent=12))

    def test_parse_integer_with_exponent_with_plus_sign(self):
        symbol = '123e+12'

        self.validate_token(symbol, Number(123, symbol, exponent=12))

    def test_parse_integer_with_exponent_with_minus_sign(self):
        symbol = '123e-12'

        self.validate_token(symbol, Number(123, symbol, exponent=-12))

    def test_parse_integer_with_uppercase_exponent(self):
        symbol = '123E12'

        self.validate_token(symbol, Number(123, symbol, exponent=12))

    def test_parse_hexadecimal_integer(self):
        symbol = '0x10'

        self.validate_token(symbol, Number(16, symbol, True))

    def test_parse_hexadecimal_integer_with_exponent(self):
        symbol = '0x10p10'

        self.validate_token(symbol, Number(16, symbol, True, 10))

    def test_parse_hexadecimal_integer_with_uppercase_exponent(self):
        symbol = '0x10P10'

        self.validate_token(symbol, Number(16, symbol, True, 10))

    def test_parse_float(self):
        symbol = '123.456'

        self.validate_token(symbol, Number(123.456, symbol))

    def test_parse_float_without_decimal_digits(self):
        symbol = '123.'

        self.validate_token(symbol, Number(123., symbol))

    def test_parse_float_with_exponent(self):
        symbol = '123.456e14'

        self.validate_token(symbol, Number(123.456, symbol, False, 14))

    def test_parse_exponent_without_value_raise_malformed_number_exception(self):
        self.assert_exception('123e')

    def test_parse_number_with_multiple_dot_raise_malformed_number_exception(self):
        self.assert_exception('12.13.14')

    def test_parse_number_ending_with_letter_raise_malformed_number_exception(self):
        self.assert_exception('123a')

    def test_parse_number_ending_with_underscore_raise_malformed_number_exception(self):
        self.assert_exception('123_')

    def test_parse_hex_number_without_value_raise_malformed_number_exception(self):
        self.assert_exception('0x')
