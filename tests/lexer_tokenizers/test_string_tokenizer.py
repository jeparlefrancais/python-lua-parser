import unittest

from luaparser.exceptions import UnfinishedStringException
from luaparser.lexer_state import LexerState
from luaparser.lexer_tokenizers import StringTokenizer
from luaparser.tokens import String


class TestStringTokenizer(unittest.TestCase):

    def setUp(self):
        self.line_number = 1
        self.state = LexerState(iter([]))
        self.tokenizer = StringTokenizer("'")

    def set_line(self, line):
        self.state._line = line
        self.state._line_length = len(line)
        self.state._line_number = self.line_number
        self.state._line_index = 0

    def validate_token(self, expect_token):
        self.tokenizer.parse(self.state)
        expect_token.set_line_number(self.line_number)
        token_list = self.state.get_tokens()
        self.assertEqual(len(token_list), 1)
        self.assertEqual(token_list[0], expect_token)

    def assert_exception(self, exception):
        with self.assertRaises(exception) as context:
            self.tokenizer.parse(self.state)

        return context.exception

    def test_parse_single_quote_string(self):
        self.set_line("'hello'")
        self.validate_token(String('hello', True))

    def test_parse_double_quote_string(self):
        self.set_line('"hello"')
        self.tokenizer._quote_symbol = '"'
        self.validate_token(String('hello', False))

    def test_unfinished_single_quote_string_throw_exception(self):
        self.set_line("'hello")
        exception = self.assert_exception(UnfinishedStringException)

        self.assertEqual(exception.get_symbol(), "'hello")

    def test_unfinished_double_quote_string_throw_exception(self):
        self.set_line('"hello')
        self.tokenizer._quote_symbol = '"'
        exception = self.assert_exception(UnfinishedStringException)

        self.assertEqual(exception.get_symbol(), '"hello')

    # def test_unfinished_double_quote_string_throw_exception(self):
    #     self.set_line("'hello\\\\")
    #     exception = self.assert_exception(UnfinishedStringException)

    #     self.assertEqual(exception.get_symbol(), "'hello\\\\")
