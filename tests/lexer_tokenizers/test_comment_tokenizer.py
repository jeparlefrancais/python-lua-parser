import unittest

from luaparser import tokens
from luaparser.lexer_state import LexerState
from luaparser.lexer_tokenizers import CommentTokenizer


class TestNumberTokenizer(unittest.TestCase):

    def setUp(self):
        self.line_number = 1
        self.state = LexerState(iter([]))
        self.tokenizer = CommentTokenizer()

    def set_line(self, line):
        self.state._line = line
        self.state._line_length = len(line)
        self.state._line_number = self.line_number
        self.state._line_index = 0

    def validate_token(self, expect_token):
        self.tokenizer.parse(self.state)
        expect_token.set_line_number(self.line_number)
        token_list = self.state.get_tokens()
        self.assertEqual(len(token_list), 1)
        self.assertEqual(token_list[0], expect_token)

    def test_parse_single_line_comment(self):
        self.set_line('hello')
        self.validate_token(tokens.Comment('hello'))

    def test_parse_long_comment_without_equal_sign(self):
        self.set_line('[[hello]]')
        self.validate_token(tokens.LongComment('hello', 0))

    def test_parse_long_string_with_equal_signs(self):
        for i in range(1, 10):
            with self.subTest(equal_count=i):
                equals = '=' * i
                self.setUp()
                self.set_line('[{}[hello]{}]'.format(equals, equals))
                self.validate_token(tokens.LongComment('hello', i))
