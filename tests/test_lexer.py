import unittest

from luaparser import Lexer
from luaparser.tokens import EndOfFile


class TestLexer(unittest.TestCase):

    def setUp(self):
        self.lexer = Lexer()

    def test_parse_empty_string_returns_end_of_file_token(self):
        tokens = self.lexer.parse('')
        self.assertEqual(len(tokens), 1)
        self.assertIsInstance(tokens[0], EndOfFile)

    # def test_parse_file(self):
    #     with open('.style.yapf', 'r') as file_stream:
    #         self.exer.parse_file(file_stream)

    # def test_is_keyword_returns_true_with_lua_keywords(self):
    #     keywords = [
    #         'and', 'break', 'do', 'else', 'elseif', 'end',
    #         'false', 'for', 'function', 'if', 'in', 'local',
    #         'nil', 'not', 'or', 'repeat', 'return', 'then',
    #         'true', 'until', 'while',
    #     ]
    #     for keyword in keywords:
    #         self.assertTrue(self.lexer._is_keyword(keyword))
