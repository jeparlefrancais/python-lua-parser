import unittest

from unittest import mock

from luaparser import tokens
from tests.comparator import NodeComparator
from tests.comparators import tokens as token_comparators


class TestComparator(unittest.TestCase):

    def setUp(self):
        self.comparator = NodeComparator()

    def test_get_output_return_string(self):
        self.comparator._output = ['a', 'b']
        self.assertEqual(self.comparator.get_output(), 'a\nb')
