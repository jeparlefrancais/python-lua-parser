import os
import unittest

from luaparser import Parser, nodes, tokens
from tests.comparator import NodeComparator

_lua_test_scripts = os.path.join(os.getcwd(), 'tests', 'lua_scripts')


class IntTestParser():

    def setUp(self):
        self.parser = Parser()

    def test_string_input(self):
        result = self.parse()
        expect = self.get_expected_result()

        if result != expect:
            self.fail(self.compare(expect, result))

    def create_chunk(self, block, end_line_number=1, end_whitespaces=''):
        end_token = tokens.EndOfFile()
        end_token.set_line_number(end_line_number)
        end_token.set_preceding_whitespaces(end_whitespaces)
        return nodes.Chunk(block, end_token)

    def wrap_token(self, token, line_number=1, whitespaces=''):
        token.set_preceding_whitespaces(whitespaces)
        token.set_line_number(line_number)
        return token

    def compare(self, expect, result):
        comparator = NodeComparator()
        comparator.compare(expect, result)
        return comparator.get_output()

    def get_string_input(self):
        return None

    def get_lua_test_script(self):
        return None

    def parse(self):
        string_input = self.get_string_input()

        if string_input is None:
            file_name = self.get_lua_test_script() + '.lua'

            if not isinstance(file_name, str):
                raise ValueError('Lua script must be a str value')

            file_path = os.path.join(_lua_test_scripts, file_name)

            if not os.path.exists(file_path):
                raise OSError(
                    'Lua script named "{}" could not be found in {}'.format(
                        file_name,
                        _lua_test_scripts,
                    )
                )

            with open(file_path, 'r') as lua_script:
                return self.parser.parse_file(lua_script)
        else:
            return self.parser.parse(string_input)
