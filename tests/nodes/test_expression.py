import unittest

from luaparser import nodes, tokens


class ExpressionTestCase():

    def setUp(self):
        self.token = self.get_keyword()
        self.different_token = self.get_keyword()
        self.different_token.set_line_number(2)

    def test_eq_is_true_when_same_keyword(self):
        result = self.get_expression(self.token) == self.get_expression(self.token)
        self.assertTrue(result)

    def test_eq_is_false_when_different_keyword(self):
        result = self.get_expression(self.token) == self.get_expression(self.different_token)
        self.assertFalse(result)

    def test_ne_is_false_when_same_keyword(self):
        result = self.get_expression(self.token) != self.get_expression(self.token)
        self.assertFalse(result)

    def test_ne_is_true_when_different_keyword(self):
        result = self.get_expression(self.token) != self.get_expression(self.different_token)
        self.assertTrue(result)

    def get_keyword(self):
        raise NotImplementedError()

    def get_expression(self, token):
        raise NotImplementedError()


class TestTrueExpression(ExpressionTestCase, unittest.TestCase):

    def get_keyword(self):
        return tokens.TrueKeyword()

    def get_expression(self, token):
        return nodes.TrueExpression(token)


class TestFalseExpression(ExpressionTestCase, unittest.TestCase):

    def get_keyword(self):
        return tokens.FalseKeyword()

    def get_expression(self, token):
        return nodes.FalseExpression(token)


class TestNilExpression(ExpressionTestCase, unittest.TestCase):

    def get_keyword(self):
        return tokens.NilKeyword()

    def get_expression(self, token):
        return nodes.NilExpression(token)


class TestNumberExpression(ExpressionTestCase, unittest.TestCase):

    def get_keyword(self):
        return tokens.Number(0, '0')

    def get_expression(self, token):
        return nodes.NumberExpression(token)
