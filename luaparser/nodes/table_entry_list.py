from typing import Any, List, Union

from luaparser import tokens
from luaparser.nodes.node import Node
from luaparser.nodes.table_entries.table_entry import TableEntry

TableEntrySeparator = Union[tokens.Comma, tokens.SemiColon]


class TableEntryList(Node):

    def __init__(self, entries: List[TableEntry] = [], commas: List[TableEntrySeparator] = []):
        self._entries = entries
        self._commas = commas

    def get_entries(self) -> List[TableEntry]:
        return self._entries

    def get_entries_count(self) -> int:
        return len(self._entries)

    def __eq__(self, value: Any) -> bool:
        if not isinstance(value, TableEntryList):
            return False

        return self._entries == value._entries and self._commas == value._commas

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)
