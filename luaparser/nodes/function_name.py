from typing import Any, List, Optional

from luaparser import tokens
from luaparser.nodes.node import Node


class FunctionName(Node):

    def __init__(
        self,
        names: List[tokens.Identifier],
        dots: List[tokens.Dot] = [],
        method_name: Optional[tokens.Identifier] = None,
        colon: Optional[tokens.Colon] = None,
    ):
        self._names = names
        self._dots = dots
        self._method_name = method_name
        self._colon = colon

    def get_names(self) -> List[tokens.Identifier]:
        return self._names

    def get_method_name(self) -> Optional[tokens.Identifier]:
        return self._method_name

    def __eq__(self, value: Any) -> bool:
        if not isinstance(value, FunctionName):
            return False

        if self._names != value._names or self._dots != value._dots:
            return False

        return self._method_name == value._method_name and self._colon == value._colon

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)
