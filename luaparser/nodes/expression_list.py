from typing import Any, List

from luaparser import tokens
from luaparser.nodes.node import Node
from luaparser.nodes.expressions.expression import Expression


class ExpressionList(Node):

    def __init__(self, expressions: List[Expression] = [], commas: List[tokens.Comma] = []):
        self._expressions = expressions
        self._commas = commas

    def get_expressions(self) -> List[Expression]:
        return self._expressions

    def get_expression_count(self) -> int:
        return len(self._expressions)

    def get_commas(self) -> List[tokens.Comma]:
        return self._commas

    def __eq__(self, value: Any) -> bool:
        if not isinstance(value, ExpressionList):
            return False

        return self._expressions == value._expressions and self._commas == value._commas

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        expression_count = self.get_expression_count()
        return '[| ExpressionList of {} expression{} |]'.format(
            expression_count,
            's' if expression_count > 1 else '',
        )
