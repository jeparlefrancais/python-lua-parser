from typing import Any, List

from luaparser import tokens
from luaparser.nodes.node import Node
from luaparser.nodes.statements.statement import Statement


class Block(Node):

    def __init__(self, statements: List[Statement] = []):
        self._statements = statements

    def get_statements(self) -> List[Statement]:
        return self._statements

    def is_empty(self) -> bool:
        return len(self._statements) == 0

    def set_statements(self, statements: List[Statement]):
        self._statements = statements

    def __eq__(self, value: Any) -> bool:
        return isinstance(value, Block) and self._statements == value._statements

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        statement_count = len(self._statements)
        return '[| Block of {} statement{} |]'.format(
            statement_count,
            's' if statement_count > 1 else '',
        )
