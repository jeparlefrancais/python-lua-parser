from typing import Any, List

from luaparser import tokens
from luaparser.nodes.node import Node


class NameList(Node):

    def __init__(self, names: List[tokens.Identifier], commas: List[tokens.Comma] = []):
        self._names = names
        self._commas = commas

    def get_names(self) -> List[tokens.Identifier]:
        return self._names

    def get_name_count(self) -> int:
        return len(self._names)

    def get_commas(self) -> List[tokens.Comma]:
        return self._commas

    def __eq__(self, value: Any) -> bool:
        if not isinstance(value, NameList):
            return False

        return self._names == value._names and self._commas == value._commas

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        name_count = self.get_name_count()
        return '[| NameList of {} name{} |]'.format(
            name_count,
            's' if name_count > 1 else '',
        )
