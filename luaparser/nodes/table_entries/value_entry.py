from typing import Any

from luaparser.nodes.expressions.expression import Expression
from luaparser.nodes.table_entries.table_entry import TableEntry


class ValueEntry(TableEntry):

    def __init__(self, value: Expression):
        self._value = value

    def get_value(self) -> Expression:
        return self._value

    def __eq__(self, value: Any) -> bool:
        if not isinstance(value, ValueEntry):
            return False

        return self._value == value._value

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return '[| ValueEntry of {} |]'.format(self._value)
