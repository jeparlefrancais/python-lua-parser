from typing import Any

from luaparser import tokens
from luaparser.nodes.expressions.expression import Expression
from luaparser.nodes.table_entries.table_entry import TableEntry


class FieldEntry(TableEntry):

    def __init__(self, key: tokens.Identifier, value: Expression, assign_token: tokens.Assign):
        self._key = key
        self._value = value
        self._assign_token = assign_token

    def get_key(self) -> tokens.Identifier:
        return self._key

    def get_value(self) -> Expression:
        return self._value

    def __eq__(self, value: Any) -> bool:
        if not (isinstance(value, FieldEntry) and self._key == value._key):
            return False

        return self._value == value._value and self._assign_token == value._assign_token

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return '[| FieldEntry of {} = {} |]'.format(self._key.get_name(), self._value)
