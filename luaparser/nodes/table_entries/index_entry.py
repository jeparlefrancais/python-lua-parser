from typing import Any

from luaparser import tokens
from luaparser.nodes.expressions.expression import Expression
from luaparser.nodes.table_entries.table_entry import TableEntry


class IndexEntry(TableEntry):

    def __init__(
        self,
        key: Expression,
        value: Expression,
        left_bracket: tokens.OpeningSquareBracket,
        right_bracket: tokens.ClosingSquareBracket,
        assign: tokens.Assign,
    ):
        self._key = key
        self._value = value
        self._left_bracket = left_bracket
        self._right_bracket = right_bracket
        self._assign = assign

    def get_key_expression(self) -> Expression:
        return self._key

    def get_value_expression(self) -> Expression:
        return self._value

    def __eq__(self, value: Any) -> bool:
        if not (isinstance(value, IndexEntry) and self._assign == value._assign):
            return False

        if not (self._key == value._key and self._value == value._value):
            return False

        return self._left_bracket == value._left_bracket and self._right_bracket == value._right_bracket

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return '[| IndexEntry of {} = {} |]'.format(self._key, self._value)
