from typing import Any, Iterator

from luaparser import tokens
from luaparser.nodes.node import Node
from luaparser.nodes.block import Block


class Chunk(Node):

    def __init__(self, block: Block, end_of_file_token: tokens.EndOfFile):
        self._block = block
        self._end_of_file_token = end_of_file_token

    def get_block(self) -> Block:
        return self._block

    def get_end_of_file_token(self) -> tokens.EndOfFile:
        return self._end_of_file_token

    def __eq__(self, value: Any) -> bool:
        if not (isinstance(value, Chunk) and self._block == value._block):
            return False

        return self._end_of_file_token == value._end_of_file_token

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        statement_count = len(self._block.get_statements())
        return '[| Chunk of {} statement{} |]'.format(
            statement_count,
            's' if statement_count > 1 else '',
        )
