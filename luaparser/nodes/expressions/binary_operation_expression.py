from typing import Any, Union

from luaparser import tokens
from luaparser.nodes.expressions.expression import Expression

BinaryOperator = Union[(
    tokens.AndKeyword,
    tokens.OrKeyword,
    tokens.Equal,
    tokens.NotEqual,
    tokens.LowerThan,
    tokens.LowerOrEqualThan,
    tokens.GreaterThan,
    tokens.GreaterOrEqualThan,
    tokens.Plus,
    tokens.Minus,
    tokens.Asterisk,
    tokens.Slash,
    tokens.Percent,
    tokens.Caret,
    tokens.Concat,
)]


class BinaryOperationExpression(Expression):

    def __init__(
        self,
        left_expression: Expression,
        right_expression: Expression,
        operator: BinaryOperator,
    ):
        self._left_expression = left_expression
        self._right_expression = right_expression
        self._operator = operator

    def get_left_expression(self) -> Expression:
        return self._left_expression

    def get_right_expression(self) -> Expression:
        return self._right_expression

    def get_operator(self) -> BinaryOperator:
        return self._operator

    def __eq__(self, value: Any) -> bool:
        if not (isinstance(value, BinaryOperationExpression) and Expression.__eq__(self, value)):
            return False

        if self._operator != value._operator or self._left_expression != value._left_expression:
            return False

        return self._right_expression == value._right_expression

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return '[| BinaryOperationExpression with {} |]'.format(type(self._operator).__name__)
