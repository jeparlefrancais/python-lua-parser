from typing import Any

from luaparser import tokens
from luaparser.nodes.expressions.expression import Expression
from luaparser.nodes.expressions.prefix_expression import PrefixExpression


class ParentheseExpression(PrefixExpression):

    def __init__(
        self,
        value: Expression,
        left_parenthese: tokens.OpeningParenthese,
        right_parenthese: tokens.ClosingParenthese,
    ):
        self._value = value
        self._left_parenthese = left_parenthese
        self._right_parenthese = right_parenthese

    def get_expression(self) -> Expression:
        return self._value

    def __eq__(self, value: Any) -> bool:
        if not (isinstance(value, ParentheseExpression) and self._value == value._value):
            return False

        return self._left_parenthese == value._left_parenthese and self._right_parenthese == value._right_parenthese

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)
