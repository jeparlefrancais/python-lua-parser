from typing import Any, Optional

from luaparser import tokens
from luaparser.nodes.expressions.expression import Expression


class NumberExpression(Expression):

    def __init__(self, number_token: tokens.Number):
        self._number_token = number_token

    def get_number_token(self) -> tokens.Number:
        return self._number_token

    def __eq__(self, value: Any) -> bool:
        return isinstance(value, NumberExpression) and self._number_token == value._number_token

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return '[| Number Expression {} |]'.format(self._number_token.get_symbol())
