from typing import Any

from luaparser import tokens
from luaparser.nodes.arguments.arguments import Arguments
from luaparser.nodes.expressions.prefix_expression import PrefixExpression


class CallExpression(PrefixExpression):

    def __init__(self, value: PrefixExpression, args: Arguments):
        self._value = value
        self._args = args

    def get_prefix_expression(self) -> PrefixExpression:
        return self._value

    def get_arguments(self) -> Arguments:
        return self._args

    def __eq__(self, value: Any) -> bool:
        if not isinstance(value, CallExpression):
            return False

        return self._value == value._value and self._args == value._args

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return '[| CallExpression with argument: {} |]'.format(self._args)
