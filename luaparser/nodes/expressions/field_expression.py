from typing import Any

from luaparser import tokens
from luaparser.nodes.expressions.expression import Expression
from luaparser.nodes.expressions.prefix_expression import PrefixExpression


class FieldExpression(PrefixExpression):

    def __init__(self, value: Expression, name: tokens.Identifier, dot: tokens.Dot):
        self._value = value
        self._name = name
        self._dot = dot

    def get_value(self) -> Expression:
        return self._value

    def get_field_name(self) -> str:
        return self._name.get_name()

    def __eq__(self, value: Any) -> bool:
        if not isinstance(value, FieldExpression):
            return False

        return self._value == value._value and self._name == value._name and self._dot == value._dot

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return '[| Field Expression with {} |]'.format(self._name.get_name())
