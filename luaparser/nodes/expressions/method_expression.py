from typing import Any

from luaparser import tokens
from luaparser.nodes.arguments.arguments import Arguments
from luaparser.nodes.expressions.prefix_expression import PrefixExpression


class MethodExpression(PrefixExpression):

    def __init__(
        self,
        value: PrefixExpression,
        name: tokens.Identifier,
        arguments: Arguments,
        colon: tokens.Colon,
    ):
        self._value = value
        self._name = name
        self._arguments = arguments
        self._colon = colon

    def get_value(self) -> PrefixExpression:
        return self._value

    def get_method_name(self) -> str:
        return self._name.get_name()

    def get_arguments(self) -> Arguments:
        return self._arguments

    def __eq__(self, value: Any) -> bool:
        if not isinstance(value, MethodExpression):
            return False

        if not (self._value == value._value and self._name == value._name):
            return False

        return self._arguments == value._arguments and self._colon == value._colon

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)
