from typing import Any

from luaparser import tokens
from luaparser.nodes.expressions.expression import Expression


class TrueExpression(Expression):

    def __init__(self, true_token: tokens.TrueKeyword):
        self._true_token = true_token

    def get_true_token(self) -> tokens.TrueKeyword:
        return self._true_token

    def __eq__(self, value: Any) -> bool:
        return isinstance(value, TrueExpression) and self._true_token == value._true_token

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return '[| True Expression |]'
