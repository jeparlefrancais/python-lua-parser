from typing import Any

from luaparser import tokens
from luaparser.nodes.expressions.expression import Expression


class VarArgsExpression(Expression):

    def __init__(self, var_args_token: tokens.VarArgs):
        self._var_args_token = var_args_token

    def __eq__(self, value: Any) -> bool:
        if not isinstance(value, VarArgsExpression):
            return False

        return self._var_args_token == value._var_args_token

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)
