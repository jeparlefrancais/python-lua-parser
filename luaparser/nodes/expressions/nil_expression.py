from typing import Any

from luaparser import tokens
from luaparser.nodes.expressions.expression import Expression


class NilExpression(Expression):

    def __init__(self, nil_token: tokens.NilKeyword):
        self._nil_token = nil_token

    def __eq__(self, value: Any) -> bool:
        return isinstance(value, NilExpression) and self._nil_token == value._nil_token

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)
