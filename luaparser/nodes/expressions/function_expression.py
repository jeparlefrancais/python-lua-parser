from typing import Any, List, Optional, TYPE_CHECKING

from luaparser import tokens
from luaparser.nodes.expressions.expression import Expression

if TYPE_CHECKING:
    from luaparser.nodes.block import Block


class FunctionExpression(Expression):

    def __init__(
        self,
        block: 'Block',
        parameters: List[tokens.Identifier],
        function_token: tokens.FunctionKeyword,
        end_token: tokens.EndKeyword,
        opening_parenthese: tokens.OpeningParenthese,
        closing_parenthese: tokens.ClosingParenthese,
        parameter_commas: List[tokens.Comma] = [],
        var_args: Optional[tokens.VarArgs] = None,
    ):
        self._block = block
        self._parameters = parameters
        self._function_token = function_token
        self._end_token = end_token
        self._opening_parenthese = opening_parenthese
        self._closing_parenthese = closing_parenthese
        self._parameter_commas = parameter_commas
        self._var_args = var_args

    def get_block(self) -> 'Block':
        return self._block

    def get_parameters(self) -> List[tokens.Identifier]:
        return self._parameters

    def is_variadic(self) -> bool:
        return self._var_args is not None

    def __eq__(self, value: Any) -> bool:
        if not isinstance(value, FunctionExpression):
            return False

        if self._block != value._block or self._parameters != value._parameters:
            return False

        if self._function_token != value._function_token or self._end_token != value._end_token:
            return False

        if self._function_token != value._function_token or self._end_token != value._end_token:
            return False

        return self._var_args == value._var_args

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)
