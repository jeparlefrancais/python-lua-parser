from typing import Any, Union

from luaparser import tokens
from luaparser.nodes.expressions.expression import Expression

UnaryOperator = Union[(
    tokens.Length,
    tokens.Minus,
    tokens.NotKeyword,
)]


class UnaryOperationExpression(Expression):

    def __init__(self, operand: Expression, operator: UnaryOperator):
        self._operand = operand
        self._operator = operator

    def get_operand(self) -> Expression:
        return self._operand

    def get_operator(self) -> UnaryOperator:
        return self._operator

    def __eq__(self, value: Any) -> bool:
        if not isinstance(value, UnaryOperationExpression):
            return False

        return self._operand == value._operand and self._operator == value._operator

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return '[| UnaryOperationExpression with {} |]'.format(type(self._operator).__name__)
