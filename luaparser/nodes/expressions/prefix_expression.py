from typing import Union

from luaparser import tokens
from luaparser.nodes.expressions.expression import Expression
# from luaparser.nodes.expressions.parenthese_expression import ParentheseExpression
# from luaparser.nodes.expressions.index_expression import IndexExpression
# from luaparser.nodes.expressions.field_expression import FieldExpression


class PrefixExpression(Expression):
    pass


# PrefixExpression = Union[(
#     tokens.Identifier,
#     FieldExpression,
#     IndexExpression,
#     ParentheseExpression,
# )]
