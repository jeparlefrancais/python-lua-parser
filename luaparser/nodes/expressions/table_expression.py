from typing import Any

from luaparser import tokens
from luaparser.nodes.table_entry_list import TableEntryList
from luaparser.nodes.expressions.expression import Expression


class TableExpression(Expression):

    def __init__(
        self,
        entry_list: TableEntryList,
        left_brace: tokens.OpeningBrace,
        right_brace: tokens.ClosingBrace,
    ):
        self._entry_list = entry_list
        self._left_brace = left_brace
        self._right_brace = right_brace

    def get_entry_list(self) -> TableEntryList:
        return self._entry_list

    def __eq__(self, value: Any) -> bool:
        if not (isinstance(value, TableExpression) and self._entry_list == value._entry_list):
            return False

        return self._left_brace == value._left_brace and self._right_brace == value._right_brace

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        elements_count = self._entry_list.get_entries_count()
        return '[| Table of {} element{} |]'.format(
            elements_count,
            's' if elements_count > 1 else '',
        )
