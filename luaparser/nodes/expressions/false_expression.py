from typing import Any

from luaparser import tokens
from luaparser.nodes.expressions.expression import Expression


class FalseExpression(Expression):

    def __init__(self, false_token: tokens.FalseKeyword):
        self._false_token = false_token

    def get_false_token(self) -> tokens.FalseKeyword:
        return self._false_token

    def __eq__(self, value: Any) -> bool:
        return isinstance(value, FalseExpression) and self._false_token == value._false_token

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return '[| False Expression |]'
