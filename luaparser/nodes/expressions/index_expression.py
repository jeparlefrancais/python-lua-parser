from typing import Any, Union

from luaparser import tokens
from luaparser.nodes.expressions.expression import Expression
from luaparser.nodes.expressions.prefix_expression import PrefixExpression


class IndexExpression(PrefixExpression):

    def __init__(
        self,
        value: PrefixExpression,
        key: Expression,
        left_bracket: tokens.OpeningSquareBracket,
        right_bracket: tokens.ClosingSquareBracket,
    ):
        self._value = value
        self._key = key
        self._left_bracket = left_bracket
        self._right_bracket = right_bracket

    def get_value(self) -> PrefixExpression:
        return self._value

    def get_key(self) -> Expression:
        return self._key

    def __eq__(self, value: Any) -> bool:
        if not isinstance(value, IndexExpression):
            return False

        if not (self._value == value._value and self._key == value._key):
            return False

        return self._left_bracket == value._left_bracket and self._right_bracket == value._right_bracket

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)
