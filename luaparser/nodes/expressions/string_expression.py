from typing import Any, List, Union

from luaparser import tokens
from luaparser.nodes.expressions.expression import Expression

StringToken = Union[tokens.String, tokens.LongString]


class StringExpression(Expression):

    def __init__(self, string_token: StringToken):
        self._string_token = string_token

    def get_string_token(self) -> StringToken:
        return self._string_token

    def __eq__(self, value: Any) -> bool:
        if not isinstance(value, StringExpression):
            return False

        return self._string_token == value._string_token

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)
