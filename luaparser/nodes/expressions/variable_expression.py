from typing import Any

from luaparser import tokens
from luaparser.nodes.expressions.prefix_expression import PrefixExpression


class VariableExpression(PrefixExpression):

    def __init__(self, name: tokens.Identifier):
        self._name = name

    def get_name(self) -> str:
        return self._name.get_name()

    def __eq__(self, value: Any) -> bool:
        return isinstance(value, VariableExpression) and self._name == value._name

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return '[| Variable Expression: {} |]'.format(self._name.get_name())