from typing import Any, List

from luaparser import tokens
from luaparser.nodes.arguments import Arguments
from luaparser.nodes.expression_list import ExpressionList


class TupleArguments(Arguments):

    def __init__(
        self,
        expressions: ExpressionList,
        opening_parenthese: tokens.OpeningParenthese,
        closing_parenthese: tokens.ClosingParenthese,
    ):
        self._expressions = expressions
        self._opening_parenthese = opening_parenthese
        self._closing_parenthese = closing_parenthese

    def get_expressions(self) -> ExpressionList:
        return self._expressions

    def __eq__(self, value: Any) -> bool:
        if not (isinstance(value, TupleArguments) and Arguments.__eq__(self, value)):
            return False

        if self._expressions != value._expressions:
            return False

        if self._opening_parenthese != value._opening_parenthese:
            return False

        return self._closing_parenthese == value._closing_parenthese

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return '[| TupleArguments: {} |]'.format(self._expressions)
