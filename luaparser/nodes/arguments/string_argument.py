from typing import Any

from luaparser.nodes.arguments import Arguments
from luaparser.nodes.expressions.string_expression import StringExpression


class StringArgument(Arguments):

    def __init__(self, string_expression: StringExpression):
        self._string_expression = string_expression

    def get_string_expression(self) -> StringExpression:
        return self._string_expression

    def __eq__(self, value: Any) -> bool:
        if not (isinstance(value, StringArgument) and Arguments.__eq__(self, value)):
            return False

        return self._string_expression == value._string_expression

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return '[| StringArgument: {} |]'.format(self._string_expression)
