from typing import Any

from luaparser.nodes.arguments import Arguments
from luaparser.nodes.expressions.table_expression import TableExpression


class TableArgument(Arguments):

    def __init__(self, table_expression: TableExpression):
        self._table_expression = table_expression

    def get_table_expression(self) -> TableExpression:
        return self._table_expression

    def __eq__(self, value: Any) -> bool:
        if not (isinstance(value, TableArgument) and Arguments.__eq__(self, value)):
            return False

        return self._table_expression == value._table_expression

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return '[| TableArgument: {} |]'.format(self._table_expression)
