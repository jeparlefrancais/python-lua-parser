from luaparser.nodes.arguments.arguments import Arguments
from luaparser.nodes.arguments.tuple_arguments import TupleArguments
from luaparser.nodes.arguments.string_argument import StringArgument
from luaparser.nodes.arguments.table_argument import TableArgument
