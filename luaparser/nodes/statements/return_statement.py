from typing import Any, Iterator, List, Optional

from luaparser import tokens
from luaparser.nodes.expression_list import ExpressionList
from luaparser.nodes.statements.last_statement import LastStatement


class ReturnStatement(LastStatement):

    def __init__(
        self,
        return_token: tokens.ReturnKeyword,
        expressions: ExpressionList,
        semi_colon: Optional[tokens.SemiColon] = None,
    ):
        LastStatement.__init__(self, semi_colon)
        self._return_token = return_token
        self._expressions = expressions

    def get_return_token(self) -> tokens.ReturnKeyword:
        return self._return_token

    def get_expressions(self) -> ExpressionList:
        return self._expressions

    def has_expression(self) -> bool:
        return self._expressions.get_expression_count() > 0

    def __eq__(self, value: Any) -> bool:
        if not (isinstance(value, ReturnStatement) and LastStatement.__eq__(self, value)):
            return False

        return self._return_token == value._return_token and self._expressions == value._expressions

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        expression_count = self._expressions.get_expression_count()
        return '[| ReturnStatement with {} expression{} |]'.format(
            expression_count,
            's' if expression_count > 1 else '',
        )
