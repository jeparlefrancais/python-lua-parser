from typing import Any, Optional

from luaparser import tokens
from luaparser.nodes.block import Block
from luaparser.nodes.expressions.expression import Expression
from luaparser.nodes.statements.statement import Statement


class RepeatStatement(Statement):

    def __init__(
        self,
        block: Block,
        condition: Expression,
        repeat_token: tokens.RepeatKeyword,
        until_token: tokens.UntilKeyword,
        semi_colon: Optional[tokens.SemiColon] = None,
    ):
        Statement.__init__(self, semi_colon)
        self._block = block
        self._condition = condition
        self._repeat_token = repeat_token
        self._until_token = until_token

    def get_block(self) -> Block:
        return self._block

    def get_condition(self) -> Expression:
        return self._condition

    def get_repeat_token(self) -> tokens.RepeatKeyword:
        return self._repeat_token

    def get_until_token(self) -> tokens.UntilKeyword:
        return self._until_token

    def __eq__(self, value: Any) -> bool:
        if not (isinstance(value, RepeatStatement) and Statement.__eq__(self, value)):
            return False

        if self._block != value._block or self._condition != value._condition:
            return False

        return self._repeat_token == value._repeat_token and self._until_token == value._until_token

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return '[| RepeatStatement |]'
