from typing import Any, Optional

from luaparser import tokens
from luaparser.nodes.statements.statement import Statement


class LastStatement(Statement):

    def __init__(self, semi_colon: Optional[tokens.SemiColon]):
        Statement.__init__(self, semi_colon)

    def __eq__(self, value: Any) -> bool:
        return isinstance(value, LastStatement) and Statement.__eq__(self, value)

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)
