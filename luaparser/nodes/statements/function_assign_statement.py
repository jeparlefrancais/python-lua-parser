from typing import Any, List, Optional

from luaparser import tokens
from luaparser.nodes.statements.statement import Statement
from luaparser.nodes.block import Block
from luaparser.nodes.function_name import FunctionName
from luaparser.nodes.expressions.function_expression import FunctionExpression


class FunctionAssignStatement(Statement):

    def __init__(
        self,
        function_name: FunctionName,
        function: FunctionExpression,
        semi_colon: Optional[tokens.SemiColon] = None,
    ):
        Statement.__init__(self, semi_colon)
        self._function_name = function_name
        self._function = function

    def get_function_name(self) -> FunctionName:
        return self._function_name

    def get_block(self) -> Block:
        return self._function.get_block()

    def get_parameters(self) -> List[tokens.Identifier]:
        return self._function.get_parameters()

    def is_variadic(self) -> bool:
        return self._function.is_variadic()

    def __eq__(self, value: Any) -> bool:
        if not (isinstance(value, FunctionAssignStatement) and Statement.__eq__(self, value)):
            return False

        return self._function_name == value._function_name and self._function == value._function

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)
