from typing import Any, Optional

from luaparser import tokens
from luaparser.nodes.statements.last_statement import LastStatement


class BreakStatement(LastStatement):

    def __init__(
        self,
        break_token: tokens.BreakKeyword,
        semi_colon: Optional[tokens.SemiColon] = None,
    ):
        LastStatement.__init__(self, semi_colon)
        self._break_token = break_token

    def __eq__(self, value: Any) -> bool:
        if not (isinstance(value, BreakStatement) and LastStatement.__eq__(self, value)):
            return False

        return self._break_token == value._break_token

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return '[| BreakStatement |]'
