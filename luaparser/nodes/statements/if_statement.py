from typing import Any, List, Optional

from luaparser import tokens
from luaparser.nodes.block import Block
from luaparser.nodes.expressions.expression import Expression
from luaparser.nodes.node import Node
from luaparser.nodes.statements.statement import Statement


class ElseifBranch(Node):

    def __init__(
        self,
        block: Block,
        condition: Expression,
        elseif_token: tokens.ElseifKeyword,
        then_token: tokens.ThenKeyword,
    ):
        self._block = block
        self._condition = condition
        self._elseif_token = elseif_token
        self._then_token = then_token

    def get_block(self) -> Block:
        return self._block

    def get_condition(self) -> Expression:
        return self._condition

    def get_elseif_token(self) -> tokens.ElseifKeyword:
        return self._elseif_token

    def get_then_token(self) -> tokens.ThenKeyword:
        return self._then_token

    def __eq__(self, value: Any) -> bool:
        if not isinstance(value, ElseifBranch):
            return False

        if self._block != value._block or self._condition != value._condition:
            return False

        return self._elseif_token == value._elseif_token and self._then_token == value._then_token

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)


class ElseBranch(Node):

    def __init__(self, block: Block, else_token: tokens.ElseifKeyword):
        self._block = block
        self._else_token = else_token

    def get_block(self) -> Block:
        return self._block

    def get_else_token(self) -> tokens.ElseKeyword:
        return self._else_token

    def __eq__(self, value: Any) -> bool:
        if not isinstance(value, ElseBranch):
            return False

        return self._block == value._block and self._else_token == value._else_token

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)


class IfStatement(Statement):

    def __init__(
        self,
        block: Block,
        condition: Expression,
        elseif_branches: List[ElseifBranch],
        else_branch: Optional[ElseBranch],
        if_token: tokens.IfKeyword,
        then_token: tokens.ThenKeyword,
        end_token: tokens.EndKeyword,
        semi_colon: Optional[tokens.SemiColon] = None,
    ):
        Statement.__init__(self, semi_colon)
        self._block = block
        self._condition = condition
        self._elseif_branches = elseif_branches
        self._else_branch = else_branch
        self._if_token = if_token
        self._then_token = then_token
        self._end_token = end_token

    def get_block(self) -> Block:
        return self._block

    def get_condition(self) -> Expression:
        return self._condition

    def get_elseif_branches(self) -> List[ElseifBranch]:
        return self._elseif_branches

    def has_else_branch(self) -> bool:
        return self._else_branch is not None

    def get_else_branch(self) -> Optional[ElseBranch]:
        return self._else_branch

    def get_if_token(self) -> tokens.IfKeyword:
        return self._if_token

    def get_then_token(self) -> tokens.ThenKeyword:
        return self._then_token

    def get_end_token(self) -> tokens.EndKeyword:
        return self._end_token

    def __eq__(self, value: Any) -> bool:
        if not (isinstance(value, IfStatement) and Statement.__eq__(self, value)):
            return False

        if self._block != value._block or self._condition != value._condition:
            return False

        if self._elseif_branches != value._elseif_branches or self._if_token != value._if_token:
            return False

        if self._else_branch != value._else_branch or self._then_token != value._then_token:
            return False

        return self._end_token == value._end_token

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        branch_count = len(self._elseif_branches) + 1
        return '[| IfStatement with {} branch{}{} |]'.format(
            branch_count,
            'es' if branch_count > 1 else '',
            ' and else block' if self.has_else_branch() else '',
        )
