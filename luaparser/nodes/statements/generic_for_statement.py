from typing import Any, List, Optional

from luaparser import tokens
from luaparser.nodes.block import Block
from luaparser.nodes.expression_list import ExpressionList
from luaparser.nodes.name_list import NameList
from luaparser.nodes.statements.for_statement import ForStatement


class GenericForStatement(ForStatement):

    def __init__(
        self,
        variables: NameList,
        expressions: ExpressionList,
        block: Block,
        for_token: tokens.ForKeyword,
        in_token: tokens.InKeyword,
        do_token: tokens.DoKeyword,
        end_token: tokens.EndKeyword,
        semi_colon: Optional[tokens.SemiColon] = None,
    ):
        ForStatement.__init__(self, block, for_token, do_token, end_token, semi_colon)
        self._variables = variables
        self._expressions = expressions
        self._in_token = in_token

    def get_variables(self) -> NameList:
        return self._variables

    def get_expressions(self) -> ExpressionList:
        return self._expressions

    def __eq__(self, value: Any) -> bool:
        if not (isinstance(value, GenericForStatement) and ForStatement.__eq__(self, value)):
            return False

        if self._variables != value._variables or self._expressions != value._expressions:
            return False

        return self._in_token == value._in_token

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return '[| GenericForStatement |]'
