from typing import Any, Optional

from luaparser import tokens
from luaparser.nodes.expression_list import ExpressionList
from luaparser.nodes.name_list import NameList
from luaparser.nodes.statements.statement import Statement


class LocalVariableAssignStatement(Statement):

    def __init__(
        self,
        names: NameList,
        expressions: Optional[ExpressionList],
        local_token: tokens.LocalKeyword,
        assign_token: Optional[tokens.Assign],
        semi_colon: Optional[tokens.SemiColon] = None,
    ):
        Statement.__init__(self, semi_colon)
        self._names = names
        self._expressions = expressions
        self._local_token = local_token
        self._assign_token = assign_token

    def get_names(self) -> NameList:
        return self._names

    def get_expressions(self) -> Optional[ExpressionList]:
        return self._expressions

    def get_local_token(self) -> Optional[tokens.Assign]:
        return self._local_token

    def get_assign_token(self) -> Optional[tokens.Assign]:
        return self._assign_token

    def __eq__(self, value: Any) -> bool:
        if not (isinstance(value, LocalVariableAssignStatement) and Statement.__eq__(self, value)):
            return False

        if not (self._names == value._names and self._expressions == value._expressions):
            return False

        return self._local_token == value._local_token and self._assign_token == value._assign_token

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        name_count = self._names.get_name_count()
        return '[| LocalVariableAssignStatement of {} variable{} |]'.format(
            name_count,
            's' if name_count > 1 else '',
        )
