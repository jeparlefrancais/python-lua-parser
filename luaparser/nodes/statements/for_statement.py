from typing import Any, Optional

from luaparser import tokens
from luaparser.nodes.block import Block
from luaparser.nodes.statements.statement import Statement


class ForStatement(Statement):

    def __init__(
        self,
        _block: Block,
        for_token: tokens.ForKeyword,
        do_token: tokens.DoKeyword,
        end_token: tokens.EndKeyword,
        semi_colon: Optional[tokens.SemiColon] = None,
    ):
        Statement.__init__(self, semi_colon)
        self._block = _block
        self._for_token = for_token
        self._do_token = do_token
        self._end_token = end_token

    def get_block(self) -> Block:
        return self._block

    def get_for_token(self) -> tokens.ForKeyword:
        return self._for_token

    def get_do_token(self) -> tokens.DoKeyword:
        return self._do_token

    def get_end_token(self) -> tokens.EndKeyword:
        return self._end_token

    def __eq__(self, value: Any) -> bool:
        if not (isinstance(value, ForStatement) and Statement.__eq__(self, value)):
            return False

        if not (self._block == value._block and self._for_token == value._for_token):
            return False

        return self._do_token == value._do_token and self._end_token == value._end_token

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return '[| ForStatement |]'
