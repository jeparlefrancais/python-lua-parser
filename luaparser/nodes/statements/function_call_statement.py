from typing import Any, Optional, Union

from luaparser import tokens
from luaparser.nodes.expressions.call_expression import CallExpression
from luaparser.nodes.expressions.method_expression import MethodExpression
from luaparser.nodes.statements.statement import Statement

FunctionCallType = Union[CallExpression, MethodExpression]


class FunctionCallStatement(Statement):

    def __init__(self, call: FunctionCallType, semi_colon: Optional[tokens.SemiColon] = None):
        Statement.__init__(self, semi_colon)
        self._call = call

    def get_call_expression(self) -> FunctionCallType:
        return self._call

    def __eq__(self, value: Any) -> bool:
        if not (isinstance(value, FunctionCallStatement) and Statement.__eq__(self, value)):
            return False

        return self._call == value._call

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return '[| FunctionCallStatement |]'
