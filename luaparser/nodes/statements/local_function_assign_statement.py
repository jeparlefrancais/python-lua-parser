from typing import Any, List, Optional

from luaparser import tokens
from luaparser.nodes.block import Block
from luaparser.nodes.expressions.function_expression import FunctionExpression
from luaparser.nodes.statements.statement import Statement


class LocalFunctionAssignStatement(Statement):

    def __init__(
        self,
        name: tokens.Identifier,
        function: FunctionExpression,
        local_token: tokens.LocalKeyword,
        semi_colon: Optional[tokens.SemiColon] = None,
    ):
        Statement.__init__(self, semi_colon)
        self._name = name
        self._function = function
        self._local_token = local_token

    def get_block(self) -> Block:
        return self._function.get_block()

    def get_function_name(self) -> str:
        return self._name.get_name()

    def get_parameters(self) -> List[tokens.Identifier]:
        return self._function.get_parameters()

    def is_variadic(self) -> bool:
        return self._function.is_variadic()

    def __eq__(self, value: Any) -> bool:
        if not (isinstance(value, LocalFunctionAssignStatement) and Statement.__eq__(self, value)):
            return False

        if not (self._name == value._name and self._function == value._function):
            return False

        return self._local_token == value._local_token

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)
