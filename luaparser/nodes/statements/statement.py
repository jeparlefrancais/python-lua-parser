from typing import Any, Optional

from luaparser import tokens
from luaparser.nodes.node import Node


class Statement(Node):

    def __init__(self, semi_colon: Optional[tokens.SemiColon]):
        self._semi_colon = semi_colon

    def has_semi_colon(self) -> bool:
        return self._semi_colon is not None

    def get_semi_colon(self) -> Optional[tokens.SemiColon]:
        return self._semi_colon

    def __eq__(self, value: Any):
        return isinstance(value, Statement) and self._semi_colon == value._semi_colon

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)
