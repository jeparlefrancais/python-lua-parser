from typing import Any, Optional

from luaparser import tokens
from luaparser.nodes.expression_list import ExpressionList
from luaparser.nodes.variable_list import VariableList
from luaparser.nodes.statements.statement import Statement


class VariableAssignStatement(Statement):

    def __init__(
        self,
        variables: VariableList,
        expressions: ExpressionList,
        assign_token: tokens.Assign,
        semi_colon: Optional[tokens.SemiColon] = None,
    ):
        Statement.__init__(self, semi_colon)
        self._variables = variables
        self._expressions = expressions
        self._assign_token = assign_token

    def get_variables(self) -> VariableList:
        return self._variables

    def get_expressions(self) -> ExpressionList:
        return self._expressions

    def get_assign_token(self) -> tokens.Assign:
        return self._assign_token

    def __eq__(self, value: Any) -> bool:
        if not (isinstance(value, VariableAssignStatement) and Statement.__eq__(self, value)):
            return False

        if self._variables != value._variables or self._expressions != value._expressions:
            return False

        return self._assign_token == value._assign_token

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return '[| VariableAssignStatement |]'
