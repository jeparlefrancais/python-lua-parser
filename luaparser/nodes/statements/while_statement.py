from typing import Any, Optional

from luaparser import tokens
from luaparser.nodes.block import Block
from luaparser.nodes.statements.statement import Statement
from luaparser.nodes.expressions.expression import Expression


class WhileStatement(Statement):

    def __init__(
        self,
        condition: Expression,
        block: Block,
        while_token: tokens.WhileKeyword,
        do_token: tokens.DoKeyword,
        end_token: tokens.EndKeyword,
        semi_colon: Optional[tokens.SemiColon] = None,
    ):
        Statement.__init__(self, semi_colon)
        self._condition = condition
        self._block = block
        self._while_token = while_token
        self._do_token = do_token
        self._end_token = end_token

    def get_block(self) -> Block:
        return self._block

    def get_condition(self) -> Expression:
        return self._condition

    def get_while_token(self) -> tokens.WhileKeyword:
        return self._while_token

    def get_do_token(self) -> tokens.DoKeyword:
        return self._do_token

    def get_end_token(self) -> tokens.EndKeyword:
        return self._end_token

    def __eq__(self, value: Any) -> bool:
        if not (isinstance(value, WhileStatement) and Statement.__eq__(self, value)):
            return False

        if self._condition != value._condition or self._block != value._block:
            return False

        if self._while_token != value._while_token or self._do_token != value._do_token:
            return False

        return self._end_token == value._end_token

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return '[| WhileStatement |]'
