from typing import Any, Optional

from luaparser import tokens
from luaparser.nodes.block import Block
from luaparser.nodes.expressions.expression import Expression
from luaparser.nodes.statements.for_statement import ForStatement


class NumericForStatement(ForStatement):

    def __init__(
        self,
        variable: tokens.Identifier,
        block: Block,
        start_expression: Expression,
        end_expression: Expression,
        step_expression: Optional[Expression],
        for_token: tokens.ForKeyword,
        do_token: tokens.DoKeyword,
        end_token: tokens.EndKeyword,
        assign: tokens.Assign,
        start_end_comma: tokens.Comma,
        end_step_comma: Optional[tokens.Comma],
        semi_colon: Optional[tokens.SemiColon] = None,
    ):
        ForStatement.__init__(self, block, for_token, do_token, end_token, semi_colon)
        self._variable = variable
        self._start_expression = start_expression
        self._end_expression = end_expression
        self._step_expression = step_expression
        self._assign = assign
        self._start_end_comma = start_end_comma
        self._end_step_comma = end_step_comma

    def get_variable(self) -> tokens.Identifier:
        return self._variable

    def get_start_expression(self) -> Expression:
        return self._start_expression

    def get_end_expression(self) -> Expression:
        return self._end_expression

    def get_step_expression(self) -> Optional[Expression]:
        return self._step_expression

    def get_assign_token(self) -> tokens.Assign:
        return self._assign

    def get_start_end_comma(self) -> tokens.Comma:
        return self._start_end_comma

    def get_end_step_comma(self) -> Optional[tokens.Comma]:
        return self._end_step_comma

    def __eq__(self, value: Any) -> bool:
        if not (isinstance(value, NumericForStatement) and ForStatement.__eq__(self, value)):
            return False

        if self._variable != value._variable or self._start_expression != value._start_expression:
            return False

        if self._end_expression != value._end_expression:
            return False

        if self._step_expression != value._step_expression:
            return False

        if self._assign != value._assign or self._start_end_comma != value._start_end_comma:
            return False

        return self._end_step_comma == value._end_step_comma

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return '[| NumericForStatement |]'
