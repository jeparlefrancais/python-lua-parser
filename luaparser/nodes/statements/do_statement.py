from typing import Any, Iterator, Optional

from luaparser import tokens
from luaparser.nodes.statements.statement import Statement
from luaparser.nodes.block import Block


class DoStatement(Statement):

    def __init__(
        self,
        block: Block,
        do_token: tokens.DoKeyword,
        end_token: tokens.EndKeyword,
        semi_colon: Optional[tokens.SemiColon] = None,
    ):
        Statement.__init__(self, semi_colon)
        self._block = block
        self._do_token = do_token
        self._end_token = end_token

    def get_block(self) -> Block:
        return self._block

    def __eq__(self, value: Any) -> bool:
        if not (isinstance(value, DoStatement) and Statement.__eq__(self, value)):
            return False

        if self._block != value._block:
            return False

        return self._do_token == value._do_token and self._end_token == value._end_token

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        statement_count = len(self._block.get_statements())
        return '[| DoStatement of {} statement{} |]'.format(
            statement_count,
            's' if statement_count > 1 else '',
        )
