from typing import Any, List, Union

from luaparser import tokens
from luaparser.nodes.node import Node
from luaparser.nodes.expressions.variable_expression import VariableExpression
from luaparser.nodes.expressions.field_expression import FieldExpression
from luaparser.nodes.expressions.index_expression import IndexExpression

VariableType = Union[VariableExpression, FieldExpression, IndexExpression]


class VariableList(Node):

    def __init__(self, variables: List[VariableType], commas: List[tokens.Comma] = []):
        self._variables = variables
        self._commas = commas

    def get_variables(self) -> List[VariableType]:
        return self._variables

    def get_commas(self) -> List[tokens.Comma]:
        return self._commas

    def __eq__(self, value: Any) -> bool:
        if not isinstance(value, VariableList):
            return False

        return self._variables == value._variables and self._commas == value._commas

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)
