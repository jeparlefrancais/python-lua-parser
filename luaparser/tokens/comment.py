from typing import Any

from luaparser.tokens import Token


class Comment(Token):

    def __init__(self, comment: str):
        Token.__init__(self)
        self._comment = comment

    def get_symbol(self) -> str:
        return '--{}'.format(self._comment)

    def __eq__(self, value: Any) -> bool:
        if not (isinstance(value, Comment) and Token.__eq__(self, value)):
            return False

        return self._comment == value._comment

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return '[Token:Comment -> --{}]'.format(self._comment)


class LongComment(Token):

    def __init__(self, comment: str, equal_count: int):
        Token.__init__(self)
        self._comment = comment
        self._equal_count = equal_count

    def get_symbol(self) -> str:
        equals = '=' * self._equal_count
        return '--[{}[{}]{}]'.format(equals, self._comment, equals)

    def __eq__(self, value: Any) -> bool:
        if isinstance(value, LongComment) and Token.__eq__(self, value):
            return self._comment == value._comment and self._equal_count == value._equal_count
        else:
            return False

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return '[Token:LongComment -> --[{}[{}]{}]]'.format(
            '=' * self._equal_count,
            self._comment,
            '=' * self._equal_count,
        )
