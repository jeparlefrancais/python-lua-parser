from typing import Any

from luaparser.tokens import Token


class String(Token):

    def __init__(self, string: str, single_quotes: bool):
        Token.__init__(self)
        self._string = string
        self._single_quotes = single_quotes

    def get_string(self) -> str:
        return self._string

    def is_single_quotes(self) -> bool:
        return self._single_quotes

    def get_symbol(self) -> str:
        quotes = "'" if self._single_quotes else '"'
        return '{}{}{}'.format(quotes, self._string, quotes)

    def __eq__(self, value: Any) -> bool:
        if isinstance(value, String) and Token.__eq__(self, value):
            return self._string == value._string and self._single_quotes == value._single_quotes
        else:
            return False

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        quote = "'" if self._single_quotes else '"'
        return '[Token:String -> {}{}{}]'.format(quote, self._string, quote)


class LongString(Token):

    def __init__(self, string: str, equal_count: int):
        Token.__init__(self)
        self._string = string
        self._equal_count = equal_count

    def get_string(self) -> str:
        return self._string

    def get_equal_count(self) -> int:
        return self._equal_count

    def get_symbol(self) -> str:
        equals = '=' * self._equal_count
        return '[{}[{}]{}]'.format(equals, self._string, equals)

    def __eq__(self, value: Any) -> bool:
        if isinstance(value, LongString) and Token.__eq__(self, value):
            return self._string == value._string and self._equal_count == value._equal_count
        else:
            return False

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        equals = '=' * self._equal_count
        return self._get_repr('LongString', '[{}[{}]{}]'.format(equals, self._string, equals))
