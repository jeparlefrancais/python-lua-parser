from typing import Any


class Token():

    def __init__(self):
        self._line_number = 0
        self._whitespaces = ''

    def set_line_number(self, line_number: int):
        self._line_number = line_number

    def get_line_number(self) -> int:
        return self._line_number

    def set_preceding_whitespaces(self, whitespaces: str):
        self._whitespaces = whitespaces

    def get_preceding_whitespaces(self) -> str:
        return self._whitespaces

    def get_symbol(self) -> str:
        raise NotImplementedError()

    def _get_repr(self, token_name: str, desc: str = '') -> str:
        return '[| Token at line {}: {}{} |]'.format(
            self._line_number,
            token_name,
            '' if len(desc) == 0 else ' -> {}'.format(desc),
        )

    def __eq__(self, value: Any) -> bool:
        if not isinstance(value, Token):
            return False

        return self._line_number == value._line_number and self._whitespaces == value._whitespaces

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)
