from typing import Any

from luaparser.tokens import Token


class SemiColon(Token):

    def __init__(self):
        Token.__init__(self)

    def get_symbol(self):
        return ';'

    def __eq__(self, value: Any) -> bool:
        return isinstance(value, SemiColon) and Token.__eq__(self, value)

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return self._get_repr('SemiColon')


class Colon(Token):

    def __init__(self):
        Token.__init__(self)

    def get_symbol(self):
        return ':'

    def __eq__(self, value: Any) -> bool:
        return isinstance(value, Colon) and Token.__eq__(self, value)

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return self._get_repr('Colon')


class Comma(Token):

    def __init__(self):
        Token.__init__(self)

    def get_symbol(self):
        return ','

    def __eq__(self, value: Any) -> bool:
        return isinstance(value, Comma) and Token.__eq__(self, value)

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return self._get_repr('Comma')


class Dot(Token):

    def __init__(self):
        Token.__init__(self)

    def get_symbol(self):
        return '.'

    def __eq__(self, value: Any) -> bool:
        return isinstance(value, Dot) and Token.__eq__(self, value)

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return self._get_repr('Dot')


class VarArgs(Token):

    def __init__(self):
        Token.__init__(self)

    def get_symbol(self):
        return '...'

    def __eq__(self, value: Any) -> bool:
        return isinstance(value, VarArgs) and Token.__eq__(self, value)

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return self._get_repr('VarArgs')


class OpeningSquareBracket(Token):

    def __init__(self):
        Token.__init__(self)

    def get_symbol(self):
        return '['

    def __eq__(self, value: Any) -> bool:
        return isinstance(value, OpeningSquareBracket) and Token.__eq__(self, value)

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return self._get_repr('OpeningSquareBracket', '[')


class ClosingSquareBracket(Token):

    def __init__(self):
        Token.__init__(self)

    def get_symbol(self):
        return ']'

    def __eq__(self, value: Any) -> bool:
        return isinstance(value, ClosingSquareBracket) and Token.__eq__(self, value)

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return self._get_repr('ClosingSquareBracket', ']')


class OpeningBrace(Token):

    def __init__(self):
        Token.__init__(self)

    def get_symbol(self):
        return '{'

    def __eq__(self, value: Any) -> bool:
        return isinstance(value, OpeningBrace) and Token.__eq__(self, value)

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return self._get_repr('OpeningBrace', '{')


class ClosingBrace(Token):

    def __init__(self):
        Token.__init__(self)

    def get_symbol(self):
        return '}'

    def __eq__(self, value: Any) -> bool:
        return isinstance(value, ClosingBrace) and Token.__eq__(self, value)

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return self._get_repr('ClosingBrace', '}')


class OpeningParenthese(Token):

    def __init__(self):
        Token.__init__(self)

    def get_symbol(self):
        return '('

    def __eq__(self, value: Any) -> bool:
        return isinstance(value, OpeningParenthese) and Token.__eq__(self, value)

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return self._get_repr('OpeningParenthese', '(')


class ClosingParenthese(Token):

    def __init__(self):
        Token.__init__(self)

    def get_symbol(self):
        return ')'

    def __eq__(self, value: Any) -> bool:
        return isinstance(value, ClosingParenthese) and Token.__eq__(self, value)

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return self._get_repr('ClosingParenthese', ')')


class EndOfFile(Token):

    def __init__(self):
        Token.__init__(self)

    def get_symbol(self):
        return '<end of file>'

    def __eq__(self, value: Any) -> bool:
        return isinstance(value, EndOfFile) and Token.__eq__(self, value)

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return self._get_repr('EndOfFile')
