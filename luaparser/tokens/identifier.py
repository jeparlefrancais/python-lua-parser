from typing import Any

from luaparser.tokens import Token


class Identifier(Token):

    def __init__(self, name: str):
        Token.__init__(self)
        self._name = name

    def get_name(self) -> str:
        return self._name

    def get_symbol(self) -> str:
        return self._name

    def __eq__(self, value: Any) -> bool:
        if not (isinstance(value, Identifier) and Token.__eq__(self, value)):
            return False

        return self._name == value._name

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return self._get_repr('Identifier', self._name)
