from typing import Any

from luaparser.tokens import Token


class Assign(Token):

    def __init__(self):
        Token.__init__(self)

    def get_symbol(self) -> str:
        return '='

    def __eq__(self, value: Any) -> bool:
        return isinstance(value, Assign) and Token.__eq__(self, value)

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return self._get_repr('Operator', '=')


class Equal(Token):

    def __init__(self):
        Token.__init__(self)

    def get_symbol(self) -> str:
        return '=='

    def __eq__(self, value: Any) -> bool:
        return isinstance(value, Equal) and Token.__eq__(self, value)

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return self._get_repr('Operator', '==')


class NotEqual(Token):

    def __init__(self):
        Token.__init__(self)

    def get_symbol(self) -> str:
        return '~='

    def __eq__(self, value: Any) -> bool:
        return isinstance(value, NotEqual) and Token.__eq__(self, value)

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return self._get_repr('Operator', '~=')


class LowerThan(Token):

    def __init__(self):
        Token.__init__(self)

    def get_symbol(self) -> str:
        return '<'

    def __eq__(self, value: Any) -> bool:
        return isinstance(value, LowerThan) and Token.__eq__(self, value)

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return self._get_repr('Operator', '<')


class LowerOrEqualThan(Token):

    def __init__(self):
        Token.__init__(self)

    def get_symbol(self) -> str:
        return '<='

    def __eq__(self, value: Any) -> bool:
        return isinstance(value, LowerOrEqualThan) and Token.__eq__(self, value)

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return self._get_repr('Operator', '<=')


class GreaterThan(Token):

    def __init__(self):
        Token.__init__(self)

    def get_symbol(self) -> str:
        return '>'

    def __eq__(self, value: Any) -> bool:
        return isinstance(value, GreaterThan) and Token.__eq__(self, value)

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return self._get_repr('Operator', '>')


class GreaterOrEqualThan(Token):

    def __init__(self):
        Token.__init__(self)

    def get_symbol(self) -> str:
        return '>='

    def __eq__(self, value: Any) -> bool:
        return isinstance(value, GreaterOrEqualThan) and Token.__eq__(self, value)

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return self._get_repr('Operator', '>=')


class Plus(Token):

    def __init__(self):
        Token.__init__(self)

    def get_symbol(self) -> str:
        return '+'

    def __eq__(self, value: Any) -> bool:
        return isinstance(value, Plus) and Token.__eq__(self, value)

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return self._get_repr('Operator', '+')


class Minus(Token):

    def __init__(self):
        Token.__init__(self)

    def get_symbol(self) -> str:
        return '-'

    def __eq__(self, value: Any) -> bool:
        return isinstance(value, Minus) and Token.__eq__(self, value)

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return self._get_repr('Operator', '-')


class Asterisk(Token):

    def __init__(self):
        Token.__init__(self)

    def get_symbol(self) -> str:
        return '*'

    def __eq__(self, value: Any) -> bool:
        return isinstance(value, Asterisk) and Token.__eq__(self, value)

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return self._get_repr('Operator', '*')


class Slash(Token):

    def __init__(self):
        Token.__init__(self)

    def get_symbol(self) -> str:
        return '/'

    def __eq__(self, value: Any) -> bool:
        return isinstance(value, Slash) and Token.__eq__(self, value)

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return self._get_repr('Operator', '/')


class Percent(Token):

    def __init__(self):
        Token.__init__(self)

    def get_symbol(self) -> str:
        return '%'

    def __eq__(self, value: Any) -> bool:
        return isinstance(value, Percent) and Token.__eq__(self, value)

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return self._get_repr('Operator', '%')


class Caret(Token):

    def __init__(self):
        Token.__init__(self)

    def get_symbol(self) -> str:
        return '^'

    def __eq__(self, value: Any) -> bool:
        return isinstance(value, Caret) and Token.__eq__(self, value)

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return self._get_repr('Operator', '^')


class Concat(Token):

    def __init__(self):
        Token.__init__(self)

    def get_symbol(self) -> str:
        return '..'

    def __eq__(self, value: Any) -> bool:
        return isinstance(value, Concat) and Token.__eq__(self, value)

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return self._get_repr('Operator', '..')


class Length(Token):

    def __init__(self):
        Token.__init__(self)

    def get_symbol(self) -> str:
        return '#'

    def __eq__(self, value: Any) -> bool:
        return isinstance(value, Length) and Token.__eq__(self, value)

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return self._get_repr('Operator', '#')
