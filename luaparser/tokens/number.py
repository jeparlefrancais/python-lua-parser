from typing import Any, Optional

from luaparser.tokens import Token


class Number(Token):

    def __init__(
        self,
        value: float,
        symbol: str,
        is_hex: bool = False,
        exponent: Optional[int] = None,
    ):
        Token.__init__(self)
        self._value = value
        self._symbol = symbol
        self._is_hex = is_hex
        self._exponent = exponent

    def get_value(self) -> float:
        return self._value

    def get_symbol(self) -> str:
        return self._symbol

    def get_exponent(self) -> Optional[int]:
        return self._exponent

    def has_exponent(self) -> bool:
        return self._exponent is not None

    def is_hexadecimal(self) -> bool:
        return self._is_hex

    def __eq__(self, value: Any) -> bool:
        if not (isinstance(value, Number) and Token.__eq__(self, value)):
            return False

        if self._value != value._value or self._symbol != value._symbol:
            return False

        if self._is_hex != value._is_hex or self._exponent != value._exponent:
            return False

        return True

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return self._get_repr(
            'Number', '<{}> {} {} {}'.format(
                self._symbol,
                self._value,
                self._is_hex,
                self._exponent,
            )
        )
