from typing import Any, Set, NewType

from luaparser.tokens import Token

_keywords: Set[str] = {
    'and',
    'break',
    'do',
    'else',
    'elseif',
    'end',
    'false',
    'for',
    'function',
    'if',
    'in',
    'local',
    'nil',
    'not',
    'or',
    'repeat',
    'return',
    'then',
    'true',
    'until',
    'while',
}


class Keyword(Token):

    def __init__(self, keyword: str):
        Token.__init__(self)
        self._keyword = keyword

    def get_symbol(self) -> str:
        return self._keyword

    def __eq__(self, value: Any) -> bool:
        if not (isinstance(value, Keyword) and Token.__eq__(self, value)):
            return False

        return self._keyword == value._keyword

    def __ne__(self, value: Any) -> bool:
        return not self.__eq__(value)

    def __repr__(self) -> str:
        return self._get_repr('Keyword', self._keyword)


def _create_keyword_class(keyword):
    class_name = '{}Keyword'.format(keyword.capitalize())

    def init(self):
        Keyword.__init__(self, keyword)

    def eq(self, value: Any) -> bool:
        return isinstance(value, globals()[class_name]) and Keyword.__eq__(self, value)

    def ne(self, value: Any) -> bool:
        return not self.__eq__(value)

    def representation(self) -> str:
        return self._get_repr(class_name)

    globals()[class_name] = type(
        class_name, (Keyword,), {
            '__init__': init,
            '__eq__': eq,
            '__ne__': ne,
            '__repr__': representation,
        }
    )


for keyword in _keywords:
    _create_keyword_class(keyword)
