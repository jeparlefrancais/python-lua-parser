from typing import Callable, Dict, Set

from luaparser import tokens
from luaparser.exceptions import InvalidLongStringDelimiterException, UnexpectedSymbolException
from luaparser.lexer_state import LexerState
from luaparser.lexer_tokenizers import (
    Tokenizer,
    LongStringTokenizer,
    CommentTokenizer,
    NumberTokenizer,
)

_single_symbol_map: Dict[str, Callable[[], tokens.Token]] = {
    ']': lambda: tokens.ClosingSquareBracket(),
    '{': lambda: tokens.OpeningBrace(),
    '}': lambda: tokens.ClosingBrace(),
    '(': lambda: tokens.OpeningParenthese(),
    ')': lambda: tokens.ClosingParenthese(),
    ',': lambda: tokens.Comma(),
    ':': lambda: tokens.Colon(),
    ';': lambda: tokens.SemiColon(),
    '+': lambda: tokens.Plus(),
    '*': lambda: tokens.Asterisk(),
    '/': lambda: tokens.Slash(),
    '%': lambda: tokens.Percent(),
    '^': lambda: tokens.Caret(),
    '#': lambda: tokens.Length(),
}

_single_undetermined_map: Dict[str, Callable[[], tokens.Token]] = {
    '=': lambda: tokens.Assign(),
    '<': lambda: tokens.LowerThan(),
    '>': lambda: tokens.GreaterThan(),
}

_double_symbol_map: Dict[str, Callable[[], tokens.Token]] = {
    '==': lambda: tokens.Equal(),
    '~=': lambda: tokens.NotEqual(),
    '<=': lambda: tokens.LowerOrEqualThan(),
    '>=': lambda: tokens.GreaterOrEqualThan(),
}

_double_symbol_first_chars: Set[str] = set(symbol[0] for symbol in _double_symbol_map)
_single_undetermined_set: Set[str] = set(
    _single_undetermined_map.keys()
) | _double_symbol_first_chars
_single_undetermined_set.add('~')


class OperatorSeparatorTokenizer(Tokenizer):

    def __init__(self):
        self._symbol = ''

    def parse(self, lexer: LexerState):
        first_char = lexer.get_current_char()
        self._symbol = first_char

        if first_char == '[':
            self._resolve_opening_square_bracket(lexer)
        elif first_char == '-':
            self._resolve_minus(lexer)
        elif first_char == '.':
            self._resolve_dot(lexer)
        else:
            self._resolve_char(lexer, first_char)

    def _resolve_char(self, lexer: LexerState, first_char: str):
        lexer.next_char()
        constructor = _single_symbol_map.get(first_char)

        if constructor is not None:
            lexer.append_token(constructor())
        else:
            constructor = _single_undetermined_map.get(first_char)

            if constructor is None and first_char not in _single_undetermined_set:
                self._raise_exception(lexer)

            second_char = lexer.get_current_char()
            symbol = first_char + second_char
            double_constructor = _double_symbol_map.get(symbol)

            if double_constructor is not None:
                lexer.next_char()
                lexer.append_token(double_constructor())
            else:
                if constructor is not None:
                    lexer.append_token(constructor())
                else:
                    self._raise_exception(lexer)

    def _resolve_opening_square_bracket(self, lexer: LexerState):
        lexer.next_char()
        index = lexer.find('[')

        if index < 0:
            lexer.append_token(tokens.OpeningSquareBracket())
        else:
            sub_string = lexer.sub_string(end=index)

            if all([c == '=' for c in sub_string]):
                length = len(sub_string)
                lexer.next_char(length + 1)
                tokenizer = LongStringTokenizer(length)
                tokenizer.parse(lexer)
            else:
                if sub_string[0] == '=':
                    for i, c in enumerate(sub_string):
                        if c != '=':
                            raise InvalidLongStringDelimiterException(
                                lexer.get_line_number(),
                                '[' + '=' * (i + 1),
                            )
                else:
                    lexer.append_token(tokens.OpeningSquareBracket())

    def _resolve_minus(self, lexer: LexerState):
        lexer.next_char()
        second_char = lexer.get_current_char()

        if second_char == '-':
            lexer.next_char()
            tokenizer = CommentTokenizer()
            tokenizer.parse(lexer)
        else:
            lexer.append_token(tokens.Minus())

    def _resolve_dot(self, lexer: LexerState):
        second_char = lexer.peak_next_char()

        if second_char == '.':
            lexer.next_char(2)
            third_char = lexer.get_current_char()

            if third_char == '.':
                lexer.next_char()
                lexer.append_token(tokens.VarArgs())
            else:
                lexer.append_token(tokens.Concat())
        elif second_char.isdigit():
            number_tokenizer = NumberTokenizer()
            number_tokenizer.parse(lexer)
        else:
            lexer.next_char()
            lexer.append_token(tokens.Dot())

    def _raise_exception(self, lexer: LexerState):
        raise UnexpectedSymbolException(lexer.get_line_number(), self._symbol)
