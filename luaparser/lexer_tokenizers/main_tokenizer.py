from typing import List

from luaparser.lexer_state import LexerState
from luaparser.lexer_tokenizers import (
    IdentifierTokenizer,
    OperatorSeparatorTokenizer,
    NumberTokenizer,
    StringTokenizer,
    Tokenizer,
)
from luaparser.tokens import EndOfFile


class MainTokenizer(Tokenizer):

    def parse(self, lexer: LexerState):
        while lexer.next_line():
            self._parse_line(lexer)

        lexer.skip_whitespaces()
        lexer.append_token(EndOfFile())

    def _parse_line(self, lexer: LexerState):
        lexer.skip_whitespaces()

        while not lexer.has_line_ended():
            first_char = lexer.get_current_char()

            if first_char.isidentifier():
                identifier_tokenizer = IdentifierTokenizer()
                identifier_tokenizer.parse(lexer)

            elif first_char == "'" or first_char == '"':
                string_tokenizer = StringTokenizer(first_char)
                string_tokenizer.parse(lexer)

            elif first_char.isnumeric():
                number_tokenizer = NumberTokenizer()
                number_tokenizer.parse(lexer)

            else:
                operator_tokenizer = OperatorSeparatorTokenizer()
                operator_tokenizer.parse(lexer)

            lexer.skip_whitespaces()
