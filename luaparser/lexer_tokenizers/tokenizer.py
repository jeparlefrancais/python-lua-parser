from luaparser.lexer_state import LexerState


class Tokenizer():

    def parse(self, lexer: LexerState):
        raise NotImplementedError()
