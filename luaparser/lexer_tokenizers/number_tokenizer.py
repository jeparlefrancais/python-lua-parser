from typing import Optional, Set

from luaparser.exceptions import MalformedNumberException
from luaparser.lexer_state import LexerState
from luaparser.lexer_tokenizers import Tokenizer
from luaparser.tokens import Number

_hex_letter_set: Set[str] = {'a', 'b', 'c', 'd', 'e', 'f', 'A', 'B', 'C', 'D', 'E', 'F'}


class NumberTokenizer(Tokenizer):

    def __init__(self):
        self._symbol = ''

    def parse(self, lexer: LexerState):
        first_digits = lexer.accumulate_while_condition(self._is_digit)
        self._symbol = first_digits

        next_char = lexer.get_current_char()

        exponent: Optional[int] = self._parse_optional_exponent(lexer)

        if exponent is not None:
            lexer.append_token(Number(int(first_digits), self._symbol, False, exponent))

        elif first_digits == '0' and (next_char == 'x' or next_char == 'X'):
            lexer.next_char()
            self._symbol += next_char
            self._parse_hex_number(lexer)

        elif next_char == '.':
            lexer.next_char()
            self._symbol += next_char
            decimal_digits = lexer.accumulate_while_condition(self._is_digit)
            self._symbol += decimal_digits
            number_value = float(self._symbol)
            exponent = self._parse_optional_exponent(lexer)

            lexer.append_token(Number(number_value, self._symbol, False, exponent))

        elif self._is_alphanumeric(next_char):
            self._raise_exception(lexer)

        else:
            lexer.append_token(Number(int(first_digits), self._symbol))

        self._raise_if_next_char_is_incorrect(lexer)

    def _parse_hex_number(self, lexer: LexerState):
        hex_digits = lexer.accumulate_while_condition(self._hex_is_digit)
        self._symbol += hex_digits

        if len(hex_digits) == 0:
            self._raise_exception(lexer)

        exponent = self._parse_optional_exponent(lexer, 'p')

        lexer.append_token(Number(int(hex_digits, 16), self._symbol, True, exponent))

        self._raise_if_next_char_is_incorrect(lexer)

    def _parse_optional_exponent(self, lexer: LexerState, exponent_char='e') -> Optional[int]:
        next_char = lexer.get_current_char()

        if next_char.lower() != exponent_char:
            return None

        lexer.next_char()
        self._symbol += next_char
        return self._parse_exponent(lexer)

    def _parse_exponent(self, lexer: LexerState) -> int:
        next_char = lexer.get_current_char()
        negate = False

        if next_char == '+' or next_char == '-':
            lexer.next_char()
            self._symbol += next_char
            negate = next_char == '-'

        digits = lexer.accumulate_while_condition(self._is_digit)
        self._symbol += digits

        if len(digits) == 0:
            self._raise_exception(lexer)

        value = int(digits)

        return -value if negate else value

    def _is_digit(self, char: str) -> bool:
        return char.isdigit()

    def _hex_is_digit(self, char: str) -> bool:
        return char.isdigit() or char.lower() in _hex_letter_set

    def _is_alphanumeric(self, char: str) -> bool:
        return char.isalnum() or char == '_'

    def _is_alphanumeric_or_dot(self, char: str) -> bool:
        return char.isalnum() or char == '_' or char == '.'

    def _raise_if_next_char_is_incorrect(self, lexer: LexerState):
        next_char = lexer.get_current_char()

        if next_char.isalpha() or next_char == '_' or next_char == '.':
            self._raise_exception(lexer)

    def _raise_exception(self, lexer: LexerState):
        symbol = self._symbol + lexer.accumulate_while_condition(self._is_alphanumeric_or_dot)
        raise MalformedNumberException(lexer.get_line_number(), symbol)
