from luaparser.lexer_state import LexerState
from luaparser.lexer_tokenizers import Tokenizer
from luaparser.tokens import Comment, LongComment


class CommentTokenizer(Tokenizer):

    def parse(self, lexer: LexerState):
        comment = lexer.sub_string()

        if len(comment) >= 2 and comment[0] == '[':
            index = comment.find('[', 1)

            if index >= 0:
                if all(comment[i] == '=' for i in range(1, index)):
                    lexer.next_char(index + 1)
                    self._parse_long_comment(lexer, index - 1)
                    return

        lexer.append_token(Comment(comment))
        lexer.next_line()

    def _parse_long_comment(self, lexer: LexerState, equal_count: int):
        comment = lexer.accumulate_until_symbol(']{}]'.format('=' * equal_count), True)

        if comment is None:
            pass  # raise unfinish string error
        else:
            lexer.append_token(LongComment(comment, equal_count))
            lexer.next_char(equal_count + 2)
