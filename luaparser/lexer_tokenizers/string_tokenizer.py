from luaparser.exceptions import UnfinishedStringException
from luaparser.lexer_state import LexerState
from luaparser.lexer_tokenizers import Tokenizer
from luaparser.tokens import String


class StringTokenizer(Tokenizer):

    def __init__(self, quote_symbol):
        self._quote_symbol = quote_symbol

    def parse(self, lexer: LexerState):
        lexer.next_char()
        string = lexer.accumulate_until_symbol(self._quote_symbol)

        if string is None:
            if lexer.ends_with('\\'):
                pass  # check if escaped

            raise UnfinishedStringException(
                lexer.get_line_number(),
                self._quote_symbol + lexer.sub_string(),
            )
        else:
            lexer.next_char()
            lexer.append_token(String(string, self._quote_symbol == "'"))
