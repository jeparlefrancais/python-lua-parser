from typing import Dict, Callable

from luaparser import tokens
from luaparser.lexer_state import LexerState
from luaparser.lexer_tokenizers import Tokenizer

_keyword_class_map: Dict[str, Callable[[], tokens.Keyword]] = {
    'and': tokens.AndKeyword,
    'break': tokens.BreakKeyword,
    'do': tokens.DoKeyword,
    'else': tokens.ElseKeyword,
    'elseif': tokens.ElseifKeyword,
    'end': tokens.EndKeyword,
    'false': tokens.FalseKeyword,
    'for': tokens.ForKeyword,
    'function': tokens.FunctionKeyword,
    'if': tokens.IfKeyword,
    'in': tokens.InKeyword,
    'local': tokens.LocalKeyword,
    'nil': tokens.NilKeyword,
    'not': tokens.NotKeyword,
    'or': tokens.OrKeyword,
    'repeat': tokens.RepeatKeyword,
    'return': tokens.ReturnKeyword,
    'then': tokens.ThenKeyword,
    'true': tokens.TrueKeyword,
    'until': tokens.UntilKeyword,
    'while': tokens.WhileKeyword,
}


class IdentifierTokenizer(Tokenizer):

    def parse(self, lexer: LexerState):
        word = lexer.accumulate_while_condition(self._char_is_identifier)
        keyword_constructor = _keyword_class_map.get(word)

        if keyword_constructor is None:
            lexer.append_token(tokens.Identifier(word))
        else:
            lexer.append_token(keyword_constructor())

    def _char_is_identifier(self, char: str):
        return char.isalpha() or char.isdigit() or char == '_'
