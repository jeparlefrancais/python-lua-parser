from luaparser.exceptions import UnfinishedLongStringException
from luaparser.lexer_state import LexerState
from luaparser.lexer_tokenizers import Tokenizer
from luaparser.tokens import LongString


class LongStringTokenizer(Tokenizer):

    def __init__(self, equal_count: int):
        self._equal_count = equal_count

    def parse(self, lexer: LexerState):
        string = lexer.accumulate_until_symbol(']{}]'.format('=' * self._equal_count), True)

        if string is None:
            self._raise_exception(lexer)
        else:
            lexer.next_char(self._equal_count + 2)
            lexer.append_token(LongString(string, self._equal_count))

    def _raise_exception(self, lexer: LexerState):
        if lexer.has_file_ended():
            raise UnfinishedLongStringException(lexer.get_line_number(), '<end of file>')
        else:
            raise UnfinishedLongStringException(lexer.get_line_number(), lexer.sub_string())
