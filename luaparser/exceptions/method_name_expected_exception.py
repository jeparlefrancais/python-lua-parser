from luaparser.exceptions import ParserException
from luaparser.tokens import Colon


class MethodNameExpectedException(ParserException):

    def __init__(self, colon_token: Colon):
        ParserException.__init__(
            self,
            colon_token,
            'method name expected after colon',
        )
        self._colon_token = colon_token

    def get_colon_token(self) -> Colon:
        return self._colon_token
