class LexerException(Exception):

    def __init__(self, line_number: int, message: str):
        Exception.__init__(self, '{}: {}'.format(line_number, message))
        self._line_number = line_number

    def get_line_number(self) -> int:
        return self._line_number
