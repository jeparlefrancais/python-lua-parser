from luaparser.exceptions import ParserException
from luaparser.tokens import Dot, Token


class NameExpectedException(ParserException):

    def __init__(self, dot_token: Dot):
        ParserException.__init__(
            self,
            dot_token,
            'name expected after dot',
        )
        self._dot_token = dot_token

    def get_dot_token(self) -> Dot:
        return self._dot_token
