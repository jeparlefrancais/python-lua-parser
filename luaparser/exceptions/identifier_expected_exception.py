from luaparser.exceptions import ParserException
from luaparser.tokens import Token


class IdentifierExpectedException(ParserException):

    def __init__(self, previous_token: Token):
        ParserException.__init__(
            self,
            previous_token,
            "identifier expected after symbol '{}'".format(previous_token.get_symbol()),
        )
        self._previous_token = previous_token

    def get_previous_token(self) -> Token:
        return self._previous_token
