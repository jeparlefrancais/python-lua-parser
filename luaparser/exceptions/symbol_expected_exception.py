from luaparser.exceptions import ParserException
from luaparser.tokens import Token


class SymbolExpectedException(ParserException):

    def __init__(self, expected_symbol: str, next_token: Token):
        ParserException.__init__(
            self,
            next_token,
            "'{}' expected near '{}'".format(expected_symbol, next_token.get_symbol()),
        )
        self._expected_symbol = expected_symbol
        self._next_token = next_token

    def get_expected_symbol(self) -> str:
        return self._expected_symbol

    def get_next_token(self) -> Token:
        return self._next_token
