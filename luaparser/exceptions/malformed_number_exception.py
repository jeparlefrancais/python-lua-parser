from luaparser.exceptions import LexerException


class MalformedNumberException(LexerException):

    def __init__(self, line_number: int, symbol: str):
        LexerException.__init__(self, line_number, "malformed number near '{}'".format(symbol))
        self._symbol = symbol

    def get_symbol(self) -> str:
        return self._symbol