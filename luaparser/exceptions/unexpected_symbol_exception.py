from luaparser.exceptions import LexerException


class UnexpectedSymbolException(LexerException):

    def __init__(self, line_number: int, symbol: str):
        LexerException.__init__(self, line_number, "unexpected symbol near '{}'".format(symbol))
        self._symbol = symbol

    def get_symbol(self) -> str:
        return self._symbol
