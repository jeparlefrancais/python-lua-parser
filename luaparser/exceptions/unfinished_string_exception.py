from luaparser.exceptions import LexerException


class UnfinishedStringException(LexerException):

    def __init__(self, line_number: int, symbol: str):
        LexerException.__init__(
            self,
            line_number,
            "unfinished string near '{}'".format(symbol),
        )
        self._symbol = symbol

    def get_symbol(self) -> str:
        return self._symbol


class UnfinishedLongStringException(LexerException):

    def __init__(self, line_number: int, symbol: str):
        LexerException.__init__(
            self,
            line_number,
            "unfinished long string near '{}'".format(symbol),
        )
        self._symbol = symbol

    def get_symbol(self) -> str:
        return self._symbol
