from luaparser.tokens import Token


class ParserException(Exception):

    def __init__(self, token: Token, message: str):
        Exception.__init__(self, '{}: {}'.format(token.get_line_number(), message))
        self._token = token

    def get_line_number(self) -> int:
        return self._token.get_line_number()
