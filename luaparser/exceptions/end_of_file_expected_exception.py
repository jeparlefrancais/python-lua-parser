from luaparser.exceptions.symbol_expected_exception import SymbolExpectedException
from luaparser.tokens import Token


class EndOfFileExpectedException(SymbolExpectedException):

    def __init__(self, next_token: Token):
        SymbolExpectedException.__init__(self, '<end of file>', next_token)
