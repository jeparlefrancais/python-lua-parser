from luaparser.exceptions import ParserException
from luaparser.tokens import Token


class UnexpectedTokenException(ParserException):

    def __init__(self, token: Token):
        ParserException.__init__(
            self,
            token,
            "unexpected symbol near '{}'".format(token.get_symbol()),
        )
        self._token = token

    def get_token(self) -> Token:
        return self._token
