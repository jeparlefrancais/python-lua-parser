import io
from typing import List, Set

from luaparser.lexer_state import LexerState
from luaparser.lexer_tokenizers import MainTokenizer
from luaparser.tokens import Token


class FileLineIterator():

    def __init__(self, file: io.StringIO):
        self._file = file

    def __iter__(self):
        return self

    def __next__(self):
        line = self._file.readline()

        if len(line) == 0:
            raise StopIteration

        return line


class Lexer():

    def parse(self, string: str) -> List[Token]:
        line_iterator = iter(string.splitlines(keepends=True))
        state = LexerState(line_iterator)

        tokenizer = MainTokenizer()
        tokenizer.parse(state)

        return state.get_tokens()

    def parse_file(self, file: io.StringIO) -> List[Token]:
        line_iterator = FileLineIterator(file)
        state = LexerState(line_iterator)

        tokenizer = MainTokenizer()
        tokenizer.parse(state)

        return state.get_tokens()
