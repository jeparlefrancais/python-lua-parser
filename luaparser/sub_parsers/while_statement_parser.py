from typing import cast, Callable

from luaparser import nodes, sub_parsers, tokens, TokenQueue
from luaparser.exceptions import ExpressionExpectedException, SymbolExpectedException


class WhileStatementParser(sub_parsers.BaseStatementParser):

    def parse(self, token_queue: TokenQueue) -> nodes.WhileStatement:
        while_token = cast(tokens.WhileKeyword, token_queue.pop_front())
        expression_parser = sub_parsers.ExpressionParser()
        condition = expression_parser.try_parse(token_queue)

        if condition is None:
            raise ExpressionExpectedException(while_token)

        do_token = token_queue.cast_or_raise(
            tokens.DoKeyword,
            self._get_expected_do_keyword_exception,
        )
        block_parser = sub_parsers.BlockParser()
        block = block_parser.parse(token_queue, tokens.EndKeyword)
        end_token = token_queue.cast_or_raise(
            tokens.EndKeyword,
            self._get_expected_end_keyword_exception,
        )

        return nodes.WhileStatement(
            condition,
            block,
            while_token,
            do_token,
            end_token,
            self.pop_semi_colon(token_queue),
        )

    def _get_expected_end_keyword_exception(self, token_queue: TokenQueue) -> Exception:
        return SymbolExpectedException('end', token_queue.pop_front())

    def _get_expected_do_keyword_exception(self, token_queue: TokenQueue) -> Exception:
        return SymbolExpectedException('do', token_queue.pop_front())
