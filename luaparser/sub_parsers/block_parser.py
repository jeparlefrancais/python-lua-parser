from typing import List, Optional, Type

from luaparser import nodes, sub_parsers, tokens, TokenQueue
from luaparser.exceptions import EndOfFileExpectedException


class BlockParser():

    def parse(
        self,
        token_queue: TokenQueue,
        *block_ending_token_type: Type[tokens.Token],
    ) -> nodes.Block:
        statements: List[nodes.Statement] = []

        while not token_queue.is_next_instance_of(*block_ending_token_type):
            statement_parser = sub_parsers.StatementParser()
            statement = statement_parser.try_parse(token_queue)

            if statement is None:
                break
            elif isinstance(statement, nodes.LastStatement):
                statements.append(statement)
                break
            else:
                statements.append(statement)

        return nodes.Block(statements)
