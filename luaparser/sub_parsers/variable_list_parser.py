from typing import cast, List, Optional, Union

from luaparser import nodes, tokens, TokenQueue
from luaparser.exceptions import UnexpectedExpressionException
from luaparser.sub_parsers import ExpressionParser

_variable_types = (nodes.VariableExpression, nodes.FieldExpression, nodes.IndexExpression)
VariableType = Union[nodes.VariableExpression, nodes.FieldExpression, nodes.IndexExpression]


class VariableListParser():

    def parse(
        self,
        token_queue: TokenQueue,
        initial_var: Optional[nodes.Expression] = None,
    ) -> nodes.VariableList:
        if initial_var is None:
            initial_var = self._get_variable(token_queue)
        else:
            if isinstance(initial_var, _variable_types):
                cast(VariableType, initial_var)
            else:
                raise UnexpectedExpressionException(token_queue.pop_front())

        variables: List[VariableType] = [initial_var]
        commas: List[tokens.Comma] = []

        while token_queue.is_next_instance_of(tokens.Comma):
            commas.append(cast(tokens.Comma, token_queue.pop_front()))
            variables.append(self._get_variable(token_queue))

        return nodes.VariableList(variables, commas)

    def _get_variable(self, token_queue: TokenQueue) -> VariableType:
        expression_parser = ExpressionParser()
        expression = expression_parser.try_parse(token_queue)

        if expression is not None and isinstance(expression, _variable_types):
            return expression
        else:
            raise UnexpectedExpressionException(token_queue.pop_front())
