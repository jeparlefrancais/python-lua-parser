from typing import cast, List

from luaparser import tokens, TokenQueue
from luaparser.exceptions import ExpressionExpectedException
from luaparser.nodes import Expression, ExpressionList
from luaparser.sub_parsers import ExpressionParser


class ExpressionListParser():

    def parse(self, token_queue: TokenQueue) -> ExpressionList:
        expressions: List[Expression] = []
        commas: List[tokens.Comma] = []
        expression_parser = ExpressionParser()
        first_expression = expression_parser.try_parse(token_queue)

        if first_expression is not None:
            expressions.append(first_expression)

            while token_queue.is_next_instance_of(tokens.Comma):
                comma = cast(tokens.Comma, token_queue.pop_front())
                commas.append(comma)

                new_expression = expression_parser.try_parse(token_queue)

                if new_expression is None:
                    raise ExpressionExpectedException(comma)

                expressions.append(new_expression)

        return ExpressionList(expressions, commas)
