from typing import cast

from luaparser import nodes, sub_parsers, tokens, TokenQueue
from luaparser.exceptions import ExpressionExpectedException


class LocalVariableAssignStatementParser(sub_parsers.BaseStatementParser):

    def parse(
        self,
        local_token: tokens.LocalKeyword,
        token_queue: TokenQueue,
    ) -> nodes.LocalVariableAssignStatement:
        name_list_parser = sub_parsers.NameListParser()
        name_list = name_list_parser.parse(token_queue)

        if token_queue.is_next_instance_of(tokens.Assign):
            assign_token = cast(tokens.Assign, token_queue.pop_front())
            expression_list_parser = sub_parsers.ExpressionListParser()
            expression_list = expression_list_parser.parse(token_queue)

            if expression_list.get_expression_count() == 0:
                raise ExpressionExpectedException(assign_token)

            return nodes.LocalVariableAssignStatement(
                name_list,
                expression_list,
                local_token,
                assign_token,
                self.pop_semi_colon(token_queue),
            )
        else:
            return nodes.LocalVariableAssignStatement(
                name_list,
                None,
                local_token,
                None,
                self.pop_semi_colon(token_queue),
            )
