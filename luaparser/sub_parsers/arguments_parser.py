from typing import cast

from luaparser import nodes, sub_parsers, tokens, TokenQueue
from luaparser.exceptions import SymbolExpectedException, UnexpectedTokenException


class ArgumentsParser():

    def parse(self, token_queue: TokenQueue) -> nodes.Arguments:
        if token_queue.is_next_instance_of(tokens.OpeningParenthese):
            return self._parse_tuple_arguments(token_queue)
        elif token_queue.is_next_instance_of(tokens.OpeningBrace):
            return self._parse_table_argument(token_queue)
        else:
            return self._parse_string_argument(token_queue)

    def _parse_tuple_arguments(self, token_queue: TokenQueue) -> nodes.TupleArguments:
        opening_parenthese = cast(tokens.OpeningParenthese, token_queue.pop_front())
        expression_list_parser = sub_parsers.ExpressionListParser()
        expressions = expression_list_parser.parse(token_queue)

        closing_parenthese = token_queue.cast_or_raise(
            tokens.ClosingParenthese,
            self._get_expected_closing_parenthese_exception,
        )

        return nodes.TupleArguments(expressions, opening_parenthese, closing_parenthese)

    def _parse_string_argument(self, token_queue: TokenQueue) -> nodes.StringArgument:
        if token_queue.is_next_instance_of(tokens.String):
            string_token = cast(tokens.String, token_queue.pop_front())
            return nodes.StringArgument(nodes.StringExpression(string_token))
        elif token_queue.is_next_instance_of(tokens.LongString):
            long_string_token = cast(tokens.LongString, token_queue.pop_front())
            return nodes.StringArgument(nodes.StringExpression(long_string_token))
        else:
            raise UnexpectedTokenException(token_queue.pop_front())

    def _parse_table_argument(self, token_queue: TokenQueue) -> nodes.TableArgument:
        parser = sub_parsers.TableParser()
        table_expression = parser.parse(token_queue)
        return nodes.TableArgument(table_expression)

    def _get_expected_closing_parenthese_exception(self, token_queue: TokenQueue) -> Exception:
        return SymbolExpectedException(')', token_queue.pop_front())
