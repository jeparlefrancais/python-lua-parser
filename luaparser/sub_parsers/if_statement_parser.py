from typing import cast, List, Optional

from luaparser import nodes, sub_parsers, tokens, TokenQueue
from luaparser.exceptions import ExpressionExpectedException, SymbolExpectedException


class IfStatementParser(sub_parsers.BaseStatementParser):

    def parse(self, token_queue: TokenQueue) -> nodes.IfStatement:
        if_token = cast(tokens.IfKeyword, token_queue.pop_front())

        expression_parser = sub_parsers.ExpressionParser()
        if_condition = expression_parser.try_parse(token_queue)

        if if_condition is None:
            raise ExpressionExpectedException(if_token)

        if_then_token = token_queue.cast_or_raise(
            tokens.ThenKeyword,
            self._get_expected_then_exception,
        )

        block_parser = sub_parsers.BlockParser()
        if_block = block_parser.parse(
            token_queue,
            tokens.ElseifKeyword,
            tokens.ElseKeyword,
            tokens.EndKeyword,
        )
        elseif_branches: List[nodes.ElseifBranch] = []

        while token_queue.is_next_instance_of(tokens.ElseifKeyword):
            elseif_token = cast(tokens.ElseifKeyword, token_queue.pop_front())
            condition = expression_parser.try_parse(token_queue)

            if condition is None:
                raise ExpressionExpectedException(elseif_token)

            then_token = token_queue.cast_or_raise(
                tokens.ThenKeyword,
                self._get_expected_then_exception,
            )
            elseif_branches.append(
                nodes.ElseifBranch(
                    block_parser.parse(
                        token_queue,
                        tokens.ElseifKeyword,
                        tokens.ElseKeyword,
                        tokens.EndKeyword,
                    ),
                    condition,
                    elseif_token,
                    then_token,
                )
            )

        else_token: Optional[tokens.ElseKeyword] = None
        else_branch: Optional[nodes.ElseBranch] = None

        if token_queue.is_next_instance_of(tokens.ElseKeyword):
            else_token = cast(tokens.ElseKeyword, token_queue.pop_front())
            else_block = block_parser.parse(token_queue, tokens.EndKeyword)
            else_branch = nodes.ElseBranch(else_block, else_token)

        end_token = token_queue.cast_or_raise(
            tokens.EndKeyword,
            self._get_expected_end_exception,
        )

        return nodes.IfStatement(
            if_block,
            if_condition,
            elseif_branches,
            else_branch,
            if_token,
            if_then_token,
            end_token,
            self.pop_semi_colon(token_queue),
        )

    def _get_expected_then_exception(self, token_queue: TokenQueue) -> Exception:
        return SymbolExpectedException('then', token_queue.pop_front())

    def _get_expected_end_exception(self, token_queue: TokenQueue) -> Exception:
        return SymbolExpectedException('end', token_queue.pop_front())
