from typing import cast, Optional

from luaparser import sub_parsers, tokens, TokenQueue


class BaseStatementParser():

    def pop_semi_colon(self, token_queue: TokenQueue) -> Optional[tokens.SemiColon]:
        if token_queue.is_next_instance_of(tokens.SemiColon):
            return cast(tokens.SemiColon, token_queue.pop_front())
        else:
            return None
