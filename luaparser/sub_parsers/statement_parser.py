from typing import cast, Optional

from luaparser import nodes, sub_parsers, tokens, TokenQueue
from luaparser.exceptions import EndOfFileExpectedException, IdentifierExpectedException


class StatementParser(sub_parsers.BaseStatementParser):

    def try_parse(self, token_queue: TokenQueue) -> Optional[nodes.Statement]:
        if token_queue.is_next_instance_of(tokens.DoKeyword):
            return self._parse_do_statement(token_queue)
        elif token_queue.is_next_instance_of(tokens.WhileKeyword):
            return self._parse_while_statement(token_queue)
        elif token_queue.is_next_instance_of(tokens.RepeatKeyword):
            return self._parse_repeat_statement(token_queue)
        elif token_queue.is_next_instance_of(tokens.ForKeyword):
            return self._parse_for_statement(token_queue)
        elif token_queue.is_next_instance_of(tokens.ReturnKeyword):
            return self._parse_return_statement(token_queue)
        elif token_queue.is_next_instance_of(tokens.BreakKeyword):
            return self._parse_break_statement(token_queue)
        elif token_queue.is_next_instance_of(tokens.LocalKeyword):
            local_token = cast(tokens.LocalKeyword, token_queue.pop_front())

            if token_queue.is_next_instance_of(tokens.FunctionKeyword):
                return self._parse_local_function_assign_statement(local_token, token_queue)
            else:
                return self._parse_local_variable_assign_statement(local_token, token_queue)
        elif token_queue.is_next_instance_of(tokens.IfKeyword):
            return self._parse_if_statement(token_queue)
        elif token_queue.is_next_instance_of(tokens.FunctionKeyword):
            return self._parse_function_assign_statement(token_queue)
        else:
            return self._resolve_function_call_or_variable_assign_statement(token_queue)

    def _parse_do_statement(self, token_queue: TokenQueue) -> nodes.DoStatement:
        parser = sub_parsers.DoStatementParser()
        return parser.parse(token_queue)

    def _parse_while_statement(self, token_queue: TokenQueue) -> nodes.WhileStatement:
        parser = sub_parsers.WhileStatementParser()
        return parser.parse(token_queue)

    def _parse_repeat_statement(self, token_queue: TokenQueue) -> nodes.RepeatStatement:
        parser = sub_parsers.RepeatStatementParser()
        return parser.parse(token_queue)

    def _parse_for_statement(self, token_queue: TokenQueue) -> nodes.Statement:
        parser = sub_parsers.ForStatementParser()
        return parser.parse(token_queue)

    def _parse_if_statement(self, token_queue: TokenQueue) -> nodes.IfStatement:
        parser = sub_parsers.IfStatementParser()
        return parser.parse(token_queue)

    def _parse_local_function_assign_statement(
        self,
        local_token: tokens.LocalKeyword,
        token_queue: TokenQueue,
    ) -> nodes.LocalFunctionAssignStatement:
        function_token = cast(tokens.FunctionKeyword, token_queue.pop_front())
        name = token_queue.cast_or_raise(
            tokens.Identifier,
            self._get_expected_local_function_name_exception,
        )

        parser = sub_parsers.FunctionExpressionParser()
        function_expression = parser.parse(token_queue, function_token)

        return nodes.LocalFunctionAssignStatement(
            name,
            function_expression,
            local_token,
            self.pop_semi_colon(token_queue),
        )

    def _parse_local_variable_assign_statement(
        self,
        local_token: tokens.LocalKeyword,
        token_queue: TokenQueue,
    ) -> nodes.LocalVariableAssignStatement:
        local_variable_parser = sub_parsers.LocalVariableAssignStatementParser()
        return local_variable_parser.parse(local_token, token_queue)

    def _parse_function_assign_statement(
        self,
        token_queue: TokenQueue,
    ) -> nodes.FunctionAssignStatement:
        function_token = cast(tokens.FunctionKeyword, token_queue.pop_front())
        function_name_parser = sub_parsers.FunctionNameParser()
        function_name = function_name_parser.parse(token_queue)

        function_parser = sub_parsers.FunctionExpressionParser()
        function_expression = function_parser.parse(token_queue, function_token)

        return nodes.FunctionAssignStatement(
            function_name,
            function_expression,
            self.pop_semi_colon(token_queue),
        )

    def _parse_return_statement(self, token_queue: TokenQueue) -> nodes.ReturnStatement:
        return_token = cast(tokens.ReturnKeyword, token_queue.pop_front())
        expression_list_parser = sub_parsers.ExpressionListParser()
        expression_list = expression_list_parser.parse(token_queue)

        return nodes.ReturnStatement(
            return_token,
            expression_list,
            self.pop_semi_colon(token_queue),
        )

    def _parse_break_statement(self, token_queue: TokenQueue) -> nodes.BreakStatement:
        break_token = cast(tokens.BreakKeyword, token_queue.pop_front())

        return nodes.BreakStatement(break_token, self.pop_semi_colon(token_queue))

    def _resolve_function_call_or_variable_assign_statement(
        self,
        token_queue: TokenQueue,
    ) -> Optional[nodes.Statement]:
        if not token_queue.is_next_instance_of(tokens.Identifier, tokens.OpeningParenthese):
            return None

        expression_parser = sub_parsers.ExpressionParser()
        expression = expression_parser.try_parse(token_queue)

        if isinstance(expression, (nodes.CallExpression, nodes.MethodExpression)):
            return nodes.FunctionCallStatement(
                expression,
                self.pop_semi_colon(token_queue),
            )
        else:
            variable_parser = sub_parsers.VariableAssignStatementParser()
            return variable_parser.try_parse(token_queue, expression)

    def _get_expected_local_function_name_exception(self, token_queue: TokenQueue) -> Exception:
        return IdentifierExpectedException(token_queue.get_previous_token())
