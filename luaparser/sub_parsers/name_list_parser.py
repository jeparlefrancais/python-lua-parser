from typing import cast, List

from luaparser import tokens, TokenQueue
from luaparser.exceptions import IdentifierExpectedException
from luaparser.nodes import NameList


class NameListParser():

    def parse(self, token_queue: TokenQueue) -> NameList:
        names: List[tokens.Identifier] = []
        commas: List[tokens.Comma] = []

        names.append(
            token_queue.cast_or_raise(
                tokens.Identifier,
                self._get_expected_identifier_exception,
            )
        )

        while token_queue.is_next_instance_of(tokens.Comma):
            commas.append(cast(tokens.Comma, token_queue.pop_front()))

            names.append(
                token_queue.cast_or_raise(
                    tokens.Identifier,
                    self._get_expected_identifier_exception,
                )
            )

        return NameList(names, commas)

    def _get_expected_identifier_exception(self, token_queue: TokenQueue) -> Exception:
        return IdentifierExpectedException(token_queue.get_previous_token())
