from typing import cast

from luaparser import nodes, sub_parsers, tokens, TokenQueue
from luaparser.exceptions import ExpressionExpectedException, SymbolExpectedException


class RepeatStatementParser(sub_parsers.BaseStatementParser):

    def parse(self, token_queue: TokenQueue) -> nodes.RepeatStatement:
        repeat_token = cast(tokens.RepeatKeyword, token_queue.pop_front())
        block_parser = sub_parsers.BlockParser()
        block = block_parser.parse(token_queue, tokens.UntilKeyword)

        until_token = token_queue.cast_or_raise(
            tokens.UntilKeyword,
            self._get_expected_until_exception,
        )
        expression_parser = sub_parsers.ExpressionParser()
        condition = expression_parser.try_parse(token_queue)

        if condition is None:
            raise ExpressionExpectedException(until_token)

        return nodes.RepeatStatement(
            block,
            condition,
            repeat_token,
            until_token,
            self.pop_semi_colon(token_queue),
        )

    def _get_expected_until_exception(self, token_queue) -> Exception:
        return SymbolExpectedException('until', token_queue.pop_front())
