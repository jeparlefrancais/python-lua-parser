from typing import cast, Callable, Optional, Union

from luaparser import TokenQueue, nodes, tokens
from luaparser.exceptions import (
    ParserException,
    ExpressionExpectedException,
    MethodNameExpectedException,
    NameExpectedException,
    SymbolExpectedException,
)
from luaparser.sub_parsers.arguments_parser import ArgumentsParser
from luaparser.sub_parsers.function_expression_parser import FunctionExpressionParser
from luaparser.sub_parsers.table_parser import TableParser

_binary_operators = (
    tokens.OrKeyword,
    tokens.AndKeyword,
    tokens.Equal,
    tokens.NotEqual,
    tokens.LowerThan,
    tokens.LowerOrEqualThan,
    tokens.GreaterThan,
    tokens.GreaterOrEqualThan,
    tokens.Concat,
    tokens.Plus,
    tokens.Minus,
    tokens.Asterisk,
    tokens.Slash,
    tokens.Percent,
    tokens.Caret,
)
BinaryOperators = Union[(
    tokens.OrKeyword,
    tokens.AndKeyword,
    tokens.Equal,
    tokens.NotEqual,
    tokens.LowerThan,
    tokens.LowerOrEqualThan,
    tokens.GreaterThan,
    tokens.GreaterOrEqualThan,
    tokens.Concat,
    tokens.Plus,
    tokens.Minus,
    tokens.Asterisk,
    tokens.Slash,
    tokens.Percent,
    tokens.Caret,
)]

_precedence = {
    tokens.OrKeyword: 1,
    tokens.AndKeyword: 2,
    tokens.Equal: 3,
    tokens.NotEqual: 3,
    tokens.LowerThan: 3,
    tokens.LowerOrEqualThan: 3,
    tokens.GreaterThan: 3,
    tokens.GreaterOrEqualThan: 3,
    tokens.Concat: 4,
    tokens.Plus: 5,
    tokens.Minus: 5,
    tokens.Asterisk: 6,
    tokens.Slash: 6,
    tokens.Percent: 6,
    tokens.Caret: 8,
}

_unary_operator_precendence = 7


class ExpressionParser():

    def try_parse(self, token_queue: TokenQueue) -> Optional[nodes.Expression]:
        left_expression = self._try_parse_single_expression(token_queue)

        if left_expression is None:
            return None

        return self._parse_right_value(left_expression, token_queue, 0)

    def _try_parse_single_expression(self, token_queue: TokenQueue) -> Optional[nodes.Expression]:
        primary_expression = self._parse_primary_expression(token_queue)

        if primary_expression is None:
            return None

        if isinstance(primary_expression, nodes.PrefixExpression):
            new_prefix: nodes.PrefixExpression = primary_expression
            resolved = self._resolve_prefix_expression(new_prefix, token_queue)

            while id(new_prefix) != id(resolved):
                new_prefix = resolved
                resolved = self._resolve_prefix_expression(new_prefix, token_queue)

            return resolved
        else:
            return primary_expression

    def _parse_primary_expression(self, token_queue: TokenQueue) -> Optional[nodes.Expression]:
        if token_queue.is_next_instance_of(tokens.Identifier):
            return nodes.VariableExpression(cast(tokens.Identifier, token_queue.pop_front()))
        elif token_queue.is_next_instance_of(tokens.TrueKeyword):
            return nodes.TrueExpression(cast(tokens.TrueKeyword, token_queue.pop_front()))
        elif token_queue.is_next_instance_of(tokens.FalseKeyword):
            return nodes.FalseExpression(cast(tokens.FalseKeyword, token_queue.pop_front()))
        elif token_queue.is_next_instance_of(tokens.NilKeyword):
            return nodes.NilExpression(cast(tokens.NilKeyword, token_queue.pop_front()))
        elif token_queue.is_next_instance_of(tokens.Number):
            return nodes.NumberExpression(cast(tokens.Number, token_queue.pop_front()))
        elif token_queue.is_next_instance_of(tokens.String):
            return nodes.StringExpression(cast(tokens.String, token_queue.pop_front()))
        elif token_queue.is_next_instance_of(tokens.LongString):
            return nodes.StringExpression(cast(tokens.LongString, token_queue.pop_front()))
        elif token_queue.is_next_instance_of(tokens.FunctionKeyword):
            return self._parse_function_expression(token_queue)
        elif token_queue.is_next_instance_of(tokens.OpeningParenthese):
            return self._parse_parenthese_expression(token_queue)
        elif token_queue.is_next_instance_of(tokens.OpeningBrace):
            return TableParser().parse(token_queue)
        elif token_queue.is_next_instance_of(tokens.Length):
            return self._parse_unary_expression(token_queue)
        elif token_queue.is_next_instance_of(tokens.Minus):
            return self._parse_unary_expression(token_queue)
        elif token_queue.is_next_instance_of(tokens.NotKeyword):
            return self._parse_unary_expression(token_queue)
        elif token_queue.is_next_instance_of(tokens.VarArgs):
            return nodes.VarArgsExpression(cast(tokens.VarArgs, token_queue.pop_front()))

        return None

    def _resolve_prefix_expression(
        self,
        prefix: nodes.PrefixExpression,
        token_queue: TokenQueue,
    ) -> nodes.PrefixExpression:
        if token_queue.is_next_instance_of(tokens.OpeningSquareBracket):
            opening_bracket = cast(tokens.OpeningSquareBracket, token_queue.pop_front())

            parser = ExpressionParser()
            value = parser.try_parse(token_queue)

            if value is None:
                raise ExpressionExpectedException(opening_bracket)

            closing_bracket = token_queue.cast_or_raise(
                tokens.ClosingSquareBracket,
                self._get_expected_closing_square_bracket_exception,
            )

            return nodes.IndexExpression(prefix, value, opening_bracket, closing_bracket)

        elif token_queue.is_next_instance_of(tokens.Dot):
            dot_token = cast(tokens.Dot, token_queue.pop_front())
            name = token_queue.cast_or_raise(
                tokens.Identifier,
                lambda queue: self._get_expected_field_name_exception(dot_token, queue),
            )

            return nodes.FieldExpression(prefix, name, dot_token)

        elif token_queue.is_next_instance_of(
            tokens.OpeningParenthese,
            tokens.OpeningBrace,
            tokens.String,
            tokens.LongString,
        ):
            argument_parser = ArgumentsParser()
            arguments = argument_parser.parse(token_queue)

            return nodes.CallExpression(prefix, arguments)

        elif token_queue.is_next_instance_of(tokens.Colon):
            colon_token = cast(tokens.Colon, token_queue.pop_front())
            name = token_queue.cast_or_raise(
                tokens.Identifier,
                lambda queue: self._get_expected_method_name_exception(colon_token, queue),
            )
            argument_parser = ArgumentsParser()
            arguments = argument_parser.parse(token_queue)

            return nodes.MethodExpression(prefix, name, arguments, colon_token)

        else:
            return prefix

    def _parse_function_expression(self, token_queue: TokenQueue) -> nodes.FunctionExpression:
        parser = FunctionExpressionParser()
        return parser.parse(token_queue)

    def _parse_parenthese_expression(self, token_queue: TokenQueue) -> nodes.ParentheseExpression:
        opening_parenthese = cast(tokens.OpeningParenthese, token_queue.pop_front())

        parser = ExpressionParser()
        expression = parser.try_parse(token_queue)

        if expression is None:
            raise ExpressionExpectedException(opening_parenthese)

        if not token_queue.is_next_instance_of(tokens.ClosingParenthese):
            raise SymbolExpectedException(')', token_queue.pop_front())

        closing_parenthese = cast(tokens.ClosingParenthese, token_queue.pop_front())

        return nodes.ParentheseExpression(expression, opening_parenthese, closing_parenthese)

    def _parse_unary_expression(self, token_queue: TokenQueue) -> nodes.UnaryOperationExpression:
        operator = cast(nodes.UnaryOperator, token_queue.pop_front())

        next_expression = self._try_parse_single_expression(token_queue)

        if next_expression is None:
            raise ExpressionExpectedException(operator)

        expression = self._parse_right_value(
            next_expression,
            token_queue,
            _unary_operator_precendence,
        )

        return nodes.UnaryOperationExpression(expression, operator)

    def _parse_right_value(
        self,
        left_expression: nodes.Expression,
        token_queue: TokenQueue,
        min_precedence: int,
    ) -> nodes.Expression:
        while token_queue.is_next_instance_of(*_binary_operators):
            operator: BinaryOperators = cast(BinaryOperators, token_queue.front())
            operator_precedence = _precedence.get(type(operator), 0)

            if operator_precedence < min_precedence:
                break

            token_queue.pop_front()
            right_expression = self._try_parse_single_expression(token_queue)

            if right_expression is None:
                raise ExpressionExpectedException(operator)

            while token_queue.is_next_instance_of(*_binary_operators):
                next_operator: BinaryOperators = cast(BinaryOperators, token_queue.front())
                next_operator_precedence = _precedence.get(type(next_operator), 0)

                if next_operator_precedence <= operator_precedence:
                    if isinstance(next_operator, (tokens.Concat, tokens.Caret)):
                        if next_operator_precedence < operator_precedence:
                            break
                    else:
                        break

                right_expression = self._parse_right_value(
                    right_expression,
                    token_queue,
                    next_operator_precedence,
                )

            left_expression = nodes.BinaryOperationExpression(
                left_expression,
                right_expression,
                operator,
            )

        return left_expression

    def _get_expected_closing_square_bracket_exception(self, token_queue: TokenQueue) -> Exception:
        return SymbolExpectedException(']', token_queue.pop_front())

    def _get_expected_field_name_exception(
        self,
        dot_token: tokens.Dot,
        token_queue: TokenQueue,
    ) -> Exception:
        return NameExpectedException(dot_token)

    def _get_expected_method_name_exception(
        self,
        colon_token: tokens.Colon,
        token_queue: TokenQueue,
    ) -> Exception:
        return MethodNameExpectedException(colon_token)
