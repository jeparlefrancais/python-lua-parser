from typing import cast

from luaparser import tokens
from luaparser import TokenQueue
from luaparser.exceptions import UnexpectedTokenException
from luaparser.nodes import Chunk
from luaparser.sub_parsers import BlockParser


class ChunkParser():

    def parse(self, token_queue: TokenQueue) -> Chunk:
        block_parser = BlockParser()
        block = block_parser.parse(token_queue, tokens.EndOfFile)
        end_of_file = token_queue.cast_or_raise(
            tokens.EndOfFile,
            self._get_unexpected_token_exception,
        )

        return Chunk(block, end_of_file)

    def _get_unexpected_token_exception(self, token_queue: TokenQueue) -> Exception:
        return UnexpectedTokenException(token_queue.pop_front())
