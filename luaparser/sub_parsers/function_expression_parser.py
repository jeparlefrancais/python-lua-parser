from typing import cast, List, Optional

from luaparser import nodes, sub_parsers, tokens, TokenQueue
from luaparser.exceptions import InvalidFunctionParameterException, SymbolExpectedException


class FunctionExpressionParser(sub_parsers.BaseStatementParser):

    def parse(
        self,
        token_queue: TokenQueue,
        function_token: tokens.FunctionKeyword = None,
    ) -> nodes.FunctionExpression:
        if function_token is None:
            function_token = cast(tokens.FunctionKeyword, token_queue.pop_front())

        opening_parenthese = token_queue.cast_or_raise(
            tokens.OpeningParenthese,
            self._get_expected_opening_parenthese_exception,
        )
        parameters: List[tokens.Identifier] = []
        parameter_commas: List[tokens.Comma] = []
        var_args: Optional[tokens.VarArgs] = None

        if token_queue.is_next_instance_of(tokens.Identifier):
            parameters.append(cast(tokens.Identifier, token_queue.pop_front()))

            while token_queue.is_next_instance_of(tokens.Comma):
                comma = cast(tokens.Comma, token_queue.pop_front())
                parameter_commas.append(comma)

                if token_queue.is_next_instance_of(tokens.Identifier):
                    parameters.append(cast(tokens.Identifier, token_queue.pop_front()))
                elif token_queue.is_next_instance_of(tokens.VarArgs):
                    var_args = cast(tokens.VarArgs, token_queue.pop_front())
                    break
                else:
                    raise InvalidFunctionParameterException(comma)

        elif token_queue.is_next_instance_of(tokens.VarArgs):
            var_args = cast(tokens.VarArgs, token_queue.pop_front())

        closing_parenthese = token_queue.cast_or_raise(
            tokens.ClosingParenthese,
            self._get_expected_closing_parenthese_exception,
        )
        block_parser = sub_parsers.BlockParser()
        block = block_parser.parse(token_queue, tokens.EndKeyword)

        end_token = token_queue.cast_or_raise(
            tokens.EndKeyword,
            self._get_expected_end_exception,
        )

        return nodes.FunctionExpression(
            block,
            parameters,
            function_token,
            end_token,
            opening_parenthese,
            closing_parenthese,
            parameter_commas,
            var_args,
        )

    def _get_expected_closing_parenthese_exception(self, token_queue: TokenQueue) -> Exception:
        return SymbolExpectedException(')', token_queue.pop_front())

    def _get_expected_opening_parenthese_exception(self, token_queue: TokenQueue) -> Exception:
        return SymbolExpectedException('(', token_queue.pop_front())

    def _get_expected_end_exception(self, token_queue: TokenQueue) -> Exception:
        return SymbolExpectedException('end', token_queue.pop_front())
