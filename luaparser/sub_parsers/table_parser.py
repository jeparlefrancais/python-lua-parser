from typing import cast, List, Optional

from luaparser import nodes, tokens, TokenQueue, sub_parsers
from luaparser.exceptions import ExpressionExpectedException, SymbolExpectedException
from luaparser.nodes.table_entry_list import TableEntrySeparator


class TableParser():

    def parse(self, token_queue: TokenQueue) -> nodes.TableExpression:
        opening_brace = cast(tokens.OpeningBrace, token_queue.pop_front())
        entries: List[nodes.TableEntry] = []
        separators: List[TableEntrySeparator] = []

        while not token_queue.is_next_instance_of(tokens.ClosingBrace):
            entries.append(self._parse_table_entry(token_queue))

            if token_queue.is_next_instance_of(tokens.Comma, tokens.SemiColon):
                separator = cast(TableEntrySeparator, token_queue.pop_front())
                separators.append(separator)
            else:
                break

        closing_brace = token_queue.cast_or_raise(
            tokens.ClosingBrace,
            self._get_expected_closing_brace_exception,
        )

        return nodes.TableExpression(
            nodes.TableEntryList(entries, separators),
            opening_brace,
            closing_brace,
        )

    def _parse_table_entry(self, token_queue: TokenQueue) -> nodes.TableEntry:
        if token_queue.is_next_instance_of(tokens.OpeningSquareBracket):
            return self._parse_index_table_entry(token_queue)
        elif token_queue.are_next_instances_of(tokens.Identifier, tokens.Assign):
            return self._parse_field_table_entry(token_queue)
        else:
            return self._parse_value_table_entry(token_queue)

    def _parse_index_table_entry(self, token_queue: TokenQueue) -> nodes.IndexEntry:
        opening_bracket = cast(tokens.OpeningSquareBracket, token_queue.pop_front())

        key_parser = sub_parsers.ExpressionParser()
        key_expression = key_parser.try_parse(token_queue)

        if key_expression is None:
            raise ExpressionExpectedException(opening_bracket)

        closing_bracket = token_queue.cast_or_raise(
            tokens.ClosingSquareBracket,
            self._get_expected_closing_square_bracket_exception,
        )
        assign_operator = token_queue.cast_or_raise(
            tokens.Assign,
            self._get_expected_assign_exception,
        )
        value_parser = sub_parsers.ExpressionParser()
        value_expression = value_parser.try_parse(token_queue)

        if value_expression is None:
            raise ExpressionExpectedException(assign_operator)

        return nodes.IndexEntry(
            key_expression,
            value_expression,
            opening_bracket,
            closing_bracket,
            assign_operator,
        )

    def _parse_field_table_entry(self, token_queue: TokenQueue) -> nodes.FieldEntry:
        key = cast(tokens.Identifier, token_queue.pop_front())
        assign_operator = cast(tokens.Assign, token_queue.pop_front())
        value_parser = sub_parsers.ExpressionParser()
        value_expression = value_parser.try_parse(token_queue)

        if value_expression is None:
            raise ExpressionExpectedException(token_queue.get_previous_token())

        return nodes.FieldEntry(key, value_expression, assign_operator)

    def _parse_value_table_entry(self, token_queue: TokenQueue) -> nodes.ValueEntry:
        value_parser = sub_parsers.ExpressionParser()
        value_expression = value_parser.try_parse(token_queue)

        if value_expression is None:
            raise ExpressionExpectedException(token_queue.get_previous_token())

        return nodes.ValueEntry(value_expression)

    def _get_expected_closing_brace_exception(self, token_queue: TokenQueue) -> Exception:
        return SymbolExpectedException('}', token_queue.pop_front())

    def _get_expected_closing_square_bracket_exception(self, token_queue: TokenQueue) -> Exception:
        return SymbolExpectedException(']', token_queue.pop_front())

    def _get_expected_assign_exception(self, token_queue: TokenQueue) -> Exception:
        return SymbolExpectedException('=', token_queue.pop_front())
