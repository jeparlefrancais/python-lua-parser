from typing import cast

from luaparser import nodes, sub_parsers, tokens, TokenQueue
from luaparser.exceptions import SymbolExpectedException


class DoStatementParser(sub_parsers.BaseStatementParser):

    def parse(self, token_queue: TokenQueue) -> nodes.DoStatement:
        do_token = cast(tokens.DoKeyword, token_queue.pop_front())
        block_parser = sub_parsers.BlockParser()
        block = block_parser.parse(token_queue, tokens.EndKeyword)
        end_token = token_queue.cast_or_raise(
            tokens.EndKeyword,
            self._get_end_token_expected_exception,
        )

        return nodes.DoStatement(
            block,
            do_token,
            end_token,
            self.pop_semi_colon(token_queue),
        )

    def _get_end_token_expected_exception(self, token_queue: TokenQueue) -> Exception:
        return SymbolExpectedException('end', token_queue.pop_front())