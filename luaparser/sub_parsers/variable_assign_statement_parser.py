from typing import Optional

from luaparser import nodes, sub_parsers, tokens, TokenQueue
from luaparser.exceptions import ExpressionExpectedException, SymbolExpectedException


class VariableAssignStatementParser(sub_parsers.BaseStatementParser):

    def try_parse(
        self,
        token_queue: TokenQueue,
        initial_expression: Optional[nodes.Expression] = None,
    ) -> Optional[nodes.VariableAssignStatement]:
        variable_list_parser = sub_parsers.VariableListParser()
        variable_list = variable_list_parser.parse(token_queue, initial_expression)

        assign_token = token_queue.cast_or_raise(
            tokens.Assign,
            self._get_expected_assign_operator,
        )

        expression_list_parser = sub_parsers.ExpressionListParser()
        expression_list = expression_list_parser.parse(token_queue)

        if expression_list.get_expression_count() == 0:
            raise ExpressionExpectedException(assign_token)

        return nodes.VariableAssignStatement(
            variable_list,
            expression_list,
            assign_token,
            self.pop_semi_colon(token_queue),
        )

    def _get_expected_assign_operator(self, token_queue: TokenQueue) -> Exception:
        return SymbolExpectedException('=', token_queue.pop_front())
