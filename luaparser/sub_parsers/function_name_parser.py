from typing import cast, List

from luaparser import nodes, tokens, TokenQueue
from luaparser.exceptions import IdentifierExpectedException, MethodNameExpectedException


class FunctionNameParser():

    def parse(self, token_queue: TokenQueue) -> nodes.FunctionName:
        names: List[tokens.Identifier] = [
            token_queue.cast_or_raise(
                tokens.Identifier,
                self._get_expected_function_name_exception,
            )
        ]
        dots: List[tokens.Dot] = []

        while token_queue.is_next_instance_of(tokens.Dot):
            dots.append(cast(tokens.Dot, token_queue.pop_front()))
            names.append(
                token_queue.cast_or_raise(
                    tokens.Identifier,
                    self._get_expected_function_name_exception,
                )
            )

        if token_queue.is_next_instance_of(tokens.Colon):
            colon = cast(tokens.Colon, token_queue.pop_front())
            method_name = token_queue.cast_or_raise(
                tokens.Identifier,
                self._get_expected_method_name_exception,
            )
            return nodes.FunctionName(names, dots, method_name, colon)
        else:
            return nodes.FunctionName(names, dots)

    def _get_expected_function_name_exception(self, token_queue: TokenQueue) -> Exception:
        return IdentifierExpectedException(token_queue.get_previous_token())

    def _get_expected_method_name_exception(self, token_queue: TokenQueue) -> Exception:
        return MethodNameExpectedException(cast(tokens.Colon, token_queue.get_previous_token()))
