from typing import cast, Optional, Tuple

from luaparser import nodes, sub_parsers, tokens, TokenQueue
from luaparser.exceptions import (
    ExpressionExpectedException,
    EndOfFileExpectedException,
    SymbolExpectedException,
)


class ForStatementParser(sub_parsers.BaseStatementParser):

    def parse(self, token_queue: TokenQueue) -> nodes.Statement:
        for_token = cast(tokens.ForKeyword, token_queue.pop_front())

        if token_queue.are_next_instances_of(tokens.Identifier, tokens.Assign):
            return self._parse_numeric_for(for_token, token_queue)
        else:
            return self._parse_generic_for(for_token, token_queue)

    def _parse_numeric_for(
        self,
        for_token: tokens.ForKeyword,
        token_queue: TokenQueue,
    ) -> nodes.NumericForStatement:
        variable = cast(tokens.Identifier, token_queue.pop_front())
        assign_token = cast(tokens.Assign, token_queue.pop_front())

        expression_parser = sub_parsers.ExpressionParser()
        start_expression = expression_parser.try_parse(token_queue)

        if start_expression is None:
            raise ExpressionExpectedException(assign_token)

        start_end_comma = token_queue.cast_or_raise(
            tokens.Comma,
            self._get_expected_comma_exception,
        )
        end_expression = expression_parser.try_parse(token_queue)

        if end_expression is None:
            raise ExpressionExpectedException(start_end_comma)

        end_step_comma: Optional[tokens.Comma] = None
        step_expression: Optional[nodes.Expression] = None

        if token_queue.is_next_instance_of(tokens.Comma):
            end_step_comma = cast(tokens.Comma, token_queue.pop_front())
            step_expression = expression_parser.try_parse(token_queue)

            if step_expression is None:
                raise ExpressionExpectedException(end_step_comma)

        do_token, block, end_token = self._parse_for_body(token_queue)

        return nodes.NumericForStatement(
            variable,
            block,
            start_expression,
            end_expression,
            step_expression,
            for_token,
            do_token,
            end_token,
            assign_token,
            start_end_comma,
            end_step_comma,
            self.pop_semi_colon(token_queue),
        )

    def _parse_generic_for(
        self,
        for_token: tokens.ForKeyword,
        token_queue: TokenQueue,
    ) -> nodes.GenericForStatement:
        name_list_parser = sub_parsers.NameListParser()
        names = name_list_parser.parse(token_queue)

        in_token = token_queue.cast_or_raise(
            tokens.InKeyword,
            self._get_expected_in_exception,
        )

        expression_list_parser = sub_parsers.ExpressionListParser()
        expressions = expression_list_parser.parse(token_queue)

        if expressions.get_expression_count() == 0:
            raise ExpressionExpectedException(in_token)

        do_token, block, end_token = self._parse_for_body(token_queue)

        return nodes.GenericForStatement(
            names,
            expressions,
            block,
            for_token,
            in_token,
            do_token,
            end_token,
            self.pop_semi_colon(token_queue),
        )

    def _parse_for_body(
        self,
        token_queue: TokenQueue,
    ) -> Tuple[tokens.DoKeyword, nodes.Block, tokens.EndKeyword]:
        do_token = token_queue.cast_or_raise(
            tokens.DoKeyword,
            self._get_expected_do_exception,
        )

        block_parser = sub_parsers.BlockParser()
        block = block_parser.parse(token_queue, tokens.EndKeyword)

        end_token = token_queue.cast_or_raise(
            tokens.EndKeyword,
            self._get_expected_end_exception,
        )

        return do_token, block, end_token

    def _get_expected_comma_exception(self, token_queue: TokenQueue) -> Exception:
        return SymbolExpectedException(',', token_queue.pop_front())

    def _get_expected_in_exception(self, token_queue: TokenQueue) -> Exception:
        return SymbolExpectedException('in', token_queue.pop_front())

    def _get_expected_do_exception(self, token_queue: TokenQueue) -> Exception:
        return SymbolExpectedException('do', token_queue.pop_front())

    def _get_expected_end_exception(self, token_queue: TokenQueue) -> Exception:
        return SymbolExpectedException('end', token_queue.pop_front())
