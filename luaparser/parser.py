import io

from typing import List
from luaparser import nodes, TokenQueue

from luaparser import sub_parsers
from luaparser.lexer import Lexer
from luaparser.tokens import Token


class Parser():

    def __init__(self):
        self._lexer = Lexer()
        self._chunk_parser = sub_parsers.ChunkParser()

    def parse(self, string: str) -> nodes.Chunk:
        return self._parse_tokens(self._lexer.parse(string))

    def parse_file(self, file: io.StringIO) -> nodes.Chunk:
        return self._parse_tokens(self._lexer.parse_file(file))

    def _parse_tokens(self, token_list: List[Token]) -> nodes.Chunk:
        return self._chunk_parser.parse(TokenQueue(token_list))
