from typing import cast, Callable, List, NewType, Optional, Type, TypeVar

from luaparser.tokens import Token, Comment, LongComment

T = TypeVar('T', bound=Token)


class TokenQueue():

    def __init__(self, tokens: List[Token]):
        self._tokens = list(filter(self._filter_comments, reversed(tokens)))
        self._length = len(tokens)
        self._previous_token: Token = Token()

    def front(self) -> Token:
        return self._tokens[-1]

    def is_next_instance_of(self, *class_types: type) -> bool:
        return self._length > 0 and isinstance(self._tokens[-1], class_types)

    def are_next_instances_of(self, *class_types: type) -> bool:
        if self._length < len(class_types):
            return False

        for index, class_type in enumerate(class_types):
            if not isinstance(self._tokens[-(index + 1)], class_type):
                return False

        return True

    def pop_front(self) -> Token:
        self._length -= 1
        self._previous_token = self._tokens.pop()
        return self._previous_token

    def cast_or_raise(
        self,
        token_type: Type[T],
        exception_builder: Callable[['TokenQueue'], Exception],
    ) -> T:
        if self.is_next_instance_of(token_type):
            return cast(token_type, self.pop_front())  # type: ignore
        else:
            raise exception_builder(self)

    def get_tokens(self) -> List[Token]:
        return list(reversed(self._tokens))

    def get_previous_token(self) -> Token:
        return self._previous_token

    def _filter_comments(self, token: Token) -> bool:
        return not isinstance(token, (Comment, LongComment))