from typing import Callable, Iterator, List, Optional

from luaparser.tokens import Identifier, Token


class LexerState():

    def __init__(self, line_iterator: Iterator):
        self.tokens: List[Token] = []
        self._line_iterator = line_iterator
        self._line_number = 0
        self._line = ''
        self._line_length = 0
        self._line_index = 0
        self._file_ended = False
        self._preceding_whitespaces: List[str] = []

    def append_token(self, token: Token):
        token.set_line_number(self._line_number)
        token.set_preceding_whitespaces(''.join(self._preceding_whitespaces))
        self.tokens.append(token)
        self._preceding_whitespaces = []

    def get_line_number(self) -> int:
        return self._line_number

    def get_tokens(self):
        return self.tokens

    def next_line(self) -> bool:
        self._line_index = 0
        self._line_number += 1

        try:
            self._line = next(self._line_iterator)
        except StopIteration:
            self._line = ''
            self._line_length = 0
            self._file_ended = True

            if self._line_number > 1:
                self._line_number += ''.join(self._preceding_whitespaces).count('\n') - 1

            return False

        self._line_length = len(self._line)

        return True

    def has_line_ended(self) -> bool:
        return self._line_index >= self._line_length

    def has_file_ended(self) -> bool:
        return self._file_ended

    def next_char(self, count: int = 1):
        self._line_index += count

    def skip_whitespaces(self):
        start_index = self._line_index

        while self._line_index < self._line_length and self._line[self._line_index].isspace():
            self._line_index += 1

        self._preceding_whitespaces.append(self._line[start_index:self._line_index])

    def get_current_char(self) -> str:
        return self._line[self._line_index] if self._line_index < self._line_length else ''

    def peak_next_char(self, count: int = 1) -> str:
        index = self._line_index + count
        return self._line[index] if index < self._line_length else ''

    def ends_with(self, string: str) -> bool:
        return self._line.endswith(string)

    def find(self, sub_string: str) -> int:
        return self._line.find(sub_string, self._line_index)

    def sub_string(self, start: Optional[int] = None, end: Optional[int] = None) -> str:
        start = self._line_index if start is None else start
        end = self._line_length if end is None else end

        return self._line[start:end]

    def accumulate_while_condition(self, callback: Callable[[str], bool]) -> str:
        start_index = self._line_index
        while self._line_index < self._line_length and callback(self._line[self._line_index]):
            self._line_index += 1

        return self.sub_string(start=start_index, end=self._line_index)

    def accumulate_until_symbol(self, symbol: str, multiline: bool = False) -> Optional[str]:
        index = self.find(symbol)

        if index >= 0:
            current_index = self._line_index
            self._jump_to(index)
            return self.sub_string(start=current_index, end=index)

        if not multiline:
            return None

        result = []

        while index < 0:
            result.append(self.sub_string())

            if not self.next_line():
                return None

            index = self.find(symbol)

        current_index = self._line_index
        self._jump_to(index)
        return ''.join(result) + self.sub_string(start=current_index, end=index)

    def is_char_escaped_at(self, index: int) -> bool:
        return False  # TODO

    def _jump_to(self, new_index):
        self._line_index = new_index
